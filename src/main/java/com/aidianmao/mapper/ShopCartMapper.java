package com.aidianmao.mapper;

import com.aidianmao.entity.ShopCart;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface ShopCartMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(ShopCart record);

    ShopCart selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ShopCart record);

    List<Map<String,Object>> selectAll(Map<String, Object> params);
}