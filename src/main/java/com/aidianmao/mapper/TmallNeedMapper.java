package com.aidianmao.mapper;

import com.aidianmao.entity.TmallNeed;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface TmallNeedMapper {
    List<ModelMap> selectAll(Map<String,Object> params);

    List<ModelMap> selectAllSale(Map<String,Object> params);

    Map<String,Object> selectById(String id);

    Map<String,Object> selectSaleById(String id);

    int deleteByPrimaryKey(String id);

    int insert(TmallNeed record);

    int insertSelective(TmallNeed record);

    TmallNeed selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(TmallNeed record);

    int updateByPrimaryKey(TmallNeed record);

    Map<String,Object> selectSaleDetailById(String id);
}