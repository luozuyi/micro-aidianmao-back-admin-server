package com.aidianmao.mapper;

import com.aidianmao.entity.BarginPrice;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface BarginPriceMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(BarginPrice record);

    BarginPrice selectByPrimaryKey(String id);

    Map<String,Object> selectById(String id);

    Map<String,Object> selectByMemberId(String id);

    List<Map<String,Object>> selectAll(Map<String,Object> params);

    int updateByPrimaryKeySelective(BarginPrice record);
}