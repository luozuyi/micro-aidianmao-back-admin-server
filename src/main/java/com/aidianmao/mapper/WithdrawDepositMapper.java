package com.aidianmao.mapper;

import com.aidianmao.entity.WithdrawDeposit;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Mapper
@Repository
public interface WithdrawDepositMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(WithdrawDeposit record);

    WithdrawDeposit selectByPrimaryKey(String id);

    Map<String,Object> selectById(String id);

    Map<String,Object> selectBankCardAddress(String id);

    List<Map<String,Object>> selectAll(Map<String,Object> params);

    int updateByPrimaryKeySelective(WithdrawDeposit record);

    List<WithdrawDeposit> selectAllWithdraw(Map<String,Object> params);
}