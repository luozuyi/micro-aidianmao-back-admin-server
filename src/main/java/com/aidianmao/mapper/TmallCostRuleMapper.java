package com.aidianmao.mapper;

import com.aidianmao.entity.TmallCostRule;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface TmallCostRuleMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(TmallCostRule record);

    TmallCostRule selectByPrimaryKey(String id);

    TmallCostRule selectByNum(String num);

    TmallCostRule selectByDiscount(TmallCostRule record);

    List<TmallCostRule> selectAll();

    int updateByPrimaryKeySelective(TmallCostRule record);

}