package com.aidianmao.mapper;

import com.aidianmao.entity.SysRoleSysRes;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface SysRoleSysResMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysRoleSysRes record);

    int insertSelective(SysRoleSysRes record);

    SysRoleSysRes selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysRoleSysRes record);

    int updateByPrimaryKey(SysRoleSysRes record);

    List<SysRoleSysRes> selectByRoleId(String roleId);

    int deleteByResId(String resId);
}