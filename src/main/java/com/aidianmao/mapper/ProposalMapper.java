package com.aidianmao.mapper;

import com.aidianmao.entity.Proposal;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface ProposalMapper {
    Map<String,Object> selectById(String id);

    List<ModelMap> selectAll(Map<String,Object> params);

    int deleteByPrimaryKey(String id);

    int insert(Proposal record);

    int insertSelective(Proposal record);

    Proposal selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Proposal record);

    int updateByPrimaryKey(Proposal record);
}