package com.aidianmao.mapper;

import com.aidianmao.entity.OnlineQuestionAnswer;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface OnlineQuestionAnswerMapper {
    List<ModelMap> selectAll(Map<String,Object> params);

    Map<String,Object> selectById(String id);

    int deleteByPrimaryKey(String id);

    int insert(OnlineQuestionAnswer record);

    int insertSelective(OnlineQuestionAnswer record);

    OnlineQuestionAnswer selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(OnlineQuestionAnswer record);

    int updateByPrimaryKey(OnlineQuestionAnswer record);
}