package com.aidianmao.mapper;

import com.aidianmao.entity.PlatformMessage;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface PlatformMessageMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(PlatformMessage record);

    PlatformMessage selectByPrimaryKey(String id);

    List<Map<String,Object>> selectAll(Map<String,Object> params);

    int updateByPrimaryKeySelective(PlatformMessage record);

    List<PlatformMessage> selectByMemberId(String memberId);
}