package com.aidianmao.mapper;

import com.aidianmao.entity.AdminMember;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface AdminMemberMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(AdminMember record);

    AdminMember selectByPrimaryKey(String id);

    AdminMember selectByMemberId(String memberId);

    List<Map<String,Object>> selectAll(Map<String,Object> params);

    List<Map<String,Object>> selectByAdminId(Map<String,Object> params);

    List<Map<String,Object>> selectMember(Map<String,Object> params);

    List<Map<String,Object>> selectAdminMember(Map<String,Object> params);

    List<Map<String,Object>> selectSellAdminAll(Map<String,Object> params);

    int updateByPrimaryKeySelective(AdminMember record);

    int deleteMany(Map<String,Object> map);

    Map<String,Object> selectSellById(String id);

    Integer selectByDay(String adminId);

    Integer selectByDays(String adminId);

    Integer selectCount(String adminId);
}