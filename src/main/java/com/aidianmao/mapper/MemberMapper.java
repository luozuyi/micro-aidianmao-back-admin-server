package com.aidianmao.mapper;

import com.aidianmao.entity.Member;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface MemberMapper {
    List<ModelMap> selectIncomeExpensesDetailById(Map<String,Object> params);
    List<ModelMap> selectAllMember(Map<String,Object> params);

    int deleteByPrimaryKey(String id);

    Member selectMemberById(String id);

    int insert(Member record);

    int insertSelective(Member record);

    Member selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Member record);

    int updateByPrimaryKey(Member record);

    Member selectByUserNameOrPhone(String loginWords);

    List<Member> selectByPhone(String phone);

    int updateCustomerSelective(Member record);

    List<Map<String,Object>> selectAllCustomer(Map<String,Object> params);

    Map<String,Object> selectCustomerById(String memberId);

    List<Map<String,Object>> selectByWeek(Map<String,Object> params);

    Integer selectMemberCount();

    Integer selectByCount();

    Integer selectByMonth();

    Integer selectByDay();

    Integer selectByDays();

    List<Member> selectAll(Map<String,Object> params);

}