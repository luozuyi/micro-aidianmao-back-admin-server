package com.aidianmao.mapper;

import com.aidianmao.entity.AssessTmall;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface AssessTmallMapper {
    List<Map<String,Object>> selectAll(Map<String,Object> params);

    Map<String,Object> selectById(String id);

    int deleteByPrimaryKey(String id);

    int insert(AssessTmall record);

    int insertSelective(AssessTmall record);

    AssessTmall selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(AssessTmall record);

    int updateByPrimaryKey(AssessTmall record);
}