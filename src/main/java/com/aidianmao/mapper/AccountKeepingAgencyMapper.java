package com.aidianmao.mapper;

import com.aidianmao.entity.AccountKeepingAgency;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface AccountKeepingAgencyMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(AccountKeepingAgency record);

    AccountKeepingAgency selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(AccountKeepingAgency record);

    List<ModelMap> selectAll(Map<String,Object> params);
}