package com.aidianmao.mapper;

import com.aidianmao.entity.WithdrawFreezeDetail;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface WithdrawFreezeDetailMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(WithdrawFreezeDetail record);

    WithdrawFreezeDetail selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(WithdrawFreezeDetail record);

    List<WithdrawFreezeDetail> selectByWithdrawDepositId(String withdrawDepositId);

    List<Map<String,Object>> selectAll(Map<String,Object> params);
}