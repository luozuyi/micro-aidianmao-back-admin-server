package com.aidianmao.mapper;

import com.aidianmao.entity.TmallFastCommit;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface TmallFastCommitMapper {
    List<ModelMap> selectAll(Map<String,Object> params);

    int deleteByPrimaryKey(String id);

    int insert(TmallFastCommit record);

    int insertSelective(TmallFastCommit record);

    TmallFastCommit selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(TmallFastCommit record);

    int updateByPrimaryKey(TmallFastCommit record);
}