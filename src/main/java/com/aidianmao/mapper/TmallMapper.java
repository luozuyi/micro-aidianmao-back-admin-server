package com.aidianmao.mapper;

import com.aidianmao.entity.Tmall;
import com.aidianmao.entity.TmallDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface TmallMapper {
    int deleteByPrimaryKey(String id);

    int insert(Tmall record);

    int insertSelective(Tmall record);

    Tmall selectByPrimaryKey(String id);

   // Map<String,Object> selectById(String id);

    TmallDetail selectById(String id);

    int updateByPrimaryKey(@Param("tmallCostRuleId") String tmallCostRuleId,@Param("id") String id);

    int updateByPrimaryKeySelective(Tmall record);

    List<Map<String,Object>> selectAll(Map<String,Object> params);

    List<Map<String,Object>> selectAllTmall(Map<String,Object> params);

    Integer selectByMonth();

    Integer selectByDay();

    Integer selectByYesterday();

    Map<String,Object> selectByTmallId(String id);
}