package com.aidianmao.mapper;

import com.aidianmao.entity.IncomeExpensesDetail;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Mapper
@Repository
public interface IncomeExpensesDetailMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(IncomeExpensesDetail record);

    IncomeExpensesDetail selectByPrimaryKey(String id);

    List<Map<String,Object>> selectAll(Map<String,Object> params);

    int updateByPrimaryKeySelective(IncomeExpensesDetail record);

}