package com.aidianmao.mapper;

import com.aidianmao.entity.Bank;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface BankMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(Bank record);

    Bank selectByPrimaryKey(String id);

    List<Bank> selectAll();

    int updateByPrimaryKeySelective(Bank record);
}