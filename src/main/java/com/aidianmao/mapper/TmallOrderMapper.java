package com.aidianmao.mapper;

import com.aidianmao.entity.TmallOrder;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface TmallOrderMapper {
    Integer selectTmallOrderCount();

    int deleteByPrimaryKey(String id);

    int insert(TmallOrder record);

    int insertSelective(TmallOrder record);

    TmallOrder selectByPrimaryKey(String id);

    TmallOrder selectByCode(String code);

    int updateByPrimaryKeySelective(TmallOrder record);

    int updateByPrimaryKey(TmallOrder record);

    List<Map<String,Object>> selectAll(Map<String,Object> params);

    List<Map<String,Object>> selectByStatus(Map<String,Object> params);

    List<Map<String,Object>> selectByTransferStatus(Map<String,Object> params);

    List<Map<String,Object>> selectFundAdjustAll(Map<String,Object> params);

    Map<String,Object> detailById(String id);

    Map<String,Object> statusDetailById(String id);

    Map<String,Object> payDetailById(String id);

    Map<String,Object> paymentDetailById(String id);

    Map<String,Object> selectTakeOverNameById(String id);

    Integer selectByDay();

    Integer selectMonth();

    Integer selectUnpaid();
}