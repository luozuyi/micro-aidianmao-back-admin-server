package com.aidianmao.mapper;

import com.aidianmao.entity.MemberFundAdjust;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface MemberFundAdjustMapper {
    int deleteByPrimaryKey(String id);

    int insert(MemberFundAdjust record);

    int insertSelective(MemberFundAdjust record);

    MemberFundAdjust selectByPrimaryKey(String id);

    MemberFundAdjust selectByRelationId(String id);

    List<Map<String, Object>> selectAll(Map<String, Object> params);

    int updateByPrimaryKeySelective(MemberFundAdjust record);

    int updateByPrimaryKey(MemberFundAdjust record);
}