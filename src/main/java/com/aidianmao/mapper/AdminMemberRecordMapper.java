package com.aidianmao.mapper;

import com.aidianmao.entity.AdminMemberRecord;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface AdminMemberRecordMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(AdminMemberRecord record);

    AdminMemberRecord selectByPrimaryKey(String id);

    List<Map<String,Object>> selectAll(Map<String,Object> params);

    int updateByPrimaryKeySelective(AdminMemberRecord record);
}