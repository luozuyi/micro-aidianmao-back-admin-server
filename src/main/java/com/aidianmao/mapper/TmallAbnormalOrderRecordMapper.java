package com.aidianmao.mapper;

import com.aidianmao.entity.TmallAbnormalOrderRecord;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface TmallAbnormalOrderRecordMapper {
    TmallAbnormalOrderRecord selectByOrderId(String orderId);

    Map<String,Object> selectById(String id);

    List<ModelMap> selectAll(Map<String,Object> params);

    int deleteByPrimaryKey(String id);

    int insert(TmallAbnormalOrderRecord record);

    int insertSelective(TmallAbnormalOrderRecord record);

    TmallAbnormalOrderRecord selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(TmallAbnormalOrderRecord record);

    int updateByPrimaryKey(TmallAbnormalOrderRecord record);
}