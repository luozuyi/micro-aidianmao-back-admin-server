package com.aidianmao.mapper;

import com.aidianmao.entity.PlatformFundDetail;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface PlatformFundDetailMapper {
    int deleteByPrimaryKey(String id);

    int insert(PlatformFundDetail record);

    int insertSelective(PlatformFundDetail record);

    PlatformFundDetail selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(PlatformFundDetail record);

    int updateByPrimaryKey(PlatformFundDetail record);

    List<PlatformFundDetail> selectAll(Map<String,Object> params);

    List<Map<String,Object>> selectAllByTime(Map<String,Object> params);
}