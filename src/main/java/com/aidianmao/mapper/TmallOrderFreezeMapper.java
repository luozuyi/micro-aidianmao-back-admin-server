package com.aidianmao.mapper;

import com.aidianmao.entity.TmallOrderFreeze;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface TmallOrderFreezeMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(TmallOrderFreeze record);

    TmallOrderFreeze selectByPrimaryKey(String id);

    List<Map<String,Object>> selectAll(Map<String,Object> params);

    int updateByPrimaryKeySelective(TmallOrderFreeze record);

    List<TmallOrderFreeze> selectByTmallOrderId(String tmallOrderId);

    List<TmallOrderFreeze> selectByMemberId(String memberId);
}