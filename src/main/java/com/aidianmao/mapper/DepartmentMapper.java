package com.aidianmao.mapper;

import com.aidianmao.entity.Department;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface DepartmentMapper {
    int deleteByPrimaryKey(String id);

    int insert(Department record);

    int insertSelective(Department record);

    Department selectByPrimaryKey(String id);

    List<Department> selectByHeadManAdminId(String id);
    Department selectByHeadAdminId(String id);

    int updateByPrimaryKeySelective(Department record);

    int updateByPrimaryKey(Department record);

    List<Map<String,Object>> selectAll();

    List<Map<String,Object>> selectLeaderName();
}