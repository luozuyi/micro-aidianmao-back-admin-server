package com.aidianmao.mapper;

import com.aidianmao.entity.Remittance;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Mapper
@Repository
public interface RemittanceMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(Remittance record);

    Remittance selectByPrimaryKey(String id);

    Map<String,Object> selectById(String id);

    List<Map<String,Object>> selectAll(Map<String,Object> params);

    int updateByPrimaryKeySelective(Remittance record);
}