package com.aidianmao.mapper;

import com.aidianmao.entity.BusinessCooperation;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface BusinessCooperationMapper{
    Map<String,Object> selectById(String id);

    List<ModelMap> selectAll(Map<String,Object> params);

    int deleteByPrimaryKey(String id);

    int insert(BusinessCooperation record);

    int insertSelective(BusinessCooperation record);

    BusinessCooperation selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BusinessCooperation record);

    int updateByPrimaryKey(BusinessCooperation record);
}