package com.aidianmao.mapper;

import com.aidianmao.entity.SalespersonConfig;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface SalespersonConfigMapper {
    Map<String,Object> selectDetailById(String id);

    List<Map<String,Object>> selectAll(Map<String,Object> params);

    int deleteByPrimaryKey(String id);

    SalespersonConfig selectByAdminId(String adminId);

    int insert(SalespersonConfig record);

    int insertSelective(SalespersonConfig record);

    SalespersonConfig selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SalespersonConfig record);

    int updateByPrimaryKey(SalespersonConfig record);
}