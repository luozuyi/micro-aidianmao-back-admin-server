package com.aidianmao.mapper;

import com.aidianmao.entity.Alipay;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface AlipayMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(Alipay record);

    Alipay selectByPrimaryKey(String id);

    Map<String,Object> selectById(String id);

    List<Map<String,Object>> selectAll(Map<String,Object> params);

    int updateByPrimaryKeySelective(Alipay record);
}