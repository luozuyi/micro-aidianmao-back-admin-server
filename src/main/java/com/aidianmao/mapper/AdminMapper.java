package com.aidianmao.mapper;

import com.aidianmao.entity.Admin;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface AdminMapper {
    int deleteByPrimaryKey(String id);

    int insert(Admin record);

    int insertSelective(Admin record);

    Admin selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Admin record);

    int updateByPrimaryKey(Admin record);

    Admin selectByAdminName(String adminName);

    Admin selectByRealName(String realName);

    List<Map<String,Object>> selectAll(Map<String,Object> params);

    Map<String,Object> selectAdminAndRoleByPrimaryKey(String id);

    List<Admin> selectByGroup(String adminId);

    int updateByGroup(@Param("adminId") String adminId,@Param("id") String id);

    Admin selectById(String adminId);
}