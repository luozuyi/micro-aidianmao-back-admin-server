package com.aidianmao.mapper;

import com.aidianmao.entity.MemberRemark;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface MemberRemarkMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(MemberRemark record);

    MemberRemark selectByPrimaryKey(String id);

    List<Map<String,Object>> selectCntentByMemberId(String memberId);

    List<Map<String,Object>> selectByMemberId(String memberId);

    Map<String,Object> selectById(String id);

    int updateByPrimaryKeySelective(MemberRemark record);
    
}