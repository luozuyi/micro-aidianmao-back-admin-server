package com.aidianmao.mapper;

import com.aidianmao.entity.Advertise;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface AdvertiseMapper {
    List<ModelMap> selectAdvertiseAll(Map<String,Object> params);

    int deleteByPrimaryKey(String id);

    int insert(Advertise record);

    int insertSelective(Advertise record);

    Advertise selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Advertise record);

    int updateByPrimaryKey(Advertise record);
}