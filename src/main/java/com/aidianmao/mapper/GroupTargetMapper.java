package com.aidianmao.mapper;

import com.aidianmao.entity.GroupTarget;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Mapper
@Repository
public interface GroupTargetMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(GroupTarget record);

    GroupTarget selectByPrimaryKey(String id);

    Map<String,Object> selectById(String id);

    List<Map<String,Object>> selectAll(Map<String,Object> params);

    int updateByPrimaryKeySelective(GroupTarget record);
}