package com.aidianmao.mapper;

import com.aidianmao.entity.CustomerQqRecord;
import com.aidianmao.entity.CustomerServiceQq;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface CustomerServiceQqMapper {
    List<CustomerServiceQq> selectNameByType(String type);

    CustomerServiceQq selectById(String id);

    int updateStatus(CustomerServiceQq customerServiceQq);

    int deleteByPrimaryKey(String id);

    int insert(CustomerServiceQq record);

    int insertSelective(CustomerServiceQq record);

    CustomerServiceQq selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(CustomerServiceQq record);

    int updateByPrimaryKey(CustomerServiceQq record);
}