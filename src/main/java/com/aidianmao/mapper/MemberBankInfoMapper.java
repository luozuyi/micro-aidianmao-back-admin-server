package com.aidianmao.mapper;

import com.aidianmao.entity.MemberBankInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface MemberBankInfoMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(MemberBankInfo record);

    MemberBankInfo selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(MemberBankInfo record);
}