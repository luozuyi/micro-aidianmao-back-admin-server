package com.aidianmao.mapper;

import com.aidianmao.entity.Group;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface GroupMapper {
    int deleteByPrimaryKey(String id);

    int insert(Group record);

    int insertSelective(Group record);

    Group selectByPrimaryKey(String id);

    List<Group> selectByLeaderId(String leaderId);

    List<Map<String,Object>> selectLeader();

    int updateByPrimaryKeySelective(Group record);

    int updateByPrimaryKey(Group record);

    List<Map<String,Object>> selectAll();
}