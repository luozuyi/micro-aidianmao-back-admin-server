package com.aidianmao.mapper;

import com.aidianmao.entity.SalespersonTarget;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface SalespersonTargetMapper {
    int deleteByPrimaryKey(String id);

    int insert(SalespersonTarget record);

    int insertSelective(SalespersonTarget record);

    SalespersonTarget selectByPrimaryKey(String id);

    SalespersonTarget selectBySaleapersonId(String saleapersonId);

    List<SalespersonTarget> selectBySaleId(String saleapersonId);

   SalespersonTarget selectByMonthAndYearAndGoal(SalespersonTarget record);


    int updateByPrimaryKeySelective(SalespersonTarget record);

    int updateByPrimaryKey(SalespersonTarget record);
}