package com.aidianmao.mapper;

import com.aidianmao.entity.DepartmentTarget;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Mapper
@Repository
public interface DepartmentTargetMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(DepartmentTarget record);

    DepartmentTarget selectByPrimaryKey(String id);

    Map<String,Object> selectById(String id);

    List<Map<String,Object>> selectAll(Map<String,Object> params);

    int updateByPrimaryKeySelective(DepartmentTarget record);
}