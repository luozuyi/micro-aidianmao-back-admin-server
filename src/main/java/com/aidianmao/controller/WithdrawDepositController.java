package com.aidianmao.controller;

import com.aidianmao.entity.WithdrawDeposit;
import com.aidianmao.service.WithdrawDepositService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class WithdrawDepositController {
    @Autowired
    private WithdrawDepositService withdrawDepositService;
    /**
     * 分页条件查询提现信息
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 条件查询的参数
     * @return
     */
    @GetMapping(value = "v1/auth/withdraw-deposit/paginations")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return withdrawDepositService.pageList(pageNum,pageSize,params);
    }
    /**
     * 提现信息详情
     * @param id 主键id
     * @return
     */
    @GetMapping(value = "v1/auth/withdraw-deposit/details")
    public Result detail(String id) {
        return withdrawDepositService.detail(id);
    }

    /**
     * 提现信息修改
     * @param record 参数WithdrawDeposit实体
     * @param checkResult 审核结果
     * @param aidianmaoAdminToken 凭据
     * @return
     */
    @PatchMapping(value = "v1/auth/withdraw-deposit/contents")
    public Result update(WithdrawDeposit record,Integer checkResult,Integer checkType,@CookieValue String aidianmaoAdminToken) {
        return withdrawDepositService.update(record,checkResult,checkType,aidianmaoAdminToken);
    }
    /**
     * 提现信息免手续费
     * @param id 主键id
     * @return
     */
    @PatchMapping(value = "v1/auth/withdraw-deposit/contents-fee")
    public Result updateServiceFee(String id,@CookieValue String aidianmaoAdminToken) {
        return withdrawDepositService.updateServiceFee(id,aidianmaoAdminToken);
    }

    /**
     * 拒绝申请
     * @param id     主键id
     * @param refuseReason      拒绝原因
     * @param aidianmaoAdminToken  凭据
     * @return
     */
    @PatchMapping(value = "v1/auth/withdraw-deposit/refuse")
    public Result refuseApply(String id,String refuseReason,@CookieValue String aidianmaoAdminToken){
        return withdrawDepositService.refuseApply(id,refuseReason,aidianmaoAdminToken);
    }

    /**
     * 处理中
     * @param id    主键id
     * @param checkResult     申请结果
     * @param aidianmaoAdminToken   凭据
     * @return
     */
    @PatchMapping(value = "v1/auth/withdraw-deposit/accept")
    public Result acceptApply(String id,String checkResult,@CookieValue String aidianmaoAdminToken){
        return withdrawDepositService.acceptApply(id,checkResult,aidianmaoAdminToken);
    }

    /**
     * 接受申请
     * @param id     主键id
     * @param aidianmaoAdminToken  凭据
     * @return
     */
    @PatchMapping(value = "v1/auth/withdraw-deposit/processing")
    public  Result processing(String id,@CookieValue String aidianmaoAdminToken){
        return withdrawDepositService.processing(id,aidianmaoAdminToken);
    }

}
