package com.aidianmao.controller;

import com.aidianmao.service.CitysService;
import com.aidianmao.service.CountrysService;
import com.aidianmao.service.ProvincesService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author Catch22
 * @date 2018年6月21日
 */
@RestController
public class ProCityCountryController {

	@Autowired
	private ProvincesService proService;

	@Autowired
	private CitysService cityService;

	@Autowired
	private CountrysService countryService;

	/**
	 * 条件查询省份列表
	 * @param pageNum  当前页
	 * @param pageSize  一页显示多少条
	 * @param params     参数map
	 * @return
	 */
	@GetMapping(value = "v1/auth/provinces/selection")
	public Result getAllProvinces(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
		return proService.listPage(pageNum, pageSize, params);
	}

	/**
	 * id查询省份详情
	 * @param id 主键id
	 * @return
	 */
	@GetMapping(value = "v1/auth/provinces/detail")
	public Result getProvincesByPrimaryKey(@RequestParam String id) {
		return proService.selectByPrimaryKey(id);
	}

	/**
	 * 条件查询城市列表
	 * @param pageNum  当前页
	 * @param pageSize  一页显示多少条
	 * @param params     参数map
	 * @return
	 */
	@GetMapping(value = "v1/auth/citys/selection")
	public Result getAllCitys(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
		return cityService.listPage(pageNum, pageSize, params);
	}

	/**
	 * id查询城市详情
	 * @param id 主键id
	 * @return
	 */
	@GetMapping(value = "v1/auth/citys/detail")
	public Result getCitysByPrimaryKey(@RequestParam String id) {
		return cityService.selectByPrimaryKey(id);
	}

	/**
	 * 条件查询区列表
	 * @param pageNum  当前页
	 * @param pageSize  一页显示多少条
	 * @param params     参数map
	 * @return
	 */
	@GetMapping(value = "v1/auth/countrys/selection")
	public Result getAllCountrys(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
		return countryService.listPage(pageNum, pageSize, params);
	}

	/**
	 * id查询区详情
	 * @param id 主键id
	 * @return
	 */
	@GetMapping(value = "v1/auth/countrys/detail")
	public Result getCountrysByPrimaryKey(@RequestParam String id) {
		return countryService.selectByPrimaryKey(id);
	}
}
