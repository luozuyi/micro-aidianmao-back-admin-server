package com.aidianmao.controller;

import com.aidianmao.entity.Proposal;
import com.aidianmao.service.ProposalService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class ProposalController {

    @Autowired
    private ProposalService proposalService;

    /**
     *  建议分页条件查询
     * @param pageNum  当前页
     * @param pageSize  一页显示多少条
     * @param params     参数map
     * @return
     */
    @GetMapping(value = "v1/auth/proposals/pagination")
    public Result pagaProposalList(Integer pageNum, Integer pageSize,@RequestParam Map<String,Object> params){
        return proposalService.pagaList(pageNum,pageSize,params);
    }

    /**
     * 建议详情
     * @param id 主键id
     * @return
     */
    @GetMapping(value = "v1/auth/proposals/id")
    public  Result detail(String id){
        return proposalService.detail(id);
    }

    /**
     * 修改建议信息
     * @param proposal 主键id
     * @return
     */
    @PatchMapping(value = "v1/auth/proposals")
    public Result update(Proposal proposal, @CookieValue String aidianmaoAdminToken){
        return proposalService.update(proposal,aidianmaoAdminToken);
    }

    /**
     * 删除
     * @param id 主键id
     * @return
     */
    @PatchMapping(value = "v1/auth/proposals/id")
    public Result delete(String id,@CookieValue String aidianmaoAdminToken){
        return proposalService.deleteProposal(id,aidianmaoAdminToken);
    }
}
