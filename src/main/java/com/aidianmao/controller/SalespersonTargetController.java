package com.aidianmao.controller;

import com.aidianmao.entity.SalespersonTarget;
import com.aidianmao.service.SalespersonTargetService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SalespersonTargetController {

    @Autowired
    private SalespersonTargetService salespersonTargetService;

    /**
     * 添加个人目标
     * @param id
     * @param target
     * @return
     */
    @PostMapping(value = "v1/auth/salesperson-target")
    public Result insert(String id,SalespersonTarget target){
        return salespersonTargetService.insert(id,target);
    }


    /**
     * 个人目标列表
     * @param pageNum   当前页
     * @param pageSize  一页显示多少条
     * @param saleapersonId  业务员id
     * @return
     */
    @GetMapping(value = "v1/auth/salesperson-target/paginations")
    public Result pageList( Integer pageNum,Integer pageSize,String saleapersonId){
        return salespersonTargetService.pageList(pageNum,pageSize,saleapersonId);
    }
}
