package com.aidianmao.controller;

import com.aidianmao.service.BarginPriceService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class BarginPriceController {
    @Autowired
    private BarginPriceService barginPriceService;

    /**
     * 分页条件砍价信息
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/bargin-price/paginations")
    public Result pageLists(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return barginPriceService.pageList(pageNum,pageSize,params);
    }


    /**
     * 分页条件砍价信息
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/bargin-price/pagination")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return barginPriceService.pageList(pageNum,pageSize,params);
    }
    /**
     * 砍价信息详情
     * @param id 主键id
     * @return
     */
    @GetMapping(value = "v1/auth/bargin-price/details")
    public Result detail(String id) {
        return barginPriceService.detail(id);
    }

    /**
     * 我来联系
     * @param id             主键id
     * @param remark         备注
     * @param aidianmaoAdminToken 凭据
     * @return
     */
    @PostMapping(value = "v1/auth/bargin-price/relation")
    public Result myRelation(String id,String remark,@CookieValue String aidianmaoAdminToken){
        return barginPriceService.myRelation(id,remark,aidianmaoAdminToken);
    }
}
