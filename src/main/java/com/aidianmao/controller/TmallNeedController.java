package com.aidianmao.controller;

import com.aidianmao.entity.ProvincesCitysCountrys;
import com.aidianmao.entity.TmallNeed;
import com.aidianmao.service.TmallNeedService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class TmallNeedController {

    @Autowired
    private TmallNeedService tmallNeedService;

    /**
     * 动态分页条件查询需求
     * @param pageNum  当前页
     * @param pageSize  一页显示多少条
     * @param params     条件参数
     * @return
     */
     @GetMapping(value = "v1/auth/tmall-needs/pagination")
    public Result listPage(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params){
           return tmallNeedService.listPageBySelection(pageNum,pageSize,params);
    }


    /**
     * 修改需求
     * @param tmallNeed 参数实体
     * @return
     */
    @PatchMapping(value = "v1/auth/tmall-needs")
    public Result updateNeed(TmallNeed tmallNeed, ProvincesCitysCountrys provincesCitysCountrys,@CookieValue String aidianmaoAdminToken){
        return tmallNeedService.updateTmallNeed(tmallNeed,provincesCitysCountrys,aidianmaoAdminToken);
    }

    /**
     * 求购的详细信息
     * @param id 主键ID
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-needs/id")
    public Result selectById(String id){
        return tmallNeedService.selectById(id);
    }


    /**
     * 销售求购列表
     * @param pageNum  当前页
     * @param pageSize  一页显示多少条
     * @param params     条件参数
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-needs/sale/pagination")
    public Result listNeedPage(Integer pageNum, Integer pageSize,@RequestParam Map<String,Object> params){
        return tmallNeedService.saleNeed(pageNum,pageSize,params);
    }

    /**
     * 销售中的我去联系
     * @param id 主键ID
     * @return
     */
    @PostMapping(value = "v1/auth/tmall-needs/sale/id")
    public Result selectSaleById(String id,@CookieValue String aidianmaoAdminToken){
        return tmallNeedService.saleRelation(id,aidianmaoAdminToken);
    }

    /**
     * 销售中的找店信息收集详情
     * @param id 主键ID
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-needs/sale/detail")
    public Result saleDetail(String id){
        return tmallNeedService.saleDetail(id);
    }
}
