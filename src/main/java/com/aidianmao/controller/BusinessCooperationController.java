package com.aidianmao.controller;

import com.aidianmao.entity.BusinessCooperation;
import com.aidianmao.service.BusinessCooperationService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class BusinessCooperationController {

    @Autowired
    private BusinessCooperationService businessCooperationService;

    /**
     * 商业合作列表
     * @param pageNum  当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/business-cooperations/pagination")
    public Result pageList(Integer pageNum, Integer pageSize,@RequestParam Map<String, Object> params){
        return businessCooperationService.pageList(pageNum,pageSize,params);
    }



    /**
     * 商业合作详情
     * @param id  主键id
     * @return
     */
    @GetMapping(value = "v1/auth/business-cooperations/id")
    public Result detail(String id){
        return businessCooperationService.detail(id);
    }
}
