package com.aidianmao.controller;

import com.aidianmao.service.IncomeExpensesDetailService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class IncomeExpensesDetailController {
    @Autowired
    private IncomeExpensesDetailService incomeExpensesDetailService;
    /**
     * 分页条件查询会员收入明细信息
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 条件查询的参数
     * @return
     */
    @GetMapping(value = "v1/auth/income-expenses-detail/paginations")
    public Result pageList(Integer pageNum, Integer pageSize,@RequestParam Map<String,Object> params) {
        return incomeExpensesDetailService.pageList(pageNum,pageSize,params);
    }
}
