package com.aidianmao.controller;

import com.aidianmao.entity.AssessTmall;
import com.aidianmao.service.AssessTmallService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class AssessTmallController {
    @Autowired
    private AssessTmallService assessTmallService;

    /**
     * 分页条件查询
     * @param pageNum   当前页
     * @param pageSize  一页显示多少条
     * @param params    条件参数
     * @return
     */
    @GetMapping(value = "v1/auth/assess-tmalls/pagination")
    public Result pageTmallList(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params){
        return assessTmallService.pageAssessTmallList(pageNum,pageSize,params);
    }

    /**
     * 估价详情
     * @param id  主键id
     * @return
     */
    @GetMapping(value = "v1/auth/assess-tmalls/id")
    public Result detail(String id){
        return assessTmallService.detail(id);
    }

    /**
     *修改估价信息
     * @param assessTmall 估计信息
     * @return
     */
    @PatchMapping(value = "v1/auth/assess-tmall/assessResult")
    public Result updateAssessResult(AssessTmall assessTmall,@CookieValue String aidianmaoAdminToken){
        return  assessTmallService.updateAssessResult(assessTmall,aidianmaoAdminToken);
    }
}
