package com.aidianmao.controller;

import com.aidianmao.entity.Remittance;
import com.aidianmao.service.RemittanceService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class RemittanceController {
    @Autowired
    private RemittanceService remittanceService;
    /**
     * 分页条件查询线下汇款
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 条件查询的参数
     * @return
     */
    @GetMapping(value = "v1/auth/remittance/paginations")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return remittanceService.pageList(pageNum,pageSize,params);
    }
    /**
     * 线下汇款详情
     * @param id 主键id
     * @return
     */
    @GetMapping(value = "v1/auth/remittance/details")
    public Result detail(String id) {
        return remittanceService.detail(id);
    }

    /**
     * 线下汇款修改
     * @param record 参数Remittance实体
     * @param checkResult 审核结果
     * @param aidianmaoAdminToken 凭据
     * @return
     */
    @PatchMapping(value = "v1/auth/remittance/contents")
    public Result update(Remittance record,Integer checkResult,@CookieValue String aidianmaoAdminToken) {
        return remittanceService.update(record,checkResult,aidianmaoAdminToken);
    }
}
