package com.aidianmao.controller;

import com.aidianmao.entity.TmallOrder;
import com.aidianmao.service.TmallAbnormalOrderRecordService;
import com.aidianmao.utils.Result;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class TmallAbnormalOrderRecordController {

    @Autowired
    private TmallAbnormalOrderRecordService tmallAbnormalOrderRecordService;

    /**
     * 分页条件查询异常订单
     * @param pageNum     当前页
     * @param pageSize    一页显示多少条
     * @param params      条件参数
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-abnormal-order-records/pagination")
    public Result listPage(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return tmallAbnormalOrderRecordService.listPage(pageNum, pageSize, params);
    }

    /**
     * 异常订单详情
     * @param id    主键id
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-abnormal-order-records/id")
    public Result detail(String id){
        return tmallAbnormalOrderRecordService.detail(id);
    }


}
