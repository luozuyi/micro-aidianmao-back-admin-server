package com.aidianmao.controller;

import com.aidianmao.entity.Department;
import com.aidianmao.service.DepartmentService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService;

    /**
     * 添加部门
     * @param department 部门对象
     * @return
     */
    @PostMapping(value = "v1/auth/departments")
    public Result addDepartment(Department department) {
        return departmentService.addDepartment(department);
    }

    /**
     * 分页查询部门
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/departments/pagination")
    public Result pageList(Integer pageNum, Integer pageSize) {
        return departmentService.pageList(pageNum,pageSize);
    }

    /**
     * 查询部长的名字
     * @return
     */
    @GetMapping(value = "v1/auth/departments/leaderName")
    public Result pageName(){
        return departmentService.listName();
    }

}
