package com.aidianmao.controller;

import com.aidianmao.entity.FriendshipLink;
import com.aidianmao.service.FriendshipLinkService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class FriendshipLinkController {

    @Autowired
    private FriendshipLinkService friendshipLinkService;

    /**
     * 友情链接分页查询
     * @param pageSize   当前页
     * @param pageNum    一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/friendship-link/paginations")
    public Result pageList(Integer pageSize, Integer pageNum){
        return friendshipLinkService.pageList(pageNum,pageSize);
    }

    /**
     * 添加友情链接
     * @param friendshipLink  友情链接实体
     * @return
     */
    @PostMapping(value = "v1/auth/friendship-link")
    public Result insertFriendship(FriendshipLink friendshipLink){
        return friendshipLinkService.insertFriendshipLink(friendshipLink);
    }

    /**
     * 修改友情链接
     * @param friendshipLink  友情链接实体
     * @return
     */
    @PatchMapping(value = "v1/auth/friendship-link")
    public Result update(FriendshipLink friendshipLink){
        return friendshipLinkService.update(friendshipLink);
    }

    /**
     * 禁用成功
     * @param id  主键id
     * @return
     */
    @PatchMapping(value = "v1/auth/friendship-link/id")
    public Result updateDel(String id){
        return friendshipLinkService.updateDel(id);
    }

    /**
     *启用成功
     * @param id  主键id
     * @return
     */
    @PatchMapping(value = "v1/auth/friendship-links/id")
    public Result updateDelFlag(String id){
        return friendshipLinkService.updateDelFlag(id);
    }

    /**
     * 删除（真删除）
     * @param id  主键id
     * @return
     */
    @DeleteMapping(value = "v1/auth/friendship-links/id")
    public Result delete(String id){
        return friendshipLinkService.delete(id);
    }

}
