package com.aidianmao.controller;

import com.aidianmao.entity.AccountKeepingAgency;
import com.aidianmao.service.AccountKeepingAgencyService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class AccountKeepingAgencyController {
    @Autowired
    private AccountKeepingAgencyService accountKeepingAgencyService;
    /**
     * 分页查询所有记账
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/account-keeping-agency/paginations")
    public Result pageList(Integer pageNum, Integer pageSize,@RequestParam Map<String, Object> params){
        return accountKeepingAgencyService.pageList(pageNum,pageSize,params);
    }
    /**
     * 记账详情
     * @param id 主键
     * @return
     */
    @GetMapping(value = "v1/auth/account-keeping-agency/details")
    public Result detail(String id){
        return accountKeepingAgencyService.detail(id);
    }

    /**
     * 交接
     * @param list
     * @return
     */
    @PatchMapping(value = "v1/auth/account-keeping-agency/ids")
    public Result update(@RequestParam(value = "ids[]") String[] list){
        return  accountKeepingAgencyService.update(list);
    }

    /**
     * 取消交接
     * @param list
     * @return
     */
    @PatchMapping(value = "v1/auth/account-keeping-agency/cancelTakeOver")
    public Result cancelTakeOver(@RequestParam(value = "ids[]") String[] list){
        return  accountKeepingAgencyService.cancelTakeOver(list);
    }

    /**
     *  记账修改
     *  @param record 记账对象
     *  @return
     */
    /*@PatchMapping(value = "v1/auth/account-keeping-agency/contents")
    public Result update(AccountKeepingAgency record){
        return accountKeepingAgencyService.update(record);
    }*/
    /**
     *  记账新增
     *  @param record 记账对象
     *  @return
     */
    /*@PostMapping(value = "v1/auth/account-keeping-agency/informations")
    public Result addAccountKeepingAgency(AccountKeepingAgency record){
        return accountKeepingAgencyService.insert(record);
    }*/


}
