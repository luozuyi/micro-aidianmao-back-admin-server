package com.aidianmao.controller;

import com.aidianmao.service.AdminService;
import com.aidianmao.utils.Result;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class AdminController {
    @Autowired
    private AdminService adminService;

    /**
     * 添加管理员
     * @param adminName 管理员名称
     * @param realName 管理员真实名称
     * @param password 管理员密码
     * @param roleId 管理员角色id
     * @param isDelFlag 是否禁用0：否，1：是
     * @param surePassword 确认密码
     * @param maximunNum 最大客户数
     * @return
     */
    @PostMapping(value = "v1/auth/admins")
    public Result addAdmin(String adminName,String realName,String qq, String password, String roleId, String isDelFlag, String surePassword,Integer maximunNum) {
        return adminService.addAdmin(adminName,realName,qq,password, roleId, isDelFlag, surePassword,maximunNum);
    }

    /**
     * 分页查询管理员列表
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/admins/pagination")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return adminService.pageList(pageNum, pageSize,params);
    }

    /**
     * 查询管理员详情以及角色
     * @param adminId 主键id
     * @return
     */
    @GetMapping(value = "v1/auth/admins/id")
    public Result findById(String adminId) {
        return adminService.findById(adminId);
    }

    /**
     * 修改管理员以及角色
     * @param adminId 管理员id
     * @param password 密码
     * @param roleId 要修改的角色id
     * @param isDisable 是否禁用
     * @param surePassword 确认密码
     * @return
     */
    @PatchMapping(value = "v1/auth/admins")
    public Result update(String adminId, String password, String roleId, String isDisable, String surePassword) {
        return adminService.update(adminId, password, roleId, isDisable, surePassword);
    }

    /**
     * 分页查询我的组员
     * @param aidianmaoAdminToken
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/admin/group-pagination")
    public Result pageListGroup(@CookieValue String aidianmaoAdminToken, Integer pageNum, Integer pageSize) {
        return adminService.pageListGroup(aidianmaoAdminToken,pageNum, pageSize);
    }

    /**
     * 分组信息修改
     * @param adminId 组长主键ID
     * @param id 组员主键ID
     * @return
     */
    @PatchMapping(value = "v1/auth/admin/group-contents")
    public Result updateGroup(String adminId, String id) {
        return adminService.updateGroup(adminId, id);
    }

    /**
     * 启用
     * @param id
     * @return
     */
    @PatchMapping(value = "v1/auth/admin/del_flag/id")
    public Result updateDel(String id){
        return adminService.updateDel(id);
    }

    /**
     * 禁用
     * @param id
     * @return
     */
    @PatchMapping(value = "v1/auth/admins/del_flag/id")
    public Result delFlag(String id){
        return adminService.delFlag(id);
    }

    /**
     * 删除管理员
     * @param id
     * @return
     */
    @DeleteMapping(value = "v1/auth/admin/id")
    public Result delete(String id){
        return adminService.delete(id);
    }
}
