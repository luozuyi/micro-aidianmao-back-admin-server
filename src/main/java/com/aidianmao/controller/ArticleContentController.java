package com.aidianmao.controller;

import com.aidianmao.service.ArticleContentService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ArticleContentController {
    @Autowired
    private ArticleContentService articleContentService;

    /**
     * 分页查询文章内容
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/artilce-contents/pagination")
    public Result listPage(Integer pageNum, Integer pageSize) {
        return articleContentService.listPage(pageNum, pageSize);
    }
}
