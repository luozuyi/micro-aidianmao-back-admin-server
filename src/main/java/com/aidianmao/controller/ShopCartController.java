package com.aidianmao.controller;

import com.aidianmao.service.ShopCartService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class ShopCartController {
    @Autowired
    private ShopCartService shopCartService;
    /**
     * 分页查询购店车
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数params
     * @return
     */
    @GetMapping(value = "v1/auth/shop-cart/paginations")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params){
        return shopCartService.pageList(pageNum,pageSize,params);
    }
}
