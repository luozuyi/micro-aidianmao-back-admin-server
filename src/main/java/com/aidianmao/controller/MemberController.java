package com.aidianmao.controller;

import com.aidianmao.entity.Member;
import com.aidianmao.entity.MemberFundAdjust;
import com.aidianmao.service.MemberService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Map;

@RestController
public class MemberController {
    @Autowired
    private MemberService memberService;

    /**
     * 分页条件查询
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数
     * @return
     */
    @GetMapping(value = "v1/auth/members/pagination")
    public Result listMember(Integer pageNum, Integer pageSize,@RequestParam Map<String,Object> params){
        return memberService.pageList(pageNum,pageSize,params);
    }

    /**
     * 客户公共池分页条件查询(销售)(elasticsearch做缓存)
     * @param member
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping(value = "v1/auth/member/pagination")
    public Result pageMember(Member member,Integer pageNum, Integer pageSize){
        return memberService.listCustomer(member,pageNum,pageSize);
    }

    /**
     * 修改会员信息
     * @param member  会员对象
     * @return
     */
    @PatchMapping(value = "v1/auth/members")
    public Result update(Member member){
        return memberService.update(member);
    }


    /**
     * 查看会员收支明细
     * @param
     * @return
     */
    @GetMapping(value = "v1/auth/member/memberId")
    public Result detailIncomeExpenses(String id,Integer pageNum, Integer pageSize,@RequestParam Map<String,Object> params) {
        return memberService.detailIncomeExpenses(id,pageNum,pageSize,params);
    }

    /**
     * 资金调整
     * @param member             会员实体
     * @param adjustMoney        调整后的资金
     * @param note                调整说明
     * @param aidianmaoAdminToken  凭据
     * @return
     */

    @PostMapping(value = "v1/auth/members")
    public Result adjustFund(Member member,BigDecimal adjustMoney,String note,@CookieValue String aidianmaoAdminToken){
        return memberService.fundAdjust(member,adjustMoney,note,aidianmaoAdminToken);
    }

    /**
     * 会员详情
     * @param id  主键id
     * @return
     */
    @GetMapping(value = "v1/auth/members/id")
    public Result detail(String id){
        return memberService.detailMember(id);
    }

    /**
     *分页条件查询公共客户池
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数
     * @return
     */
    @GetMapping(value = "v1/auth/customer/paginations")
    public Result pageListCustomer(Integer pageNum, Integer pageSize,@RequestParam Map<String,Object> params){
        return memberService.pageListCustomer(pageNum,pageSize,params);
    }

    /**
     * 客户详情
     * @param memberId  客户id
     * @return
     */
    @GetMapping(value = "v1/auth/customer/details")
    public Result detailCustomer(String memberId){
        return memberService.detailCustomer(memberId);
    }

    /**
     * 修改客户信息
     * @param record 实体
     * @param content 备注内容
     * @param aidianmaoAdminToken  凭据
     * @return
     */
    @PatchMapping(value = "v1/auth/customer/ids")
    public Result updateCustomer(Member record,String content,@CookieValue String aidianmaoAdminToken){
        return memberService.updateCustomer(record,content,aidianmaoAdminToken);
    }

    /**
     *分页条件查询最近一周登录客户
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数
     * @return
     */
    @GetMapping(value = "v1/auth/customer-week/paginations")
    public Result pageListCustomerWeek(Integer pageNum, Integer pageSize,@RequestParam Map<String,Object> params){
        return memberService.pageListCustomerWeek(pageNum,pageSize,params);
    }
}
