package com.aidianmao.controller;

import com.aidianmao.service.ChannelArticleTemplateService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class ChannelArticleTemplateController {
    @Autowired
    private ChannelArticleTemplateService channelArticleTemplateService;

    /**
     * 动态分页条件查询模板
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/channel-article-templates/pagination/selection")
    public Result listPageBySelect(Integer pageNum,Integer pageSize,@RequestParam Map<String, Object> params) {
        return channelArticleTemplateService.listPageBySelect(pageNum, pageSize, params);
    }

    /**
     * 添加模板
     * @param tplUrl 模板路径名称
     * @param type 模板类型 0：文章模板 1：栏目模板
     * @return
     */
    @PostMapping(value = "v1/auth/channel-article-templates")
    public Result addChannelArticleTemplate(String tplUrl, String type){
        return channelArticleTemplateService.addChannelArticleTemplate(tplUrl, type);
    }

    /**
     * 删除模板
     * @param id 主键id
     * @return
     */
    @DeleteMapping(value = "v1/auth/channel-article-templates")
    public Result delChannelArticleTemplate(String id) {
        return channelArticleTemplateService.delChannelArticleTemplate(id);
    }

    /**
     * 获取栏目模板列表
     * @return
     */
    @GetMapping(value = "v1/auth/channel-article-templates/channel-template")
    public Result getChannelTemplateList(){
        return channelArticleTemplateService.getChannelTemplateList();
    }

    /**
     * 获取文章模板列表
     * @return
     */
    @GetMapping(value = "v1/auth/channel-article-templates/article-template")
    public Result getArtilceTemplateList(){
        return channelArticleTemplateService.getArticleTemplateList();
    }
}
