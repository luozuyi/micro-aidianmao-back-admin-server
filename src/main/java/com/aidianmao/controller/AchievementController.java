package com.aidianmao.controller;

import com.aidianmao.entity.Achievement;
import com.aidianmao.service.AchievementService;
import com.aidianmao.utils.Result;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class AchievementController {
    @Autowired
    private AchievementService achievementService;
    /**
     * 分页条件查询业绩申报
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 条件查询的参数
     * @return
     */
    @GetMapping(value = "v1/auth/achievement/paginations")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return achievementService.pageList(pageNum,pageSize,params);
    }
    /**
     * 业绩申报详情
     * @param id 主键id
     * @return
     */
    @GetMapping(value = "v1/auth/achievement/details")
    public Result detail(String id) {
        return achievementService.detail(id);
    }

    /**
     * 业绩申报修改
     * @param record 参数实体
     * @param note   审核结果
     * @param checkType 审核类型
     * @param checkResult 审核结果
     * @param aidianmaoAdminToken 凭据
     * @return
     */
    @PatchMapping(value = "v1/auth/achievement/contents")
    public Result update(Achievement record,String note, Integer checkType,Integer checkResult,@CookieValue String aidianmaoAdminToken) {
        return achievementService.update(record,note,checkType,checkResult,aidianmaoAdminToken);
    }
    /**
     * 业绩申报新增
     * @param record 参数实体
     * @param aidianmaoAdminToken  凭据
     * @return
     */
    @PostMapping(value = "v1/auth/achievement/informations")
    public Result insert(Achievement record,String buyerAdminName,String sellerAchieveName,@CookieValue String aidianmaoAdminToken) {
        return achievementService.insert(record,buyerAdminName,sellerAchieveName,aidianmaoAdminToken);
    }

    /**
     * 作废
     * @param id  主键id
     * @return
     */
    @PatchMapping(value = "v1/auth/achievement/id")
    public Result cancellation(String id){
        return achievementService.cancellation(id);
    }

    /**
     * 审批
     * @param id      主键id
     * @param checkType   审核结果 0:不通过  1: 通过
     * @param note          备注
     * @param aidianmaoAdminToken  凭据
     * @return
     */
    @PatchMapping(value = "v1/auth/achievement/check")
     public  Result checkAchieve(String id,String checkType,String note,@CookieValue String aidianmaoAdminToken){
        return achievementService.checkAchieve(id,checkType,note,aidianmaoAdminToken);
    }

    /**
     * 经理核实
     * @param id         主键id
     * @param checkType  核实结果 0:不通过 1:通过
     * @param note        备注
     * @return
     */
    @PatchMapping(value = "v1/auth/achievement/verify")
     public Result verifyAchieve(String id,String checkType,String note) {
        return achievementService.verifyAchieve(id, checkType, note);
    }

    /**
     * 修改审核
     * @param id
     * @param checkType  核实结果 0:不通过 1:通过
     * @return
     */
    @PatchMapping(value = "v1/auth/achievement/update")
    public Result updateAchieve(String id,String checkType){
        return achievementService.updateAchieve(id,checkType);
    }

}
