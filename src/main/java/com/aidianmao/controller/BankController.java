package com.aidianmao.controller;

import com.aidianmao.entity.Bank;
import com.aidianmao.service.BankService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class BankController {
    @Autowired
    private BankService bankService;
    /**
     * 分页查询银行信息
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/bank/paginations")
    public Result pageList(Integer pageNum, Integer pageSize) {
        return bankService.pageList(pageNum,pageSize);
    }
    /**
     * 银行信息详情
     * @param id 主键id
     * @return
     */
    @GetMapping(value = "v1/auth/bank/details")
    public Result detail(String id) {
        return bankService.detail(id);
    }
    /**
     * 银行信息更新
     * @param record 银行信息实体
     * @return
     */
    @PatchMapping(value = "v1/auth/bank/contents")
    public Result update(Bank record) {
        return bankService.update(record);
    }
    /**
     * 银行信息新增
     * @param record 银行信息实体
     * @return
     */
    @PostMapping(value = "v1/auth/bank/informations")
    public Result insert(Bank record) {
        return bankService.insert(record);
    }
    /**
     * 上传银行logo
     * @param file 文件
     * @return
     */
    //还没有搭建静态资源服务器接口暂时不能使用
    @PostMapping(value = "v1/auth/bank/logos")
    public Result addBankLogo(MultipartFile file){
        return bankService.addBankLogo(file);
    }
}
