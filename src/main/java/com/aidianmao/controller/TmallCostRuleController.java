package com.aidianmao.controller;

import com.aidianmao.entity.TmallCostRule;
import com.aidianmao.service.TmallCostRuleService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class TmallCostRuleController {
    @Autowired
    private TmallCostRuleService tmallCostRuleService;
    /**
     * 分页查询天猫店铺费用规则
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-cost-rules/paginations")
    public Result pageList(Integer pageNum, Integer pageSize) {
        return tmallCostRuleService.pageList(pageNum,pageSize);
    }
    /**
     * 店铺费用规则详情
     * @param id 主键ID
     * @return
     */
    /*@GetMapping(value = "v1/auth/tmall-cost-rules/details")
    public Result detail(String id) {
        return tmallCostRuleService.detail(id);
    }*/
    /**
     * 店铺费用规则删除
     * @param id 主键ID
     * @return
     */
    /*@DeleteMapping(value = "v1/auth/tmall-cost-rules/ids")
    public Result delete(String id) {
        return tmallCostRuleService.delete(id);
    }*/
    /**
     * 店铺费用规则更新
     * @param record 店铺费用规则实体
     * @return
     */
    /*@PatchMapping(value = "v1/auth/tmall-cost-rules/contents")
    public Result update(TmallCostRule record) {
        return tmallCostRuleService.update(record);
    }*/
    /**
     * 店铺费用规则新增
     * @param record 店铺费用规则实体
     * @return
     */
    @PostMapping(value = "v1/auth/tmall-cost-rules/informations")
    public Result insert(TmallCostRule record) {
        return tmallCostRuleService.insert(record);
    }
}
