package com.aidianmao.controller;

import com.aidianmao.service.TmallFastCommitService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class TmallFastCommitController {

    @Autowired
    private TmallFastCommitService tmallFastCommitService;

    /**
     * 快速挂店分页条件查询
     * @param pageNum   当前页
     * @param pageSize  一页显示多少条
     * @param params    条件参数
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-fast-commits/pagination")
    public Result listPage(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return tmallFastCommitService.listPageBySelection(pageNum, pageSize, params);
    }
}
