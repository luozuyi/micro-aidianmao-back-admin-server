package com.aidianmao.controller;

import com.aidianmao.entity.GroupTarget;
import com.aidianmao.service.GroupTargetService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class GroupTargetController {
    @Autowired
    private GroupTargetService groupTargetService;
    /**
     * 分页条件查询小组月目标
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 条件查询的参数
     * @return
     */
    @GetMapping(value = "v1/auth/group-target/paginations")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return groupTargetService.pageList(pageNum,pageSize,params);
    }
    /**
     * 小组月目标详情
     * @param id 主键id
     * @return
     */
    @GetMapping(value = "v1/auth/group-target/details")
    public Result detail(String id) {
        return groupTargetService.detail(id);
    }
    /**
     * 小组月目标修改
     * @param record 参数实体
     * @return
     */
    @PatchMapping(value = "v1/auth/group-target/contents")
    public Result update(GroupTarget record) {
        return groupTargetService.update(record);
    }
    /**
     * 小组月目标删除
     * @param id 主键id
     * @return
     */
    @DeleteMapping(value = "v1/auth/group-target/ids")
    public Result delete(String id) {
        return groupTargetService.delete(id);
    }
    /**
     * 小组月目标新增
     * @param record 参数实体
     * @return
     */
    @PostMapping(value = "v1/auth/group-target/informations")
    public Result insert(GroupTarget record,String groupName) {
        return groupTargetService.insert(record,groupName);
    }
}
