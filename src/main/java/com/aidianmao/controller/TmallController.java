package com.aidianmao.controller;

import com.aidianmao.entity.ProvincesCitysCountrys;
import com.aidianmao.entity.Tmall;
import com.aidianmao.service.TmallService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@RestController
public class TmallController {
    @Autowired
    private TmallService tmallService;

    /**
     * 修改天猫店铺
     * @param tmall 参数实体
     * @param provincesCitysCountrys 省市区参数实体
     * @param aidianmaoAdminToken 凭据
     * @return
     */
    @PatchMapping(value = "v1/auth/tmalls")
    public Result update(Tmall tmall, ProvincesCitysCountrys provincesCitysCountrys,@CookieValue String aidianmaoAdminToken ) {
        return tmallService.update(tmall,provincesCitysCountrys,aidianmaoAdminToken);
    }

    /**
     * 修改天猫店铺费率规则
     * @param id 天猫店铺主键ID
     * @return
     */
    @PatchMapping(value = "v1/auth/tmalls/rules")
    public Result updateRules(String id,String num) {
        return tmallService.updateRules(id,num);
    }

    /**
     * 主键查询店铺详情
     * @param tmallId 天猫id
     * @return
     */
    @GetMapping(value = "v1/auth/tmalls/id")
    public Result detail(String tmallId) {
        return tmallService.detail(tmallId);
    }

    /**
     * 分页查询天猫店铺
     * @param pageNum  当前页
     * @param pageSize  一页显示多少条
     * @param params     参数map
     * @return
     */
    @GetMapping(value = "v1/auth/tmalls/pagination")
    public Result pageList(Integer pageNum, Integer pageSize,@RequestParam Map<String, Object> params) {
        return tmallService.pageList(pageNum,pageSize,params);
    }

    /**
     * 上传天猫店铺图片
     * @param file  文件
     * @return
     */
    @PostMapping(value = "v1/auth/tmalls/picture")
    public Result addTmallPicture(MultipartFile file){
     return tmallService.addTmallPicture(file);
    }

    /**
     * 店铺详情
     * @param tmallId 天猫id
     * @return
     */
    @GetMapping(value = "v1/auth/tmalls/TmallId")
    public Result detailTmall(String tmallId) {
        return tmallService.detailTmall(tmallId);
    }
}
