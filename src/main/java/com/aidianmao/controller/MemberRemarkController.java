package com.aidianmao.controller;

import com.aidianmao.service.MemberRemarkService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MemberRemarkController {
    @Autowired
    private MemberRemarkService memberRemarkService;

    @GetMapping(value = "v1/auth/member-remark")
    public Result pageList(String memberId){
        return memberRemarkService.pageList(memberId);
    }



    /**
     * 销售添加会员备注信息
     * @param content 备注内容
     * @param memberId 会员ID
     * @param aidianmaoAdminToken 凭据
     * @return
     */
    @PostMapping(value = "v1/auth/member-remark/informations")
    Result insert(String content, String memberId,@CookieValue String aidianmaoAdminToken) {
        return memberRemarkService.insert(content,memberId,aidianmaoAdminToken);
    }

    /**
     * 备注详情
     * @param id
     * @return
     */
    @GetMapping(value = "v1/auth/member-remark/id")
   public Result detail(String id){
        return memberRemarkService.detail(id);
   }

}
