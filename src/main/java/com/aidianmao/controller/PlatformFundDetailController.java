package com.aidianmao.controller;

import com.aidianmao.service.PlatformFundDetailService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class PlatformFundDetailController {
    @Autowired
    private PlatformFundDetailService platformFundDetailService;

    /**
     * 分页查询平台收支明细
     * @param pageNum  当前页
     * @param pageSize  一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/platform-fund-details/pagination")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return platformFundDetailService.pageList(pageNum,pageSize,params);
    }
    /**
     * 时间段查询平台收支明细与公司总收入
     * @param pageNum  当前页
     * @param pageSize  一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/platform-fund-details/income-pagination")
    public Result pageListTotalIncome(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return platformFundDetailService.pageListTotalIncome(pageNum,pageSize,params);
    }
}
