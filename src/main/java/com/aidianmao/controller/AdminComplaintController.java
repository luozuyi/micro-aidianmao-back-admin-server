package com.aidianmao.controller;

import com.aidianmao.service.AdminComplaintService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class AdminComplaintController {
    @Autowired
    private AdminComplaintService adminComplaintService;
    /**
     * 分页条件查询业务员投诉管理信息
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 条件查询的参数
     * @return
     */
    @GetMapping(value = "v1/auth/admin-complaint/paginations")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return adminComplaintService.pageList(pageNum,pageSize,params);
    }
    /**
     * 业务员投诉管理信息详情
     * @param id 主键id
     * @return
     */
    @GetMapping(value = "v1/auth/admin-complaint/details")
    public Result detail(String id) {
        return adminComplaintService.detail(id);
    }
}
