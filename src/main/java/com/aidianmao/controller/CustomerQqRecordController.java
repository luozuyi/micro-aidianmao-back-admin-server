package com.aidianmao.controller;

import com.aidianmao.service.CustomerQqRecordService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class CustomerQqRecordController {

    @Autowired
    private CustomerQqRecordService customerQqRecordService;

    /**
     * qq顾客记录分页条件查询
     * @param pageNum    当前页
     * @param pageSize   一页显示多少条
     * @param params     条件参数
     * @return
     */
    @GetMapping(value = "v1/auth/customer-qq-record/pagination")
    public Result pageList(Integer pageNum, Integer pageSize,@RequestParam Map<String, Object> params){
        return customerQqRecordService.pageList(pageNum,pageSize,params);
    }



}
