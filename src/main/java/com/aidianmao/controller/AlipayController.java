package com.aidianmao.controller;

import com.aidianmao.entity.Alipay;
import com.aidianmao.service.AlipayService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class AlipayController {
    @Autowired
    private AlipayService alipayService;
    /**
     * 分页条件查询支付宝充值信息
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 条件查询的参数
     * @return
     */
    @GetMapping(value = "v1/auth/alipay/paginations")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return alipayService.pageList(pageNum,pageSize,params);
    }
    /**
     * 支付宝充值信息详情
     * @param id 主键id
     * @return
     */
    @GetMapping(value = "v1/auth/alipay/details")
    public Result detail(String id) {
        return alipayService.detail(id);
    }

    /**
     * 支付宝充值信息修改
     * @param record 参数Alipay实体
     * @param checkResult 审核结果
     * @param aidianmaoAdminToken 凭据
     * @return
     */
    @PatchMapping(value = "v1/auth/alipay/contents")
    public Result update(Alipay record,Integer checkResult,@CookieValue String aidianmaoAdminToken) {
        return alipayService.update(record,checkResult,aidianmaoAdminToken);
    }
}
