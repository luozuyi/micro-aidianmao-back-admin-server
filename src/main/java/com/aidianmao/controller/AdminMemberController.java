package com.aidianmao.controller;

import com.aidianmao.entity.AdminMember;
import com.aidianmao.service.AdminMemberService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class AdminMemberController {
    @Autowired
    private AdminMemberService adminMemberService;
    /**
     * 分页条件查询已被添加的客户信息
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/admin-member/paginations")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return adminMemberService.pageList(pageNum,pageSize,params);
    }

    /**
     * 分页条件查询我的客户信息
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @param aidianmaoAdminToken 凭据
     * @return
     */
    @GetMapping(value = "v1/auth/admin-member/admin-paginations")
    public Result pageListByAdmin(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params,@CookieValue String aidianmaoAdminToken) {
        return adminMemberService.pageListByAdmin(pageNum,pageSize,params,aidianmaoAdminToken);
    }
    /**
     * 我的客户信息删除
     * @param id 主键id
     * @param content 备注内容
     * @param memberId 会员ID
     * @param operationType 操作类型
     * @param aidianmaoAdminToken 凭据
     * @return
     */
    @DeleteMapping(value = "v1/auth/admin-member/ids")
    public Result delete(String id,String content,String memberId,Integer operationType,@CookieValue String aidianmaoAdminToken) {
        return adminMemberService.delete(id,content,memberId,operationType,aidianmaoAdminToken);
    }
    /**
     * 会员销售关联信息新增
     * @param record 参数实体
     * @param memberId 会员ID
     * @param operationType 操作类型
     * @param aidianmaoAdminToken   凭据
     * @return
     */
    @PostMapping(value = "v1/auth/admin-member/informations")
    public Result insert(AdminMember record,String memberId,Integer operationType,@CookieValue String aidianmaoAdminToken) {
        return adminMemberService.insert(record,memberId,operationType,aidianmaoAdminToken);
    }


    /**
     * 公共客户池
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/admin-members/paginations")
    public Result member(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return adminMemberService.selectMember(pageNum,pageSize,params);
    }

    /**
     * 加入我的客户组
     * @param id              会员id
     * @param aidianmaoAdminToken
     * @return
     */
    @PostMapping(value = "v1/auth/admin-members")
    public Result addMember(String id, @CookieValue String aidianmaoAdminToken){
        return adminMemberService.addMember(id,aidianmaoAdminToken);
    }

    /**
     * 添加会员
     * @param phone  会员手机号
     * @return
     */
    @PostMapping(value = "v1/auth/members/phone")
    public Result insertMember(String phone){
        return adminMemberService.insertMember(phone);
    }

    /**
     * 所有客户信息
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数
     * @return
     */
    @GetMapping(value = "v1/auth/admin-members/paginations0")
    public Result Adminmember(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return adminMemberService.selectAdminMember(pageNum,pageSize,params);
    }

    /**
     * 移到公共池
     * @param ids  id数组
     * @return
     */
    @DeleteMapping(value = "v1/auth/admin-members/ids")
    public Result deleteMany(@RequestParam("ids") String ids){
        return adminMemberService.deleteMany(ids);
    }

    /**
     * 我的客户信息
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/admin-members/sell/paginations")
    public Result SellAdminmember(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params,@CookieValue String aidianmaoAdminToken) {
        return adminMemberService.selectSellAdminAll(pageNum,pageSize,params,aidianmaoAdminToken);
    }

    /**
     * 我的客户信息详情
     * @param id  主键id
     * @return
     */
    @GetMapping(value =  "v1/auth/admin-members/sell/id")
    public Result selectSellId(String id){
        return adminMemberService.selectSellById(id);
    }

    /**
     * 修改我的客户信息
     * @param id                  主键id
     * @param identity            会员身份
     * @param importanceDegree    会员等级
     * @return
     */
    @PatchMapping(value = "v1/auth/admin-members/sell/id")
    public Result updateAdminMember(String id,String identity,String importanceDegree){
        return adminMemberService.updateAdminMember(id,identity,importanceDegree);
    }

    /**
     * 我的客户-添加备注
     * @param id                主键id
     * @param content         备注信息
     * @param aidianmaoAdminToken  凭据
     * @return
     */
    @PostMapping(value = "v1/auth/member-remark")
    public Result insertContent(String id,String content,@CookieValue String aidianmaoAdminToken){
        return adminMemberService.insertContent(id,content,aidianmaoAdminToken);
    }

    /**
     * 移除我的客户
     * @param id     主键id
     * @return
     */
    @DeleteMapping(value = "v1/auth/admin-member")
    public Result delete(String id){
        return adminMemberService.delete(id);
    }
}
