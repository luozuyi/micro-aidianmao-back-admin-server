package com.aidianmao.controller;

import com.aidianmao.service.HomePageService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class HomePageController {

    @Autowired
    private HomePageService homePageService;

    /**
     * 运营数据
     * @return
     */
    @GetMapping(value = "v1/auth/home-page/data")
    public Result page(){
        return homePageService.operationalData();
    }

    /**
     * 财务数据
     * @param params
     * @return
     */
    @GetMapping(value = "v1/auth/home-page/finance")
    public Result finance(Map<String,Object> params){
        return homePageService.finance(params);
    }

}
