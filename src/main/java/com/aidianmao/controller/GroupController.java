package com.aidianmao.controller;

import com.aidianmao.entity.Group;
import com.aidianmao.service.GroupService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GroupController {
    @Autowired
    private GroupService groupService;

    /**
     * 添加小组
     * @param group 小组对象
     * @return
     */
    @PostMapping(value = "v1/auth/groups")
    public Result addGroup(Group group) {
        return groupService.addGroup(group);
    }

    /**
     * 分页查询小组
     * @param pageSize 当前页
     * @param pageNum 一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/groups/pagination")
    public Result pageList(Integer pageSize, Integer pageNum) {
        return groupService.pageList(pageSize,pageNum);
    }

    /**
     * 查询小组组长
     * @return
     */
    @GetMapping(value = "v1/auth/groups/leaderName")
    public Result pageListLeader() {
        return groupService.pageListLeader();
    }
}
