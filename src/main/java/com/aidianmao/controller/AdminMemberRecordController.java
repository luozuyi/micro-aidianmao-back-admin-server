package com.aidianmao.controller;

import com.aidianmao.service.AdminMemberRecordService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class AdminMemberRecordController {
    @Autowired
    private AdminMemberRecordService adminMemberRecordService;
    /**
     * 分页条件销售操作会员记录
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/admin-member-record/paginations")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return adminMemberRecordService.pageList(pageNum,pageSize,params);
    }
}
