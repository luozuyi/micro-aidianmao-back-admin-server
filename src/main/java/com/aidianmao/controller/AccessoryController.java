package com.aidianmao.controller;

import com.aidianmao.service.AccessoryService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

@RestController
public class AccessoryController {
    @Autowired
    private AccessoryService accessoryService;

    /**
     * 上传文章封面
     * @param file 文件
     * @return
     */
    @PostMapping(value = "v1/auth/accessorys")
    public Result addArticleCover(MultipartFile file){
        return accessoryService.addArticleCover(file);
    }

    /**
     * 下载文件
     * @param accessoryId 文件id
     * @param response 响应
     * @return
     */
    @GetMapping("v1/auth/accessorys/download")
    public Result downloads(String accessoryId, HttpServletResponse response){
        return accessoryService.downloads(accessoryId, response);

    }
}
