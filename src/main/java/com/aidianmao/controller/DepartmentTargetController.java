package com.aidianmao.controller;

import com.aidianmao.entity.DepartmentTarget;
import com.aidianmao.service.DepartmentTargetService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class DepartmentTargetController {
    @Autowired
    private DepartmentTargetService departmentTargetService;
    /**
     * 分页条件查询部门月目标
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 条件查询的参数
     * @return
     */
    @GetMapping(value = "v1/auth/department-target/paginations")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return departmentTargetService.pageList(pageNum,pageSize,params);
    }
    /**
     * 部门月目标详情
     * @param id 主键id
     * @return
     */
    @GetMapping(value = "v1/auth/department-target/details")
    public Result detail(String id) {
        return departmentTargetService.detail(id);
    }
    /**
     * 部门月目标修改(支持修改月目标和月业绩)
     * @param record 参数实体
     * @return
     */
    @PatchMapping(value = "v1/auth/department-target/contents")
    public Result update(DepartmentTarget record) {
        return departmentTargetService.update(record);
    }
    /**
     * 部门月目标删除
     * @param id 主键id
     * @return
     */
    @DeleteMapping(value = "v1/auth/department-target/ids")
    public Result delete(String id) {
        return departmentTargetService.delete(id);
    }
    /**
     * 部门月目标新增
     * @param record 参数实体
     * @return
     */
    @PostMapping(value = "v1/auth/department-target/informations")
    public Result insert(DepartmentTarget record,String departName) {
        return departmentTargetService.insert(record,departName);
    }
}
