package com.aidianmao.controller;

import com.aidianmao.entity.Channel;
import com.aidianmao.service.ChannelService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class ChannelController {
    @Autowired
    private ChannelService channelService;

    /**
     * 动态分页条件查询栏目
     * @param pageNum   当前页
     * @param pageSize  一页显示多少条
     * @param params    条件参数
     * @return
     */
    @GetMapping(value = "v1/auth/channels/pagination")
    public Result listPage(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return channelService.listPageBySelection(pageNum, pageSize, params);
    }

    /**
     * 添加栏目
     * @param channel 参数实体
     * @return
     */
    @PostMapping(value = "v1/auth/channels")
    public Result addChannel(Channel channel) {
        return channelService.addChannel(channel);
    }

    /**
     * 删除栏目
     * @param id 主键id
     * @return
     */
    @DeleteMapping(value = "v1/auth/channels")
    public Result delChannel(String id){ return channelService.delChannel(id); }

    /**
     * 栏目模板列表
     * @return
     */
    @GetMapping(value = "v1/auth/channels")
    public Result getList(){
        return channelService.getList();
    }


    /**
     * 获取父栏目列表
     * @return
     */
    @GetMapping(value = "v1/auth/channels/parent")
    public Result getListByParent(){
        return channelService.getParentChannel();
    }
}
