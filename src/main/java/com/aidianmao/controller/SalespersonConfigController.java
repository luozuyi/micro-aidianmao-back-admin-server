package com.aidianmao.controller;

import com.aidianmao.service.SalespersonConfigService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Map;

@RestController
public class SalespersonConfigController {
     @Autowired
     private SalespersonConfigService salespersonConfigService;

    /**
     * 业务员分页条件查询
     * @param pageNum
     * @param pageSize
     * @param params
     * @return
     */
    @GetMapping(value = "v1/auth/salesperson-config/paginations")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params,String adminId) {
        return salespersonConfigService.pageList(pageNum,pageSize,params,adminId);
    }

    /**
     * 业务员分组
     * @param groupName
     * @param list
     * @return
     */
    @PatchMapping(value = "v1/auth/salesperson-config/ids")
    public Result divideGroup(String groupName,@RequestParam(value = "ids[]") String[] list){
        return salespersonConfigService.divideGroup(groupName,list);
    }

    /**
     * 修改业务员信息
     * @param id            主键id
     * @param maximunNum   最大客户数
     * @param roleName     级别名称
     * @return
     */
    @PatchMapping(value = "v1/auth/salesperson-config")
    public Result update(String id, Integer maximunNum, String roleName){
        return salespersonConfigService.update(id,maximunNum,roleName);
    }

    /**
     * 分部门
     * @param id
     * @param departName
     * @return
     */
    @PatchMapping(value = "v1/auth/salesperson-config/depart")
    public Result update(String id,String departName){
        return salespersonConfigService.update(id,departName);
    }

    /**
     * 详情
     * @param id  主键id
     * @return
     */
    @GetMapping(value = "v1/auth/salesperson-config/id")
    public Result detail(String id){
        return salespersonConfigService.detail(id);
    }


}
