package com.aidianmao.controller;

import com.aidianmao.service.CustomerServiceQqService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerServiceQqController {

    @Autowired
    private CustomerServiceQqService customerServiceQqService;

    /**
     * 客服分配列表
     * @param type   服务qq组类型(1.天猫特服 2.代记账特服 3.出售网店 4.代入驻咨询 5.售后及投诉)
     * @return
     */
    @GetMapping(value = "v1/auth/customer-service-qq/type")
    public Result selectNameByType(String type){
        return customerServiceQqService.selectNameByType(type);
    }

    /**
     * 分配客服人员
     * @param list    人员列表
     * @return
     */
    @PatchMapping(value = "v1/auth/customer-service-qq/status")
    public Result update(@RequestParam(value = "ids[]") String list[]){
        return customerServiceQqService.update(list);
    }

}
