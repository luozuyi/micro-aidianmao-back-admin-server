package com.aidianmao.controller;

import com.aidianmao.entity.MemberFundAdjust;
import com.aidianmao.service.MemberFundAdjustService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class MemberFundAdjustController {
    @Autowired
    private MemberFundAdjustService memberFundAdjustService;
    /**
     * 分页条件查询会员资金调整记录
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/member-fund-adjust/paginations")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return memberFundAdjustService.pageList(pageNum,pageSize,params);
    }

    /**
     * 添加会员资金调整记录
     * @param fundAdjust        资金调整实体
     * @param phone             会员名
     * @param aidianmaoAdminToken  凭据
     * @return
     */
    @PostMapping(value = "v1/auth/member-fund-adjust/informations")
    public Result insert(MemberFundAdjust fundAdjust, String phone, @CookieValue String aidianmaoAdminToken) {
        return memberFundAdjustService.insertFundAdjust(fundAdjust,phone,aidianmaoAdminToken);
    }

}
