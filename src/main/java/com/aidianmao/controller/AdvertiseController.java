package com.aidianmao.controller;

import com.aidianmao.entity.Advertise;
import com.aidianmao.service.AdvertiseService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class AdvertiseController {

    @Autowired
    private AdvertiseService advertiseService;

    /**
     * 广告分页条件查询
     * @param pageNum     当前页
     * @param pageSize    一页显示多少条
     * @param params      凭据
     * @return
     */
    @GetMapping(value = "v1/auth/advertises/paginations")
    public Result advertiseList(Integer pageNum, Integer pageSize,@RequestParam Map<String,Object> params){
        return advertiseService.pageListAdvertise(pageNum,pageSize,params);
    }

    /**
     * 禁用
     * @param id                    主键id
     * @param aidianmaoAdminToken  凭据
     * @return
     */
    @PatchMapping(value = "v1/auth/advertises/del")
    public Result updateDel(String id,@CookieValue String aidianmaoAdminToken){
        return advertiseService.updateDel(id,aidianmaoAdminToken);
    }

    /**
     * 启用
     * @param id                    主键id
     * @param aidianmaoAdminToken  凭据
     * @return
     */
    @PatchMapping(value = "v1/auth/advertises/delFlag")
    public Result updateDelFlag(String id,@CookieValue String aidianmaoAdminToken){
        return advertiseService.updateDelFlag(id,aidianmaoAdminToken);
    }

    /**
     * 修改广告内容
     * @param advertise              广告实体
     * @param aidianmaoAdminToken  凭据
     * @return
     */
    @PatchMapping(value = "v1/auth/advertise")
    public Result updateAdvertise(Advertise advertise,@CookieValue String aidianmaoAdminToken){
        return advertiseService.updateAdvertise(advertise,aidianmaoAdminToken);
    }

    /**
     * 删除广告
     * @param id   主键id
     * @return
     */

    @DeleteMapping(value = "v1/auth/advertise/id")
    public Result deleteAdvertise(String id){
        return advertiseService.deleteAdvertise(id);
    }

    /**
     * 添加广告
     * @param advertise        广告实体
     * @param aidianmaoAdminToken   凭据
     * @return
     */
    @PostMapping(value = "v1/auth/advertise")
    public Result addAdvertise(Advertise advertise,@CookieValue String aidianmaoAdminToken){
        return advertiseService.addAdvertise(advertise,aidianmaoAdminToken);
    }

    /**
     * 广告详情
     * @param id  主键id
     * @return
     */
    @GetMapping(value = "v1/auth/advertise/id")
    public Result detail(String id){
        return advertiseService.detail(id);
    }

}
