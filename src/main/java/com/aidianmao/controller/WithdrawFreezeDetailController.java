package com.aidianmao.controller;

import com.aidianmao.service.WithdrawFreezeDetailService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class WithdrawFreezeDetailController {
    @Autowired
    private WithdrawFreezeDetailService withdrawFreezeDetailService;
    /**
     * 分页条件提现查询冻结资金记录
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 条件查询的参数
     * @return
     */
    @GetMapping(value = "v1/auth/withdraw-freeze-detail/paginations")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return withdrawFreezeDetailService.pageList(pageNum,pageSize,params);
    }
}
