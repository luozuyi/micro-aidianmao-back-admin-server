package com.aidianmao.controller;

import com.aidianmao.entity.OnlineQuestionAnswer;
import com.aidianmao.service.OnlineQuestionAnswerService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class OnlineQuestionAnswerController {

    @Autowired
    private OnlineQuestionAnswerService onlineQuestionAnswerService;

    /**
     * 提问管理分页条件查询列表
     * @param pageNum     当前页
     * @param pageSize    一页显示多少条
     * @param params      条件参数
     * @return
     */
    @GetMapping(value = "v1/auth/online-question-answers/pagination")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params){
        return onlineQuestionAnswerService.pageList(pageNum,pageSize,params);
    }

    /**
     * 添加提问信息（我要提问）
     * @param onlineQuestionAnswer  提问对象
     * @return
     */
    @PostMapping(value = "v1/auth/online-question-answers")
    public Result insertQuestion(OnlineQuestionAnswer onlineQuestionAnswer,@CookieValue String aidianmaoAdminToken){
        return onlineQuestionAnswerService.insert(onlineQuestionAnswer,aidianmaoAdminToken);
    }

    /**
     * 修改提问信息
     * @param onlineQuestionAnswer  提问对象
     * @return
     */
    @PatchMapping("v1/auth/online-question-answers")
    public Result updateQuestion(OnlineQuestionAnswer onlineQuestionAnswer,@CookieValue String aidianmaoAdminToken){
        return onlineQuestionAnswerService.update(onlineQuestionAnswer,aidianmaoAdminToken);
    }
    /**
     * 删除
     * @param id  主键id
     * @return
     */
    @PatchMapping("v1/auth/online-question-answers/id")
    public Result deleteQuestion(String id,@CookieValue String aidianmaoAdminToken){
        return onlineQuestionAnswerService.deleteQuestion(id,aidianmaoAdminToken);
    }

    /**
     * 提问详情
     * @param id  主键id
     * @return
     */
    @GetMapping(value = "v1/auth/online-question-answers/id")
    public Result detail(String id){
        return onlineQuestionAnswerService.detail(id);
    }
}
