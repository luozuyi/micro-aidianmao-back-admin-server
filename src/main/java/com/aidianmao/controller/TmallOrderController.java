package com.aidianmao.controller;

import com.aidianmao.esEntity.EsTmallOrder;
import com.aidianmao.service.TmallOrderService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class TmallOrderController {
    @Autowired
    private TmallOrderService tmallOrderService;

    /**
     * 分页条件查询订单
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-orders/pagination")
    public Result pageList(String pageNum, String pageSize, EsTmallOrder esTmallOrder) {
        return tmallOrderService.pageList(pageNum,pageSize,esTmallOrder);
    }


    /**
     * 查看已取消订单详情
     * @param id 订单ID
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-orders/id4")
    public  Result detailById4(String id){
        return tmallOrderService.statusDetailById(id);
    }

    /**
     * 查看待付款订单详情
     * @param id 订单ID
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-orders/id0")
    public  Result detailById0(String id){
        return tmallOrderService.statusDetailById(id);
    }

    /**
     * 查看支付定金订单详情
     * @param id 订单ID
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-orders/id1")
    public  Result detailById1(String id){
        return tmallOrderService.payDetailById(id);
    }

    /**
     * 查看交易成功订单详情
     * @param id 订单ID
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-orders/id3")
    public  Result detailById3(String id){
        return tmallOrderService.paymentDetailById(id);
    }

    /**
     * 查看异常订单详情（无资金）
     * @param id 订单ID
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-orders/id6")
    public  Result detailById6(String id){
        return tmallOrderService.detailById(id);
    }

    /**
     * 查看已付全款订单详情
     * @param id 订单ID
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-orders/id8")
    public  Result detailById(String id){
        return tmallOrderService.paymentDetailById(id);
    }

    /**
     * 查看异常订单详情（有资金）
     * @param id 订单ID
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-orders/id")
    public  Result detailById7(String id){
        return tmallOrderService.detailById(id);
    }




    /**
     * 待付款订单
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-orders/status0")
    public Result pageListByStatus0(Integer pageNum, Integer pageSize,@RequestParam Map<String, Object> params) {
        return tmallOrderService.pageListByStatus0(pageNum,pageSize,params);
    }

    /**
     * 已经付定金的订单
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-orders/status1")
    public Result pageListByStatus1(Integer pageNum, Integer pageSize,@RequestParam Map<String, Object> params) {
        return tmallOrderService.pageListByStatus1(pageNum,pageSize,params);
    }


    /**
     * 交易成功的订单
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-orders/status3")
    public Result pageListByStatus3(Integer pageNum, Integer pageSize,@RequestParam Map<String, Object> params) {
        return tmallOrderService.pageListByStatus3(pageNum,pageSize,params);
    }

    /**
     * 已取消的订单
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-orders/status4")
    public Result pageListByStatus4(Integer pageNum, Integer pageSize,@RequestParam Map<String, Object> params) {
        return tmallOrderService.pageListByStatus4(pageNum,pageSize,params);
    }

    /**
     * 异常订单(无资金)
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-orders/status6")
    public Result pageListByStatus6(Integer pageNum, Integer pageSize,@RequestParam Map<String, Object> params) {
        return tmallOrderService.pageListByStatus6(pageNum,pageSize,params);
    }

    /**
     * 交接中
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-orders/status8")
    public Result transferStatus(Integer pageNum, Integer pageSize,@RequestParam Map<String, Object> params) {
        return tmallOrderService.transferStatus(pageNum,pageSize,params);
    }

    /**
     * 异常的订单(有资金)
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-orders/fund")
    public Result fundAdjust(Integer pageNum, Integer pageSize,@RequestParam Map<String, Object> params) {
        return tmallOrderService.selectFundAdjustAll(pageNum,pageSize,params);
    }

    /**
     * 交接
     * @param id 主键id
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-orders/transfering")
    public Result transfer(String id,@CookieValue String aidianmaoAdminToken) {
        return tmallOrderService.transfer(id,aidianmaoAdminToken);
    }

    /**
     * 取消交接
     * @param id 主键id
     * @return
     */
    @PatchMapping(value = "v1/auth/tmall-orders/cancelTransfer")
    public Result cancelTransfer(String id) {
        return tmallOrderService.cancelTransfer(id);
    }


    /**
     * 支付定金交易终止
     * @param id                      主键id
     * @param aidianmaoAdminToken    凭据
     * @param responsibility        责任
     * @return
     */
    @PatchMapping(value = "v1/auth/tmall-orders/terminations")
    public Result terminationOrder(String id,@CookieValue String aidianmaoAdminToken,String responsibility){
        return tmallOrderService.terminationOrder(id,aidianmaoAdminToken,responsibility);
    }

    /**
     * 支付全款交易终止
     * @param id                      主键id
     * @param aidianmaoAdminToken    凭据
     * @param responsibility        责任
     * @return
     */
    @PatchMapping(value = "v1/auth/tmall-orders/termination")
    public Result terminationOrders(String id,@CookieValue String aidianmaoAdminToken,String responsibility){
        return tmallOrderService.terminationOrder(id,aidianmaoAdminToken,responsibility);
    }

    /**
     * 放款
     * @param id                          主键id
     * @return
     */
    @PatchMapping(value = "v1/auth/tmall-orders/abnormal")
    public Result abnormalOrder(String id){
        return tmallOrderService.loanOrder(id);
    }


    /**
     * 支付定金卖家待确认
     * @param id                          主键id
     * @return
     */
    @PatchMapping(value = "v1/auth/tmall-orders/saleSule")
    public Result saleSule(String id){
        return tmallOrderService.saleSule(id);
    }

    /**
     * 已付全款卖家待确认
     * @param id                          主键id
     * @return
     */
    @PatchMapping(value = "v1/auth/tmall-orders/saleSules")
    public Result saleSules(String id){
        return tmallOrderService.saleSule(id);
    }


}
