package com.aidianmao.controller;

import com.aidianmao.entity.Article;
import com.aidianmao.service.ArticleService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class ArticleController {
    @Autowired
    private ArticleService articleService;

    /**
     * 动态分页条件查询文章列表
     * @param pageNum   当前页
     * @param pageSize  一页显示多少条
     * @param params    条件参数
     * @return
     */
    @GetMapping(value = "v1/auth/articles/pagination")
    public Result listPageBySelection(Integer pageNum,Integer pageSize,@RequestParam Map<String,Object> params){
        return  articleService.listPageBySelection(pageNum,pageSize,params);
    }

    /**
     * 删除文章
     * @param id 主键id
     * @return
     */
    @DeleteMapping(value = "v1/auth/articles")
    public Result delById(String id) {
        return articleService.delById(id);
    }

    /**
     * 添加文章
     * @param article 文章对象
     * @param content 文章内容
     * @return
     */
    @PostMapping(value = "v1/auth/articles")
    public Result addArticle(Article article, String content) {
        return articleService.addArticle(article, content);
    }

    /**
     * 修改文章
     * @param article 文章对象
     * @param content 文章内容
     * @return
     */
    @PatchMapping(value = "v1/auth/articles")
    public Result updateArticle(Article article, String content) {
        return articleService.updateArticle(article, content);
    }

    /**
     * 获取文章详情
     * @param id 主键id
     * @return
     */
    @GetMapping(value = "v1/auth/articles/toDetail")
    public Result toDetailArticle(String id){
        return articleService.toDetailArticle(id);
    }
}
