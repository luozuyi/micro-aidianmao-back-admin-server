package com.aidianmao.controller;

import com.aidianmao.service.PlatformMessageService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class PlatformMessageController {

    @Autowired
    private PlatformMessageService platformMessageService;

    /**
     *  站内信分页查询
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/paltform-message/paginations")
    public Result getList(Integer pageNum, Integer pageSize,Map<String, Object> params){
        return platformMessageService.pageList(pageNum,pageSize,params);
    }
}
