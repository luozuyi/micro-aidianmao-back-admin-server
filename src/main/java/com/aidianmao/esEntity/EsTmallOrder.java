package com.aidianmao.esEntity;


import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;

@Document(indexName = "tmallorder",type="tmallorder",indexStoreType="fs",shards=5,replicas=1,refreshInterval="-1")
public class EsTmallOrder implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    @Id
    private String id;
    /**
     * 店铺编号
     */
    private String code;
    /**
     * 下单人
     */
    private String buyName;
    /**
     * 店铺名称
     */
    private String shopName;
    /**
     * 订单总价
     */
    private String price;
    /**
     * 店铺所属人
     */
    private String saleName;
    /**
     * 店铺价格
     */
    private String shopPrice;
    /**
     * 服务费
     */
    private String serviceFee;
    /**
     * 消费者保障金
     */
    private String shopDeposit;
    /**
     * 技术年费
     */
    private String shopTechServiceFee;
    /**
     * 下单时间
     */
    private String createTime;
    /**
     * 订单状态   订单状态 0:待付款 1:已付定金 2:已付全款 3:交易成功
     * 4:已取消 5:已终止 6:异常 7:交接中
     */
    private String status;
    /**
     * 订单总价大于这个价格
     */
    private String beginPrice;
    /**
     * 订单总价小于这个价格
     */
    private String endPrice;
    /**
     * 下单时间大于这个时间
     */
    private String beginTime;
    /**
     * 下单时间小于这个时间
     */
    private String endTime;
    /**
     * 默认为0 卖家是否确认 0:未确认 1:已确认
     */
    private String isSaleSure;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBuyName() {
        return buyName;
    }

    public void setBuyName(String buyName) {
        this.buyName = buyName;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSaleName() {
        return saleName;
    }

    public void setSaleName(String saleName) {
        this.saleName = saleName;
    }

    public String getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(String shopPrice) {
        this.shopPrice = shopPrice;
    }

    public String getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(String serviceFee) {
        this.serviceFee = serviceFee;
    }

    public String getShopDeposit() {
        return shopDeposit;
    }

    public void setShopDeposit(String shopDeposit) {
        this.shopDeposit = shopDeposit;
    }

    public String getShopTechServiceFee() {
        return shopTechServiceFee;
    }

    public void setShopTechServiceFee(String shopTechServiceFee) {
        this.shopTechServiceFee = shopTechServiceFee;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBeginPrice() {
        return beginPrice;
    }

    public void setBeginPrice(String beginPrice) {
        this.beginPrice = beginPrice;
    }

    public String getEndPrice() {
        return endPrice;
    }

    public void setEndPrice(String endPrice) {
        this.endPrice = endPrice;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getIsSaleSure() {
        return isSaleSure;
    }

    public void setIsSaleSure(String isSaleSure) {
        this.isSaleSure = isSaleSure;
    }


}
