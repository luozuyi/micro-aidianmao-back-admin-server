package com.aidianmao.esEntity;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * 公共客户池
 */
@Document(indexName="member",type="member",indexStoreType="fs",shards=5,replicas=1,refreshInterval="-1")
public class EsMember implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    @Id
    private String id;

    private Date createTime;

    private String userName;

    private String phone;

    private String identity;

    private String importanceDegree;

    private BigDecimal money;

    private BigDecimal freezeMoney;

    private Integer tmallCount;

    private Date currentLoginTime;

    private Integer loginCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getImportanceDegree() {
        return importanceDegree;
    }

    public void setImportanceDegree(String importanceDegree) {
        this.importanceDegree = importanceDegree;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getFreezeMoney() {
        return freezeMoney;
    }

    public void setFreezeMoney(BigDecimal freezeMoney) {
        this.freezeMoney = freezeMoney;
    }

    public Integer getTmallCount() {
        return tmallCount;
    }

    public void setTmallCount(Integer tmallCount) {
        this.tmallCount = tmallCount;
    }

    public Date getCurrentLoginTime() {
        return currentLoginTime;
    }

    public void setCurrentLoginTime(Date currentLoginTime) {
        this.currentLoginTime = currentLoginTime;
    }

    public Integer getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(Integer loginCount) {
        this.loginCount = loginCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EsMember esMember = (EsMember) o;
        return Objects.equals(id, esMember.id) &&
                Objects.equals(createTime, esMember.createTime) &&
                Objects.equals(userName, esMember.userName) &&
                Objects.equals(phone, esMember.phone) &&
                Objects.equals(identity, esMember.identity) &&
                Objects.equals(importanceDegree, esMember.importanceDegree) &&
                Objects.equals(money, esMember.money) &&
                Objects.equals(freezeMoney, esMember.freezeMoney) &&
                Objects.equals(tmallCount, esMember.tmallCount) &&
                Objects.equals(currentLoginTime, esMember.currentLoginTime) &&
                Objects.equals(loginCount, esMember.loginCount);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, createTime, userName, phone, identity, importanceDegree, money, freezeMoney, tmallCount, currentLoginTime, loginCount);
    }
}
