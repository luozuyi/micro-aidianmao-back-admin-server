package com.aidianmao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class WithdrawDeposit implements Serializable {
    /*主键ID*/
    private String id;
    /*创建时间*/
    private Date createTime;
    /*删除标志位*/
    private String delFlag;
    /*会员银行信息id*/
    private String memberBankInfoId;
    /*提现金额*/
    private BigDecimal money;
    /*到账金额*/
    private BigDecimal actualAppropriation;
    /*提现手续费*/
    private BigDecimal serviceFee;
    /*状态 0:审核中 1:打款中 2:成功 3:失败*/
    private String status;
    /*备注*/
    private String remark;
    /*审核人关联的id*/
    private String adminId;
    /*会员id*/
    private String memberId;
    /* 拒绝理由 0:真实姓名有误 1:支行信息有误 2:银行账号有误 3:提错金额 4:未经许可提现*/
    private String refuseReason;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getMemberBankInfoId() {
        return memberBankInfoId;
    }

    public void setMemberBankInfoId(String memberBankInfoId) {
        this.memberBankInfoId = memberBankInfoId == null ? null : memberBankInfoId.trim();
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getActualAppropriation() {
        return actualAppropriation;
    }

    public void setActualAppropriation(BigDecimal actualAppropriation) {
        this.actualAppropriation = actualAppropriation;
    }

    public BigDecimal getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(BigDecimal serviceFee) {
        this.serviceFee = serviceFee;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getRefuseReason() {
        return refuseReason;
    }

    public void setRefuseReason(String refuseReason) {
        this.refuseReason = refuseReason == null ? null : refuseReason.trim();
    }
}