package com.aidianmao.entity;

import java.io.Serializable;
import java.util.Date;

public class SalespersonConfig implements Serializable{
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除标志位 0:正常 1:删除 2:待删除
     */
    private String delFlag;
    /**
     *最大客户数
     */
    private Integer maximunNum;
    /**
     *管理员id
     */
    private String adminId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public Integer getMaximunNum() {
        return maximunNum;
    }

    public void setMaximunNum(Integer maximunNum) {
        this.maximunNum = maximunNum ;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId == null ? null : adminId.trim();
    }
}