package com.aidianmao.entity;

import java.io.Serializable;
import java.util.Date;

public class AdminMemberRecord implements Serializable {
    /*主键ID*/
    private String id;
    /*创建时间*/
    private Date createTime;
    /*删除标志位*/
    private String delFlag;
    /*会员ID*/
    private String memberId;
    /*销售管理员ID*/
    private String adminId;
    /*迁入私人池时间*/
    private Date impactTime;
    /*迁出私人池时间*/
    private Date ingoingTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId == null ? null : adminId.trim();
    }

    public Date getImpactTime() {
        return impactTime;
    }

    public void setImpactTime(Date impactTime) {
        this.impactTime = impactTime;
    }

    public Date getIngoingTime() {
        return ingoingTime;
    }

    public void setIngoingTime(Date ingoingTime) {
        this.ingoingTime = ingoingTime;
    }
}