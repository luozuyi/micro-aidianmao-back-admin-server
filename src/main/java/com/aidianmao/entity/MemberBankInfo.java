package com.aidianmao.entity;

import java.io.Serializable;
import java.util.Date;

public class MemberBankInfo implements Serializable{
    /*主键ID*/
    private String id;
    /*创建时间*/
    private Date createTime;
    /*删除标志位*/
    private String delFlag;
    /*会员id*/
    private String memberId;
    /*银行id*/
    private String bankId;
    /*银行账号*/
    private String account;
    /*详细支行信息id*/
    private String provinceCityCountyId;
    /*是否默认 0:默认 1:不是默认*/
    private String isDefault;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId == null ? null : bankId.trim();
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    public String getProvinceCityCountyId() {
        return provinceCityCountyId;
    }

    public void setProvinceCityCountyId(String provinceCityCountyId) {
        this.provinceCityCountyId = provinceCityCountyId == null ? null : provinceCityCountyId.trim();
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault == null ? null : isDefault.trim();
    }
}