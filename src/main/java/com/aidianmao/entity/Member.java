package com.aidianmao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Member implements Serializable{
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除状态
     */
    private String delFlag;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 真实姓名
     */
    private String realName;
    /**
     * 密码
     */
    private String password;
    /**
     * 支付密码
     */
    private String payPassword;
    /**
     * 手机号
     */
    private String phone;
    /**
     * qq号
     */
    private String qq;
    /**
     * 性别 0: 男 1: 女
     */
    private String gender;
    /**
     * 生日
     */
    private Date birthday;
    /**
     * 身份证号
     */
    private String idCard;
    /**
     * 账户资金
     */
    private BigDecimal money;
    /**
     * 冻结资金
     */
    private BigDecimal freezeMoney;
    /**
     * 会员头像
     */
    private String headImage;
    /**
     * 登陆次数
     */
    private Integer loginCount;
    /**
     * 邮件
     */
    private String email;
    /**
     * 注册ip
     */
    private String registerIp;
    /**
     * 最后登陆ip
     */
    private String lastLoginIp;
    /**
     * 当前登陆ip
     */
    private String currentLoginIp;
    /**
     * 最后登陆时间
     */
    private Date currentLoginTime;
    /**
     * 天猫店铺数量
     */
    private Integer tmallCount;
    /**
     * 会员身份 -1-默认；0-既是买家也是卖家；1-买家；2-卖家
     */
    private String identity;
    /**
     * 会员等级 0:A 1:B 2:C 3:D 4:E 备注等级说明 :A 基本谈好,待成交 B 有符合需求店铺,需跟进 C 有需求,无店铺,需持续跟进 D 有需求,加上QQ,未深入了解 E 暂时不需要
     */
    private String importanceDegree;

    /**
     *来源类型(0:自然来源 1:百度搜索 2:360搜索 3:搜狗搜索 4:亲友推荐 5:电话拜访 6:其他渠道)
     */
    private String originType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq == null ? null : qq.trim();
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender == null ? null : gender.trim();
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard == null ? null : idCard.trim();
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getFreezeMoney() {
        return freezeMoney;
    }

    public void setFreezeMoney(BigDecimal freezeMoney) {
        this.freezeMoney = freezeMoney;
    }

    public String getHeadImage() {
        return headImage;
    }

    public void setHeadImage(String headImage) {
        this.headImage = headImage == null ? null : headImage.trim();
    }

    public Integer getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(Integer loginCount) {
        this.loginCount = loginCount;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getRegisterIp() {
        return registerIp;
    }

    public void setRegisterIp(String registerIp) {
        this.registerIp = registerIp == null ? null : registerIp.trim();
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp == null ? null : lastLoginIp.trim();
    }

    public String getCurrentLoginIp() {
        return currentLoginIp;
    }

    public void setCurrentLoginIp(String currentLoginIp) {
        this.currentLoginIp = currentLoginIp == null ? null : currentLoginIp.trim();
    }

    public Date getCurrentLoginTime() {
        return currentLoginTime;
    }

    public void setCurrentLoginTime(Date currentLoginTime) {
        this.currentLoginTime = currentLoginTime;
    }

    public String getPayPassword() {
        return payPassword;
    }

    public void setPayPassword(String payPassword) {
        this.payPassword = payPassword == null ? null : payPassword.trim();
    }

    public Integer getTmallCount() {
        return tmallCount;
    }

    public void setTmallCount(Integer tmallCount) {
        this.tmallCount = tmallCount;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity == null ? null : identity.trim();
    }

    public String getImportanceDegree() {
        return importanceDegree;
    }

    public void setImportanceDegree(String importanceDegree) {
        this.importanceDegree = importanceDegree == null ? null : importanceDegree.trim();
    }

    public String getOriginType() {
        return originType;
    }

    public void setOriginType(String originType) {
        this.originType = originType == null ? null : originType.trim();
    }
}