package com.aidianmao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class GroupTarget implements Serializable {
    /*主键ID*/
    private String id;
    /*创建时间*/
    private Date createTime;
    /*删除标志位*/
    private String delFlag;
    /*组长ID*/
    private String leaderId;
    /*小组ID*/
    private String groupId;
    /*小组月目标*/
    private BigDecimal monthGoal;
    /*小组月实际销售金额*/
    private BigDecimal monthAchive;
    /*年*/
    private String year;
    /*月份*/
    private String month;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getLeaderId() {
        return leaderId;
    }

    public void setLeaderId(String leaderId) {
        this.leaderId = leaderId == null ? null : leaderId.trim();
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId == null ? null : groupId.trim();
    }

    public BigDecimal getMonthGoal() {
        return monthGoal;
    }

    public void setMonthGoal(BigDecimal monthGoal) {
        this.monthGoal = monthGoal;
    }

    public BigDecimal getMonthAchive() {
        return monthAchive;
    }

    public void setMonthAchive(BigDecimal monthAchive) {
        this.monthAchive = monthAchive;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year == null ? null : year.trim();
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month == null ? null : month.trim();
    }
}