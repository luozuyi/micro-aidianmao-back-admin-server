package com.aidianmao.entity;

import java.io.Serializable;

/**
 * 区
 * 
 * @author Catch22
 * @date 2018年5月31日
 */
public class Countrys implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	/**
	 * 区名字
	 */
	private String name;
	/**
	 * 城市id
	 */
	private String cityId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

}
