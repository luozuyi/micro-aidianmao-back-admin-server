package com.aidianmao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class TmallCostRule implements Serializable{
    /*主键id*/
    private String id;
    /*创建时间*/
    private Date createTime;
    /*删除标志位 0:正常 1:删除 2:待删除*/
    private String delFlag;
    /*买家费率*/
    private BigDecimal buyerRate;
    /*卖家费率*/
    private BigDecimal sellerRate;
    /*买家折扣*/
    private BigDecimal buyerDiscount;
    /*卖家折扣*/
    private BigDecimal sellerDiscount;
    /*序号*/
    private String num;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public BigDecimal getBuyerRate() {
        return buyerRate;
    }

    public void setBuyerRate(BigDecimal buyerRate) {
        this.buyerRate = buyerRate;
    }

    public BigDecimal getSellerRate() {
        return sellerRate;
    }

    public void setSellerRate(BigDecimal sellerRate) {
        this.sellerRate = sellerRate;
    }

    public BigDecimal getBuyerDiscount() {
        return buyerDiscount;
    }

    public void setBuyerDiscount(BigDecimal buyerDiscount) {
        this.buyerDiscount = buyerDiscount;
    }

    public BigDecimal getSellerDiscount() {
        return sellerDiscount;
    }

    public void setSellerDiscount(BigDecimal sellerDiscount) {
        this.sellerDiscount = sellerDiscount;
    }
    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num == null ? null : num.trim();
    }
}