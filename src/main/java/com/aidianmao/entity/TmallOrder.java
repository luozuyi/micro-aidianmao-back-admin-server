package com.aidianmao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class TmallOrder implements Serializable{
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除状态
     */
    private String delFlag;
    /**
     * 天猫店铺id
     */
    private String tmallId;
    /**
     * 订单编号
     */
    private String code;
    /**
     * 订单总价
     */
    private BigDecimal price;
    /**
     * 订单服务费
     */
    private BigDecimal serviceFee;
    /**
     * 买家id
     */
    private String buyMemberId;
    /**
     * 卖家id
     */
    private String saleMemberId;
    /**
     * 订单结束时间
     */
    private Date endTime;
    /**
     * 订单状态 0:待付款 1:已付款 2:交易成功 3:已取消 4:已终止
     */
    private String status;
    /**
     * 交接员id
     */
    private String takeOverUserId;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 默认为0 卖家是否确认 0:未确认 1:已确认
     */
    private String isSaleSure;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getTmallId() {
        return tmallId;
    }

    public void setTmallId(String tmallId) {
        this.tmallId = tmallId == null ? null : tmallId.trim();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(BigDecimal serviceFee) {
        this.serviceFee = serviceFee;
    }

    public String getBuyMemberId() {
        return buyMemberId;
    }

    public void setBuyMemberId(String buyMemberId) {
        this.buyMemberId = buyMemberId == null ? null : buyMemberId.trim();
    }

    public String getSaleMemberId() {
        return saleMemberId;
    }

    public void setSaleMemberId(String saleMemberId) {
        this.saleMemberId = saleMemberId == null ? null : saleMemberId.trim();
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getTakeOverUserId() {
        return takeOverUserId;
    }

    public void setTakeOverUserId(String takeOverUserId) {
        this.takeOverUserId = takeOverUserId == null ? null : takeOverUserId.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getIsSaleSure() {
        return isSaleSure;
    }

    public void setIsSaleSure(String isSaleSure) {
        this.isSaleSure = isSaleSure == null ? null : isSaleSure.trim();
    }
}