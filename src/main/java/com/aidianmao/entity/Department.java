package com.aidianmao.entity;

import java.io.Serializable;
import java.util.Date;

public class Department implements Serializable{
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除状态0：正常1：删除
     */
    private String delFlag;
    /**
     * 部门组长id
     */
    private String headManAdminId;
    /**
     * 部门名称
     */
    private String deptName;
    /**
     * 部门logo图像
     */
    private String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getHeadManAdminId() {
        return headManAdminId;
    }

    public void setHeadManAdminId(String headManAdminId) {
        this.headManAdminId = headManAdminId == null ? null : headManAdminId.trim();
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName == null ? null : deptName.trim();
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image == null ? null : image.trim();
    }
}