package com.aidianmao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 提现冻结明细实体类
 */
public class WithdrawFreezeDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除标志位 0:正常 1：删除 2：待删除
     */
    private String delFlag;
    /**
     * 会员id
     */
    private String memberId;
    /**
     * 提现id
     */
    private String withdrawDepositId;
    /**
     * 冻结金额
     */
    private BigDecimal freezeMoney;
    /**
     * 冻结状态 0: 冻结中 1: 已解冻
     */
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    public String getWithdrawDepositId() {
        return withdrawDepositId;
    }

    public void setWithdrawDepositId(String withdrawDepositId) {
        this.withdrawDepositId = withdrawDepositId == null ? null : withdrawDepositId.trim();
    }

    public BigDecimal getFreezeMoney() {
        return freezeMoney;
    }

    public void setFreezeMoney(BigDecimal freezeMoney) {
        this.freezeMoney = freezeMoney;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public WithdrawFreezeDetail() {
    }

    public WithdrawFreezeDetail(String id, Date createTime, String delFlag, String memberId, String withdrawDepositId, BigDecimal freezeMoney, String status) {
        this.id = id;
        this.createTime = createTime;
        this.delFlag = delFlag;
        this.memberId = memberId;
        this.withdrawDepositId = withdrawDepositId;
        this.freezeMoney = freezeMoney;
        this.status = status;
    }
}