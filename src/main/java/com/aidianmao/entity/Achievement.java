package com.aidianmao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Achievement implements Serializable {
    /*主键ID*/
    private String id;
    /*创建时间*/
    private Date createTime;
    /*删除标志位*/
    private String delFlag;
    /*卖家销售id*/
    private String sellerAdminId;
    /*买家销售id*/
    private String buyerAdminId;
    /*卖家销售业绩*/
    private BigDecimal sellerAchieve;
    /*买家销售业绩*/
    private BigDecimal buyerAchieve;
    /*总业绩*/
    private BigDecimal totalAchieve;
    /*操作业绩时间*/
    private Date time;
    /*状态 0:审核中;1:自己作废;2:组长审核通过;3:组长审核不通过;4:经理核对成功;5:经理核对失败*/
    private String status;
    /*来源 0:售店收入;1:其他收入*/
    private String achieveOrigin;
    /*业绩描述*/
    private String description;
    /*审核组长id*/
    private String operateGroupId;
    /*订单号*/
    private String orderCode;
    /* 审核备注*/
    private String note;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getSellerAdminId() {
        return sellerAdminId;
    }

    public void setSellerAdminId(String sellerAdminId) {
        this.sellerAdminId = sellerAdminId == null ? null : sellerAdminId.trim();
    }

    public String getBuyerAdminId() {
        return buyerAdminId;
    }

    public void setBuyerAdminId(String buyerAdminId) {
        this.buyerAdminId = buyerAdminId == null ? null : buyerAdminId.trim();
    }

    public BigDecimal getSellerAchieve() {
        return sellerAchieve;
    }

    public void setSellerAchieve(BigDecimal sellerAchieve) {
        this.sellerAchieve = sellerAchieve;
    }

    public BigDecimal getBuyerAchieve() {
        return buyerAchieve;
    }

    public void setBuyerAchieve(BigDecimal buyerAchieve) {
        this.buyerAchieve = buyerAchieve;
    }

    public BigDecimal getTotalAchieve() {
        return totalAchieve;
    }

    public void setTotalAchieve(BigDecimal totalAchieve) {
        this.totalAchieve = totalAchieve;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getAchieveOrigin() {
        return achieveOrigin;
    }

    public void setAchieveOrigin(String achieveOrigin) {
        this.achieveOrigin = achieveOrigin == null ? null : achieveOrigin.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getOperateGroupId() {
        return operateGroupId;
    }

    public void setOperateGroupId(String operateGroupId) {
        this.operateGroupId = operateGroupId == null ? null : operateGroupId.trim();
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode == null ? null : orderCode.trim();
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }
}