package com.aidianmao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class IncomeExpensesDetail implements Serializable {
    /*主键ID*/
    private String id;
    /*创建时间*/
    private Date createTime;
    /*删除标志位 0:正常 1:删除 2:待删除*/
    private String delFlag;
    /*收支类型 0:收入 1:支出*/
    private String type;
    /*场景 0:充值 1:tmall订单支付 2:提现 3:tmall店铺售出 4:tmall订单交易成功 6:佣金返利 7:差价返利 8.违约金调整返还*/
    private String moneyScene;
    /*金额*/
    private BigDecimal amountMoney;
    /*会员ID*/
    private String memberId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getMoneyScene() {
        return moneyScene;
    }

    public void setMoneyScene(String moneyScene) {
        this.moneyScene = moneyScene == null ? null : moneyScene.trim();
    }

    public BigDecimal getAmountMoney() {
        return amountMoney;
    }

    public void setAmountMoney(BigDecimal amountMoney) {
        this.amountMoney = amountMoney;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }
}