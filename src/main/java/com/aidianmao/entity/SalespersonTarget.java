package com.aidianmao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SalespersonTarget implements Serializable{
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除标志位 0:正常 1:删除 2:待删除
     */
    private String delFlag;
    /**
     * 月目标
     */
    private BigDecimal monthGoal;
    /**
     * 个人实际销售金额
     */
    private BigDecimal monthAchive;
    /**
     * 月份
     */
    private String month;
    /**
     * 年份
     */
    private String year;
    /**
     * 个人id
     */
    private String salespersonId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public BigDecimal getMonthGoal() {
        return monthGoal;
    }

    public void setMonthGoal(BigDecimal monthGoal) {
        this.monthGoal = monthGoal;
    }

    public BigDecimal getMonthAchive() {
        return monthAchive;
    }

    public void setMonthAchive(BigDecimal monthAchive) {
        this.monthAchive = monthAchive;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month == null ? null : month.trim();
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year == null ? null : year.trim();
    }

    public String getSalespersonId() {
        return salespersonId;
    }

    public void setSalespersonId(String salespersonId) {
        this.salespersonId = salespersonId == null ? null : salespersonId.trim();
    }
}