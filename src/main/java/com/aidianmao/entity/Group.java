package com.aidianmao.entity;

import java.io.Serializable;
import java.util.Date;

public class Group implements Serializable{
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除状态0：正常1：删除
     */
    private String delFlag;
    /**
     * 小组组长id
     */
    private String headManAdminId;
    /**
     * 所属部门id
     */
    private String deptId;
    /**
     * 小组名称
     */
    private String groupName;
    /**
     * 小组logo图片
     */
    private String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getHeadManAdminId() {
        return headManAdminId;
    }

    public void setHeadManAdminId(String headManAdminId) {
        this.headManAdminId = headManAdminId == null ? null : headManAdminId.trim();
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId == null ? null : deptId.trim();
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName == null ? null : groupName.trim();
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image == null ? null : image.trim();
    }
}