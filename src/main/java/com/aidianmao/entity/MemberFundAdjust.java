package com.aidianmao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class MemberFundAdjust implements Serializable{
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除标志位 0:正常 1:删除 2:待删除
     */
    private String delFlag;
    /**
     * 调整资金
     */
    private BigDecimal money;
    /**
     * 收支类型 0:收入 1:支出
     */
    private String fundType;
    /**
     * 收支类型 0:收入 1:支出
     */
    private String incomeType;
    /**
     * 会员id
     */
    private String memberId;
    /**
     * 管理员id
     */
    private String adminId;
    /**
     * 相关id
     */
    private String relationId;
    /**
     * 关联类型 0:订单 1:提现
     */
    private String relationType;
    /**
     * 调整说明
     */
    private String note;
    /**
     * 场景 0:佣金返利 1:差价返利 2:违约金调整返还 3:提现 4:充值
     */
    private String moneyScene;

    public String getMoneyScene() {
        return moneyScene;
    }

    public void setMoneyScene(String moneyScene) {
        this.moneyScene = moneyScene == null ? null : moneyScene.trim();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getFundType() {
        return fundType;
    }

    public void setFundType(String fundType) {
        this.fundType = fundType == null ? null : fundType.trim();
    }

    public String getIncomeType() {
        return incomeType;
    }

    public void setIncomeType(String incomeType) {
        this.incomeType = incomeType == null ? null : incomeType.trim();
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId == null ? null : adminId.trim();
    }

    public String getRelationId() {
        return relationId;
    }

    public void setRelationId(String relationId) {
        this.relationId = relationId == null ? null : relationId.trim();
    }

    public String getRelationType() {
        return relationType;
    }

    public void setRelationType(String relationType) {
        this.relationType = relationType == null ? null : relationType.trim();
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }
}