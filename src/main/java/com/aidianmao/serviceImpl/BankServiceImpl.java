package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Bank;
import com.aidianmao.mapper.BankMapper;
import com.aidianmao.service.BankService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import com.github.tobato.fastdfs.domain.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;
import java.util.Map;
@Transactional
@Service
public class BankServiceImpl implements BankService {
    @Autowired
    private BankMapper bankMapper;
    @Override
    public Result pageList(Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum,pageSize);
            List<Bank> list = bankMapper.selectAll();
            PageInfo<Map<String,Object>> page = new PageInfo(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result detail(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            }else {
                Bank bank = bankMapper.selectByPrimaryKey(id);
                if(bank != null){
                    result.setData(bank);
                    code = Constants.SUCCESS;
                    msg = "查询成功";
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result update(Bank record) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(record.getId())){
                code = "-3";
                msg = "主键ID不能为空";
            }else {
                Bank bank = bankMapper.selectByPrimaryKey(record.getId());
                if(bank != null){
                    bankMapper.updateByPrimaryKeySelective(record);
                    code = Constants.SUCCESS;
                    msg = "更新成功";
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result insert(Bank record) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(record.getDelFlag())){
                code = "-3";
                msg = "标志位不能为空";
            }else if(StringUtils.isBlank(record.getName())){
                code = "-4";
                msg = "银行名称不能为空";
            }else if(StringUtils.isBlank(record.getCode())){
                code = "-5";
                msg = "银行代码不能为空";
            }/*else if(StringUtils.isBlank(record.getImage())){
                code = "-6";
                msg = "银行图片不能为空";
            }*/else {
                record.setId(CommonUtil.getUUID());
                record.setCreateTime(new Date());
                bankMapper.insertSelective(record);
                code = Constants.SUCCESS;
                msg = "新增成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Autowired
    private FastFileStorageClient fastFileStorageClient;

    @Override
    public Result addBankLogo(MultipartFile file) {
        Result result = new Result();
        String msg = "初始化";
        String code = Constants.FAIL;
        try {
            String fileName = file.getOriginalFilename();
            String prefix = fileName.substring(fileName.lastIndexOf(".") + 1);
            if (CommonUtil.isImage(prefix)) {
                StorePath storePath = fastFileStorageClient.uploadFile(file.getInputStream(), file.getSize(), FilenameUtils.getExtension(file.getOriginalFilename()), null);
                String fileUrl = storePath.getFullPath();
                result.setData(fileUrl);
                code = "0";
                msg = "成功";
            } else {
                code = "-3";
                msg = "只能上传图片";
            }
        } catch (Exception e) {
            code = "-2";
            msg = "上传出错";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }
}
