package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Admin;
import com.aidianmao.entity.SalespersonConfig;
import com.aidianmao.entity.SalespersonTarget;
import com.aidianmao.mapper.AdminMapper;
import com.aidianmao.mapper.SalespersonConfigMapper;
import com.aidianmao.mapper.SalespersonTargetMapper;
import com.aidianmao.service.SalespersonTargetService;
import com.aidianmao.utils.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Transactional
@Service
public class SalespersonTargetServiceImpl implements SalespersonTargetService {

    @Autowired
    private SalespersonTargetMapper salespersonTargetMapper;
    @Autowired
    private SalespersonConfigMapper salespersonConfigMapper;
    @Autowired
    private AdminMapper adminMapper;
    @Override
    public Result insert(String id,SalespersonTarget target) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键id不能为空";
            }else if( target.getMonthGoal()== null || target.getMonthGoal().compareTo(BigDecimal.ZERO)<=0){
                code = "-4";
                msg = "月目标不能为且不能小于零";
            }else if(StringUtils.isBlank(target.getMonth())){
                code = "-5";
                msg = "月份不能为空";
            }else {
                SalespersonConfig salespersonConfig = salespersonConfigMapper.selectByPrimaryKey(id);
                if(StringUtils.isBlank(salespersonConfig.getAdminId())){
                    code = "-6";
                    msg = "没有该业务员";
                    result.setCode(code);
                    result.setMsg(msg);
                    return result;
                }
                if(salespersonConfig == null ){
                    code = "-6";
                    msg = "信息有误";
                    result.setCode(code);
                    result.setMsg(msg);
                    return result;
                }
                Admin admin = adminMapper.selectByPrimaryKey(salespersonConfig.getAdminId());
                if(admin == null){
                    code = "-7";
                    msg = "该业务员不存在";
                    result.setCode(code);
                    result.setMsg(msg);
                    return result;
                }
                   target.setId(CommonUtil.getUUID());
                   target.setCreateTime(new Date());
                   target.setDelFlag("0");
                   target.setYear(RandomCodeUtil.dateToYear(new Date()));
                   target.setSalespersonId(salespersonConfig.getAdminId());
                   SalespersonTarget salespersonTarget = salespersonTargetMapper.selectByMonthAndYearAndGoal(target);
                   if(salespersonTarget != null){
                       code = "-8";
                       msg = "已有该业务员的目标";
                   } else {
                       salespersonTargetMapper.insert(target);
                       code = Constants.SUCCESS;
                       msg = "添加成功";
                   }
            }
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result pageList(Integer pageNum,Integer pageSize,String saleapersonId) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
           List<SalespersonTarget> target = salespersonTargetMapper.selectBySaleId(saleapersonId);
            PageInfo<SalespersonTarget> page = new PageInfo(target);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

}
