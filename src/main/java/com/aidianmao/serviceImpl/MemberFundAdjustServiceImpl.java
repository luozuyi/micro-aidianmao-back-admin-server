package com.aidianmao.serviceImpl;

import com.aidianmao.entity.*;
import com.aidianmao.mapper.*;
import com.aidianmao.service.MemberFundAdjustService;
import com.aidianmao.utils.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class MemberFundAdjustServiceImpl implements MemberFundAdjustService {
    @Autowired
    private MemberFundAdjustMapper memberFundAdjustMapper;
    @Autowired
    private AlipayMapper alipayMapper;
    @Autowired
    private MemberMapper memberMapper;
    @Autowired
    private TmallOrderMapper tmallOrderMapper;
    @Autowired
    private IncomeExpensesDetailMapper incomeExpensesDetailMapper;
    @Autowired
    private PlatformFundDetailMapper platformFundDetailMapper;
    @Autowired
    private PlatformMessageMapper platformMessageMapper;
    @Autowired
    private RemittanceMapper remittanceMapper;

    @Override
    public Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum,pageSize);
            List<Map<String, Object>> list = memberFundAdjustMapper.selectAll(params);
            PageInfo<Map<String,Object>> page = new PageInfo<>(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result insert(MemberFundAdjust record, String phone, String aidianmaoAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if(StringUtils.isBlank(record.getRelationId())){
                code = "-3";
                msg = "相关ID不能为空";
            }else if(StringUtils.isBlank(record.getRelationType())){
                code = "-4";
                msg = "相关类型不能为空";
            }else if(!(Constants.MemberFundAdjustRelationType.TYPE0.getType().equals(record.getRelationType())||
                    Constants.MemberFundAdjustRelationType.TYPE1.getType().equals(record.getRelationType())||
                    Constants.MemberFundAdjustRelationType.TYPE2.getType().equals(record.getRelationType()))){
                code = "-5";
                msg = "请输入正确的相关类型";
            }else if(StringUtils.isBlank(record.getMoneyScene())){
                code = "-6";
                msg = "场景不能为空";
            }else if(!(Constants.MemberFundAdjustMoneyScene.TYPE0.getType().equals(record.getMoneyScene())||
                    Constants.MemberFundAdjustMoneyScene.TYPE1.getType().equals(record.getMoneyScene())||
                    Constants.MemberFundAdjustMoneyScene.TYPE2.getType().equals(record.getMoneyScene())||
                    Constants.MemberFundAdjustMoneyScene.TYPE3.getType().equals(record.getMoneyScene())||
                    Constants.MemberFundAdjustMoneyScene.TYPE4.getType().equals(record.getMoneyScene()))){
                code = "-7";
                msg = "请输入正确的场景";
            }else if(record.getMoney() == null || record.getMoney().compareTo(BigDecimal.ZERO)<=0){
                code = "-8";
                msg = "调整资金不能为空或非正数";
            }else if(StringUtils.isBlank(record.getFundType())){
                code = "-9";
                msg = "资金类型不能为空";
            }else if(!(Constants.MemberFundAdjustFundType.TYPE0.getType().equals(record.getFundType())||
                    Constants.MemberFundAdjustFundType.TYPE1.getType().equals(record.getFundType()))){
                code = "-10";
                msg = "请输入正确的资金类型";
            }else if(StringUtils.isBlank(record.getIncomeType())){
                code = "-11";
                msg = "收支类型不能为空";
            }else if(!(Constants.MemberFundAdjustIncomeType.TYPE0.getType().equals(record.getIncomeType())||
                    Constants.MemberFundAdjustIncomeType.TYPE1.getType().equals(record.getIncomeType()))){
                code = "-8";
                msg = "请输入正确的收支类型";
            }else if(StringUtils.isBlank(record.getNote())){
                code = "-12";
                msg = "调整说明不能为空";
            }else if(!PatternUtil.patternString(phone,"mobile")){
                code = "-13";
                msg = "请输入正确的手机号";
            }else {
                BigDecimal money = record.getMoney();
                if(Constants.MemberFundAdjustRelationType.TYPE0.getType().equals(record.getRelationType())){
                    //0.订单资金调整
                }else if (Constants.MemberFundAdjustRelationType.TYPE1.getType().equals(record.getRelationType())){
                    //1.提现资金调整
                }else if (Constants.MemberFundAdjustRelationType.TYPE2.getType().equals(record.getRelationType())){
                    //2.充值资金调整
                    Alipay alipay = alipayMapper.selectByPrimaryKey(record.getRelationId());
                    if(alipay == null){
                        code = Constants.ERROR;
                        msg = "充值记录不存在，信息输入错误";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    List<Member> list = memberMapper.selectByPhone(phone);
                    if(list.size()!=1){
                        code = Constants.ERROR;
                        msg = "会员信息不存在或者存在多个手机号相同的会员信息";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    Member member = list.get(0);
                    if(member == null){
                        code = Constants.ERROR;
                        msg = "会员不存在，信息输入错误";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    if(!alipay.getMemberId().equals(member.getId())){
                        code = Constants.ERROR;
                        msg = "充值记录与会员信息不对应，请重新确认信息";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    if(Constants.MemberFundAdjustMoneyScene.TYPE4.getType().equals(record.getMoneyScene())){
                        if(Constants.MemberFundAdjustFundType.TYPE0.getType().equals(record.getFundType())){
                            //调整可用资金
                            if(Constants.MemberFundAdjustIncomeType.TYPE0.getType().equals(record.getIncomeType())){
                                //收入
                                if(money.compareTo(alipay.getPayMoney())!=0){
                                    code = Constants.ERROR;
                                    msg = "调整资金金额与充值记录金额不一致，请重新确认信息";
                                    result.setCode(code);
                                    result.setMsg(msg);
                                    return result;
                                }
                                record.setId(CommonUtil.getUUID());
                                record.setCreateTime(new Date());
                                record.setDelFlag(Constants.DelFlag.NORMAL.getType());
                                record.setMemberId(member.getId());
                                record.setAdminId(adminId);
                                memberFundAdjustMapper.insertSelective(record);

                                BigDecimal newMoney = member.getMoney().add(money);
                                //修改会员账户资金
                                member.setMoney(newMoney);
                                memberMapper.updateCustomerSelective(member);
                                code = Constants.SUCCESS;
                                msg = "充值收入，调整成功";
                            }else if (Constants.MemberFundAdjustIncomeType.TYPE1.getType().equals(record.getIncomeType())){
                                //支出
                                record.setId(CommonUtil.getUUID());
                                record.setCreateTime(new Date());
                                record.setDelFlag(Constants.DelFlag.NORMAL.getType());
                                record.setMemberId(member.getId());
                                record.setAdminId(adminId);
                                memberFundAdjustMapper.insertSelective(record);

                                BigDecimal newMoney = member.getMoney().subtract(money);
                                if(newMoney.compareTo(BigDecimal.ZERO)<0){
                                    code = Constants.ERROR;
                                    msg = "支出资金过多，账户余额不足";
                                    result.setCode(code);
                                    result.setMsg(msg);
                                    return result;
                                }
                                //修改会员账户资金
                                member.setMoney(newMoney);
                                memberMapper.updateCustomerSelective(member);
                                code = Constants.SUCCESS;
                                msg = "调整成功";
                            }
                        }else if(Constants.MemberFundAdjustFundType.TYPE1.getType().equals(record.getFundType())){
                            //调整冻结资金(暂时不支持冻结资金调整)
                        }
                    }else {
                        code = Constants.ERROR;
                        msg = "场景输入错误";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result insertFundAdjust(MemberFundAdjust fundAdjust, String phone, String aidianmaoAdminToken){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if(StringUtils.isBlank(fundAdjust.getRelationId())){
                code = "-3";
                msg = "相关ID不能为空";
            }else if(StringUtils.isBlank(fundAdjust.getRelationType())){
                code = "-4";
                msg = "相关类型不能为空";
            }else if(!(Constants.MemberFundAdjustRelationType.TYPE0.getType().equals(fundAdjust.getRelationType())||
                    Constants.MemberFundAdjustRelationType.TYPE1.getType().equals(fundAdjust.getRelationType())||
                    Constants.MemberFundAdjustRelationType.TYPE2.getType().equals(fundAdjust.getRelationType()))){
                code = "-5";
                msg = "请输入正确的相关类型";
            }else if(StringUtils.isBlank(fundAdjust.getMoneyScene())){
                code = "-6";
                msg = "场景不能为空";
            }else if(!(Constants.MemberFundAdjustMoneyScene.TYPE0.getType().equals(fundAdjust.getMoneyScene())||
                    Constants.MemberFundAdjustMoneyScene.TYPE1.getType().equals(fundAdjust.getMoneyScene())||
                    Constants.MemberFundAdjustMoneyScene.TYPE2.getType().equals(fundAdjust.getMoneyScene())||
                    Constants.MemberFundAdjustMoneyScene.TYPE3.getType().equals(fundAdjust.getMoneyScene())||
                    Constants.MemberFundAdjustMoneyScene.TYPE4.getType().equals(fundAdjust.getMoneyScene())||
                    Constants.MemberFundAdjustMoneyScene.TYPE5.getType().equals(fundAdjust.getMoneyScene()))){
                code = "-7";
                msg = "请输入正确的场景";
            }else if(fundAdjust.getMoney() == null || fundAdjust.getMoney().compareTo(BigDecimal.ZERO)<=0){
                code = "-8";
                msg = "调整资金不能为空或非正数";
            /*}else if(StringUtils.isBlank(fundAdjust.getFundType())){
                code = "-9";
                msg = "资金类型不能为空";
            }else if(!(Constants.MemberFundAdjustFundType.TYPE0.getType().equals(fundAdjust.getFundType())||
                    Constants.MemberFundAdjustFundType.TYPE1.getType().equals(fundAdjust.getFundType()))){
                code = "-10";
                msg = "请输入正确的资金类型";*/
            }else if(StringUtils.isBlank(fundAdjust.getIncomeType())){
                code = "-11";
                msg = "收支类型不能为空";
            }else if(!(Constants.MemberFundAdjustIncomeType.TYPE0.getType().equals(fundAdjust.getIncomeType())||
                    Constants.MemberFundAdjustIncomeType.TYPE1.getType().equals(fundAdjust.getIncomeType()))){
                code = "-8";
                msg = "请输入正确的收支类型";
            }else if(StringUtils.isBlank(fundAdjust.getNote())){
                code = "-12";
                msg = "调整说明不能为空";
            }else if(StringUtils.isBlank(phone)){
                code = "-13";
                msg = "请输入手机号";
            }else if(!PatternUtil.patternString(phone,"mobile")){
                code = "-14";
                msg = "请输入正确的手机号";
            }else {
                MemberFundAdjust memberFundAdjust = memberFundAdjustMapper.selectByRelationId(fundAdjust.getRelationId());
                if (memberFundAdjust != null) {
                    code = "-26";
                    msg = "一个订单只能调整一次";
                    result.setCode(code);
                    result.setMsg(msg);
                    return result;
                }
                BigDecimal money = fundAdjust.getMoney();
                // 订单
                if (fundAdjust.getRelationType().equals("0")) {
                    TmallOrder order = tmallOrderMapper.selectByCode(fundAdjust.getRelationId());
                    if (order == null) {
                        code = "-14";
                        msg = "订单信息不存在，请核对信息";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    Member buyMember = memberMapper.selectByPrimaryKey(order.getBuyMemberId());
                    Member saleMember = memberMapper.selectByPrimaryKey(order.getSaleMemberId());
                    if (buyMember == null) {
                        code = "-24";
                        msg = "该会员不存在，请核对信息";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    if (!buyMember.getPhone().equals(phone)) {
                        code = "-15";
                        msg = "订单信息与会员信息不一致，请输入正确的订单ID和手机号";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    // 差价返利
                    if (fundAdjust.getMoneyScene().equals("1")) {
                        if (!order.getStatus().equals("3")) {
                            code = "-16";
                            msg = "只有交易成功的订单才有差价返利";
                            result.setCode(code);
                            result.setMsg(msg);
                            return result;
                        }
                        //修改订单状态
                        order.setStatus("7");
                        tmallOrderMapper.updateByPrimaryKeySelective(order);
                        // 修改卖家资金
                        saleMember.setMoney(saleMember.getMoney().subtract(money));
                        memberMapper.updateByPrimaryKeySelective(saleMember);
                        // 修改买家资金
                        buyMember.setMoney(buyMember.getMoney().add(money));
                        memberMapper.updateByPrimaryKeySelective(buyMember);
                        // 添加买家资金调整记录
                        fundAdjust.setId(CommonUtil.getUUID());
                        fundAdjust.setCreateTime(new Date());
                        fundAdjust.setDelFlag("0");
                        fundAdjust.setMemberId(order.getBuyMemberId());
                        fundAdjust.setFundType("0");
                        fundAdjust.setAdminId(adminId);
                        memberFundAdjustMapper.insertSelective(fundAdjust);

                        // 添加买家会员收支明细
                        IncomeExpensesDetail expensesDetail = new IncomeExpensesDetail();
                        expensesDetail.setId(CommonUtil.getUUID());
                        expensesDetail.setCreateTime(new Date());
                        expensesDetail.setDelFlag("0");
                        // 收支类型 0:收入 1:支出
                        expensesDetail.setType("0");
                        //场景:0:充值 1:佣金返利 2:差价返利 3:违约定金返还 4:违约金调整返还 5:支付余额返还
                        //6:全款购买 7:店铺余额 8:定金 9:违约金 10:提现 11:交易成功 12:违约全款返还 13:差价返利扣除 14:充值调整
                        expensesDetail.setMoneyScene("2");
                        expensesDetail.setAmountMoney(money);
                        expensesDetail.setMemberId(order.getBuyMemberId());
                        incomeExpensesDetailMapper.insertSelective(expensesDetail);
                        // 添加卖家会员收支明细
                        IncomeExpensesDetail expensesDetail1 = new IncomeExpensesDetail();
                        expensesDetail1.setId(CommonUtil.getUUID());
                        expensesDetail1.setCreateTime(new Date());
                        expensesDetail1.setDelFlag("0");
                        // 收支类型 0:收入 1:支出
                        expensesDetail1.setType("1");
                        //场景:0:充值 1:佣金返利 2:差价返利 3:违约定金返还 4:违约金调整返还 5:支付余额返还
                        //6:全款购买 7:店铺余额 8:定金 9:违约金 10:提现 11:交易成功 12:违约全款返还 13:差价返利扣除 14:充值调整
                        expensesDetail1.setMoneyScene("13");
                        expensesDetail1.setAmountMoney(money);
                        expensesDetail1.setMemberId(order.getSaleMemberId());
                        incomeExpensesDetailMapper.insertSelective(expensesDetail1);
                        // 添加站内信
                        PlatformMessage platformMessage = new PlatformMessage();
                        platformMessage.setId(CommonUtil.getUUID());
                        platformMessage.setCreateTime(new Date());
                        platformMessage.setMemberId(buyMember.getId());
                        platformMessage.setDelFlag(Constants.DelFlag.NORMAL.getType());
                        platformMessage.setTitle("您的订单平台返利"+ money + "元");
                        platformMessage.setContent("尊敬的爱店猫用户：\n" +
                                "您的订单平台返利" + money + "元，您的账户可用余额" + buyMember.getMoney() + "元。");
                        platformMessage.setStatus(Constants.PlatformMessageStatus.TYPE0.getType());
                        platformMessageMapper.insertSelective(platformMessage);
                        code = Constants.SUCCESS;
                        msg = "差价返利成功";
                    } else if (fundAdjust.getMoneyScene().equals("0")) {  // 佣金返利
                        if (!order.getStatus().equals("3")) {
                            code = "-17";
                            msg = "只有交易成功的订单才有佣金返利";
                            result.setCode(code);
                            result.setMsg(msg);
                            return result;
                        }

                        //修改订单状态
                        order.setStatus("7");
                        tmallOrderMapper.updateByPrimaryKeySelective(order);
                        // 修改会员资金
                        buyMember.setMoney(buyMember.getMoney().add(money));
                        memberMapper.updateByPrimaryKeySelective(buyMember);
                        // 添加资金调整记录
                        fundAdjust.setId(CommonUtil.getUUID());
                        fundAdjust.setCreateTime(new Date());
                        fundAdjust.setDelFlag("0");
                        fundAdjust.setMemberId(order.getBuyMemberId());
                        fundAdjust.setFundType("0");
                        fundAdjust.setAdminId(adminId);
                        fundAdjust.setFundType("0");
                        memberFundAdjustMapper.insertSelective(fundAdjust);
                        // 添加买家会员收支明细
                        IncomeExpensesDetail expensesDetail = new IncomeExpensesDetail();
                        expensesDetail.setId(CommonUtil.getUUID());
                        expensesDetail.setCreateTime(new Date());
                        expensesDetail.setDelFlag("0");
                        // 收支类型 0:收入 1:支出
                        expensesDetail.setType("0");
                        //场景:0:充值 1:佣金返利 2:差价返利 3:违约定金返还 4:违约金调整返还 5:支付余额返还
                        //6:全款购买 7:店铺余额 8:定金 9:违约金 10:提现 11:交易成功 12:违约全款返还 13:差价返利扣除 14:充值调整
                        expensesDetail.setMoneyScene("1");
                        expensesDetail.setAmountMoney(money);
                        expensesDetail.setMemberId(order.getBuyMemberId());
                        incomeExpensesDetailMapper.insertSelective(expensesDetail);
                        // 添加平台收支明细
                        PlatformFundDetail platformFundDetail = new PlatformFundDetail();
                        platformFundDetail.setId(CommonUtil.getUUID());
                        platformFundDetail.setCreateTime(new Date());
                        platformFundDetail.setDelFlag("0");
                        platformFundDetail.setRelationId(order.getId());
                        platformFundDetail.setRelationType("0");
                        // 收支类型 0:收入 1:支出
                        platformFundDetail.setType("1");
                        //场景:0:服务费 1:提现手续费 2:违约金 3:违约金调整返还 4:佣金返利 5:银行手续费
                        platformFundDetail.setMoneyScene("4");
                        platformFundDetail.setAmountMoney(money);
                        platformFundDetailMapper.insertSelective(platformFundDetail);
                        // 添加站内信
                        PlatformMessage platformMessage = new PlatformMessage();
                        platformMessage.setId(CommonUtil.getUUID());
                        platformMessage.setCreateTime(new Date());
                        platformMessage.setMemberId(buyMember.getId());
                        platformMessage.setDelFlag(Constants.DelFlag.NORMAL.getType());
                        platformMessage.setTitle("您的订单平台返利"+ money + "元");
                        platformMessage.setContent("尊敬的爱店猫用户：\n" +
                                "您的订单平台返利" + money + "元，您的账户可用余额" + buyMember.getMoney() + "元。");
                        platformMessage.setStatus(Constants.PlatformMessageStatus.TYPE0.getType());
                        platformMessageMapper.insertSelective(platformMessage);
                        code = Constants.SUCCESS;
                        msg = "佣金返利成功";
                    } else if (fundAdjust.getMoneyScene().equals("2")) { // 违约金调整返还
                        if (!order.getStatus().equals("6")) {
                            code = "-18";
                            msg = "只有交易终止的异常订单才有违约金调整返还";
                            result.setCode(code);
                            result.setMsg(msg);
                            return result;
                        }
                        //修改订单状态
                        order.setStatus("7");
                        tmallOrderMapper.updateByPrimaryKeySelective(order);
                        // 修改会员资金
                        buyMember.setMoney(buyMember.getMoney().add(money));
                        memberMapper.updateByPrimaryKeySelective(buyMember);
                        // 添加资金调整记录
                        fundAdjust.setId(CommonUtil.getUUID());
                        fundAdjust.setCreateTime(new Date());
                        fundAdjust.setDelFlag("0");
                        fundAdjust.setMemberId(order.getBuyMemberId());
                        fundAdjust.setAdminId(adminId);
                        fundAdjust.setFundType("0");
                        memberFundAdjustMapper.insertSelective(fundAdjust);
                        // 添加买家会员收支明细
                        IncomeExpensesDetail expensesDetail = new IncomeExpensesDetail();
                        expensesDetail.setId(CommonUtil.getUUID());
                        expensesDetail.setCreateTime(new Date());
                        expensesDetail.setDelFlag("0");
                        // 收支类型 0:收入 1:支出
                        expensesDetail.setType("0");
                        //场景:0:充值 1:佣金返利 2:差价返利 3:违约定金返还 4:违约金调整返还 5:支付余额返还
                        //6:全款购买 7:店铺余额 8:定金 9:违约金 10:提现 11:交易成功 12:违约全款返还 13:差价返利扣除 14:充值调整
                        expensesDetail.setMoneyScene("4");
                        expensesDetail.setAmountMoney(money);
                        expensesDetail.setMemberId(order.getBuyMemberId());
                        incomeExpensesDetailMapper.insertSelective(expensesDetail);
                        // 添加平台收支明细
                        PlatformFundDetail platformFundDetail = new PlatformFundDetail();
                        platformFundDetail.setId(CommonUtil.getUUID());
                        platformFundDetail.setCreateTime(new Date());
                        platformFundDetail.setDelFlag("0");
                        platformFundDetail.setRelationId(order.getId());
                        platformFundDetail.setRelationType("0");
                        // 收支类型 0:收入 1:支出
                        platformFundDetail.setType("1");
                        //场景:0:服务费 1:提现手续费 2:违约金 3:违约金调整返还 4:佣金返利 5:银行手续费
                        platformFundDetail.setMoneyScene("3");
                        platformFundDetail.setAmountMoney(money);
                        platformFundDetailMapper.insertSelective(platformFundDetail);
                        // 添加站内信
                        PlatformMessage platformMessage = new PlatformMessage();
                        platformMessage.setId(CommonUtil.getUUID());
                        platformMessage.setCreateTime(new Date());
                        platformMessage.setMemberId(buyMember.getId());
                        platformMessage.setDelFlag(Constants.DelFlag.NORMAL.getType());
                        platformMessage.setTitle("你的违约金已经退还"+ money + "元");
                        platformMessage.setContent("尊敬的爱店猫用户：\n" +
                                "你的订单违约金经我方审核，退还" + money + "元，您的账户可用余额" + buyMember.getMoney() + "元。");
                        platformMessage.setStatus(Constants.PlatformMessageStatus.TYPE0.getType());
                        platformMessageMapper.insertSelective(platformMessage);
                        code = Constants.SUCCESS;
                        msg = "违约金调整返还成功";
                    }
                } else if (fundAdjust.getRelationType().equals("2")) {   // 充值
                    if ("4".equals(fundAdjust.getMoneyScene())) {
                        Alipay alipay = alipayMapper.selectByPrimaryKey(fundAdjust.getRelationId());
                        if (alipay == null) {
                            code = "-20";
                            msg = "充值信息不存在，请核对信息";
                            result.setCode(code);
                            result.setMsg(msg);
                            return result;
                        }
                        Member member = memberMapper.selectByPrimaryKey(alipay.getMemberId());
                        if (member == null) {
                            code = "-22";
                            msg = "该会员不存在,请核对信息";
                            result.setCode(code);
                            result.setMsg(msg);
                            return result;
                        }
                        if (!phone.equals(member.getPhone())) {
                            code = "-23";
                            msg = "充值信息与会员信息不一致，请输入正确的充值ID和手机号";
                            result.setCode(code);
                            result.setMsg(msg);
                            return result;
                        }
                        if (!alipay.getStatus().equals("1")) {
                            code = "-21";
                            msg = "只有审核通过的充值金额才能调整";
                            result.setCode(code);
                            result.setMsg(msg);
                            return result;
                        }
                        //  收入
                        if (fundAdjust.getIncomeType().equals("0")) {
                            // 修改会员资金
                            BigDecimal money1 = member.getMoney().add(money);
                            member.setMoney(money1);
                            memberMapper.updateByPrimaryKeySelective(member);
                            // 添加会员资金调整记录
                            fundAdjust.setId(CommonUtil.getUUID());
                            fundAdjust.setCreateTime(new Date());
                            fundAdjust.setDelFlag("0");
                            fundAdjust.setMemberId(alipay.getMemberId());
                            fundAdjust.setAdminId(adminId);
                            fundAdjust.setFundType("0");
                            memberFundAdjustMapper.insertSelective(fundAdjust);
                            //添加会员收支明细
                            IncomeExpensesDetail expensesDetail = new IncomeExpensesDetail();
                            expensesDetail.setId(CommonUtil.getUUID());
                            expensesDetail.setCreateTime(new Date());
                            expensesDetail.setDelFlag("0");
                            // 收支类型 0:收入 1:支出
                            expensesDetail.setType("0");
                            //场景:0:充值 1:佣金返利 2:差价返利 3:违约定金返还 4:违约金调整返还 5:支付余额返还
                            //6:全款购买 7:店铺余额 8:定金 9:违约金 10:提现 11:交易成功 12:违约全款返还 13:差价返利扣除 14:充值调整
                            expensesDetail.setMoneyScene("14");
                            expensesDetail.setAmountMoney(money);
                            expensesDetail.setMemberId(alipay.getMemberId());
                            incomeExpensesDetailMapper.insertSelective(expensesDetail);

                            // 添加站内信
                            PlatformMessage platformMessage = new PlatformMessage();
                            platformMessage.setId(CommonUtil.getUUID());
                            platformMessage.setCreateTime(new Date());
                            platformMessage.setMemberId(alipay.getMemberId());
                            platformMessage.setDelFlag(Constants.DelFlag.NORMAL.getType());
                            platformMessage.setTitle(member.getUserName() + "用户，您的账户已经充值调整增加" + money + "元");
                            platformMessage.setContent("尊敬的爱店猫用户：\n" +
                                    "　 " + member.getUserName() + "，您的账户已经充值调整增加" + money + "元，账户可用余额" + money1 + "元。");
                            platformMessage.setStatus(Constants.PlatformMessageStatus.TYPE0.getType());
                            platformMessageMapper.insertSelective(platformMessage);
                            code = Constants.SUCCESS;
                            msg = "充值调整成功";
                        } else if (fundAdjust.getIncomeType().equals("1")) { //  支出
                            // 修改会员资金
                            BigDecimal money1 = member.getMoney().subtract(money);
                            if (money1.compareTo(BigDecimal.ZERO) < 0) {
                                code = "-23";
                                msg = "该会员账户资金不足";
                                result.setCode(code);
                                result.setMsg(msg);
                                return result;
                            }

                            member.setMoney(money1);
                            memberMapper.updateByPrimaryKeySelective(member);
                            // 添加会员资金调整记录
                            fundAdjust.setId(CommonUtil.getUUID());
                            fundAdjust.setCreateTime(new Date());
                            fundAdjust.setDelFlag("0");
                            fundAdjust.setMemberId(alipay.getMemberId());
                            fundAdjust.setAdminId(adminId);
                            fundAdjust.setFundType("0");
                            memberFundAdjustMapper.insertSelective(fundAdjust);
                            // 添加会员收支明细
                            IncomeExpensesDetail expensesDetail = new IncomeExpensesDetail();
                            expensesDetail.setId(CommonUtil.getUUID());
                            expensesDetail.setCreateTime(new Date());
                            expensesDetail.setDelFlag("0");
                            // 收支类型 0:收入 1:支出
                            expensesDetail.setType("1");
                            //场景:0:充值 1:佣金返利 2:差价返利 3:违约定金返还 4:违约金调整返还 5:支付余额返还
                            //6:全款购买 7:店铺余额 8:定金 9:违约金 10:提现 11:交易成功 12:违约全款返还 13:差价返利扣除 14:充值调整
                            expensesDetail.setMoneyScene("14");
                            expensesDetail.setAmountMoney(money);
                            expensesDetail.setMemberId(alipay.getMemberId());
                            incomeExpensesDetailMapper.insertSelective(expensesDetail);
                            // 添加站内信
                            PlatformMessage platformMessage = new PlatformMessage();
                            platformMessage.setId(CommonUtil.getUUID());
                            platformMessage.setCreateTime(new Date());
                            platformMessage.setMemberId(alipay.getMemberId());
                            platformMessage.setDelFlag(Constants.DelFlag.NORMAL.getType());
                            platformMessage.setTitle(member.getUserName() + "用户，您的账户已经充值调整减少" + money + "元");
                            platformMessage.setContent("尊敬的爱店猫用户：\n" +
                                    "　 " + member.getUserName() + "，您的账户已经充值调整减少" + money + "元，账户可用余额" + money1 + "元。");
                            platformMessage.setStatus(Constants.PlatformMessageStatus.TYPE0.getType());
                            platformMessageMapper.insertSelective(platformMessage);
                            code = Constants.SUCCESS;
                            msg = "充值调整成功";
                        }
                    }else if("5".equals(fundAdjust.getMoneyScene())){
                        // 线下充值调整
                        Remittance remittance = remittanceMapper.selectByPrimaryKey(fundAdjust.getRelationId());
                        if(remittance == null){
                            code = "-20";
                            msg = "线下充值信息不存在，请核对信息";
                            result.setCode(code);
                            result.setMsg(msg);
                            return result;
                        }
                        Member member = memberMapper.selectByPrimaryKey(remittance.getMemberId());
                        if(member == null){
                            code = "-22";
                            msg = "该会员不存在,请核对信息";
                            result.setCode(code);
                            result.setMsg(msg);
                            return result;
                        }
                        if (!phone.equals(member.getPhone())){
                            code  = "-19";
                            msg = "充值信息与会员信息不一致，请输入正确的充值ID和手机号";
                            result.setCode(code);
                            result.setMsg(msg);
                            return result;
                        }
                        if(!remittance.getStatus().equals("1")){
                            code  = "-21";
                            msg ="只有审核通过的充值金额才能调整";
                            result.setCode(code);
                            result.setMsg(msg);
                            return result;
                        }
                        //  收入
                        if (fundAdjust.getIncomeType().equals("0")) {
                            // 修改会员资金
                            BigDecimal money1 = member.getMoney().add(money);
                            member.setMoney(money1);
                            memberMapper.updateByPrimaryKeySelective(member);
                            // 添加会员资金调整记录
                            fundAdjust.setId(CommonUtil.getUUID());
                            fundAdjust.setCreateTime(new Date());
                            fundAdjust.setDelFlag("0");
                            fundAdjust.setMemberId(remittance.getMemberId());
                            fundAdjust.setAdminId(adminId);
                            fundAdjust.setFundType("0");
                            memberFundAdjustMapper.insertSelective(fundAdjust);
                            //添加会员收支明细
                            IncomeExpensesDetail expensesDetail = new IncomeExpensesDetail();
                            expensesDetail.setId(CommonUtil.getUUID());
                            expensesDetail.setCreateTime(new Date());
                            expensesDetail.setDelFlag("0");
                            // 收支类型 0:收入 1:支出
                            expensesDetail.setType("0");
                            //场景:0:充值 1:佣金返利 2:差价返利 3:违约定金返还 4:违约金调整返还 5:支付余额返还
                            //6:全款购买 7:店铺余额 8:定金 9:违约金 10:提现 11:交易成功 12:违约全款返还 13:差价返利扣除 14:充值调整
                            expensesDetail.setMoneyScene("14");
                            expensesDetail.setAmountMoney(money);
                            expensesDetail.setMemberId(remittance.getMemberId());
                            incomeExpensesDetailMapper.insertSelective(expensesDetail);

                            // 添加站内信
                            PlatformMessage platformMessage = new PlatformMessage();
                            platformMessage.setId(CommonUtil.getUUID());
                            platformMessage.setCreateTime(new Date());
                            platformMessage.setMemberId(remittance.getMemberId());
                            platformMessage.setDelFlag(Constants.DelFlag.NORMAL.getType());
                            platformMessage.setTitle(member.getUserName() + "用户，您的账户已经充值调整增加" + money + "元");
                            platformMessage.setContent("尊敬的爱店猫用户：\n" +
                                    "　 " + member.getUserName() + "，您的账户已经充值调整增加" + money + "元，账户可用余额" + money1 + "元。");
                            platformMessage.setStatus(Constants.PlatformMessageStatus.TYPE0.getType());
                            platformMessageMapper.insertSelective(platformMessage);
                            code = Constants.SUCCESS;
                            msg = "充值调整成功";
                        } else if (fundAdjust.getIncomeType().equals("1")) { //  支出
                            // 修改会员资金
                            BigDecimal money1 = member.getMoney().subtract(money);
                            if (money1.compareTo(BigDecimal.ZERO) < 0) {
                                code = "-23";
                                msg = "该会员账户资金不足";
                                result.setCode(code);
                                result.setMsg(msg);
                                return result;
                            }
                            member.setMoney(money1);
                            memberMapper.updateByPrimaryKeySelective(member);
                            // 添加会员资金调整记录
                            fundAdjust.setId(CommonUtil.getUUID());
                            fundAdjust.setCreateTime(new Date());
                            fundAdjust.setDelFlag("0");
                            fundAdjust.setMemberId(remittance.getMemberId());
                            fundAdjust.setAdminId(adminId);
                            fundAdjust.setFundType("0");
                            memberFundAdjustMapper.insertSelective(fundAdjust);
                            // 添加会员收支明细
                            IncomeExpensesDetail expensesDetail = new IncomeExpensesDetail();
                            expensesDetail.setId(CommonUtil.getUUID());
                            expensesDetail.setCreateTime(new Date());
                            expensesDetail.setDelFlag("0");
                            // 收支类型 0:收入 1:支出
                            expensesDetail.setType("1");
                            //场景:0:充值 1:佣金返利 2:差价返利 3:违约定金返还 4:违约金调整返还 5:支付余额返还
                            //6:全款购买 7:店铺余额 8:定金 9:违约金 10:提现 11:交易成功 12:违约全款返还 13:差价返利扣除 14:充值调整
                            expensesDetail.setMoneyScene("14");
                            expensesDetail.setAmountMoney(money);
                            expensesDetail.setMemberId(remittance.getMemberId());
                            incomeExpensesDetailMapper.insertSelective(expensesDetail);
                            // 添加站内信
                            PlatformMessage platformMessage = new PlatformMessage();
                            platformMessage.setId(CommonUtil.getUUID());
                            platformMessage.setCreateTime(new Date());
                            platformMessage.setMemberId(remittance.getMemberId());
                            platformMessage.setDelFlag(Constants.DelFlag.NORMAL.getType());
                            platformMessage.setTitle(member.getUserName() + "用户，您的账户已经充值调整减少" + money + "元");
                            platformMessage.setContent("尊敬的爱店猫用户：\n" +
                                    "　 " + member.getUserName() + "，您的账户已经充值调整减少" + money + "元，账户可用余额" + money1 + "元。");
                            platformMessage.setStatus(Constants.PlatformMessageStatus.TYPE0.getType());
                            platformMessageMapper.insertSelective(platformMessage);
                            code = Constants.SUCCESS;
                            msg = "充值调整成功";
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
