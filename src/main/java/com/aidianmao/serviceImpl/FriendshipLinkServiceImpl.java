package com.aidianmao.serviceImpl;

import com.aidianmao.entity.FriendshipLink;
import com.aidianmao.mapper.FriendshipLinkMapper;
import com.aidianmao.service.FriendshipLinkService;
import com.aidianmao.utils.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
@Service
@Transactional
public class FriendshipLinkServiceImpl implements FriendshipLinkService {

    @Autowired
    private FriendshipLinkMapper friendshipLinkMapper;


    @Override
    public Result pageList(Integer pageNum,Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<FriendshipLink> link = friendshipLinkMapper.selectAll();
            PageInfo<FriendshipLink> page = new PageInfo(link);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result insertFriendshipLink(FriendshipLink friendshipLink) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
          if(StringUtils.isBlank(friendshipLink.getLinkName())){
              code = "-3";
              msg = "链接名称不能为空";
          } else if (StringUtils.isBlank(friendshipLink.getLinkUrl())) {
              code = "-4";
              msg = "链接访问路径不能为空";
          }else if(friendshipLink.getPriority() == null){
              code = "-5";
              msg = "排序不能为空";
          }else if(!PatternUtil.patternString(friendshipLink.getPriority().toString(),"num")){
              code = "-6";
              msg = "排序必须是数字";
          } else {
               friendshipLink.setId(CommonUtil.getUUID());
               friendshipLink.setCreateTime(new Date());
               friendshipLink.setDelFlag("0");
               friendshipLinkMapper.insertSelective(friendshipLink);
              code = Constants.SUCCESS;
              msg = "添加成功";
          }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result update(FriendshipLink friendshipLink) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(friendshipLink.getId())){
                code = "-3";
                msg = "主键id不能为空";
            }else {
                FriendshipLink friendshipLink1 = friendshipLinkMapper.selectByPrimaryKey(friendshipLink.getId());
                if(friendshipLink1 == null){
                    code = "-4";
                    msg = "修改的友情链接不存在";
                    result.setCode(code);
                    result.setMsg(msg);
                    return result;
                }else {
                    friendshipLinkMapper.updateByPrimaryKeySelective(friendshipLink);
                    code = Constants.SUCCESS;
                    msg = "修改成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result updateDel(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键id不能为空";
            }else {
                FriendshipLink friendshipLink = friendshipLinkMapper.selectByPrimaryKey(id);
                if(friendshipLink == null){
                    code = "-4";
                    msg = "友情链接不存在";
                    result.setCode(code);
                    result.setMsg(msg);
                    return result;
                }else {
                    friendshipLink.setDelFlag("1");
                    friendshipLinkMapper.updateByPrimaryKeySelective(friendshipLink);
                    code = Constants.SUCCESS;
                    msg = "禁用成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result updateDelFlag(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键id不能为空";
            }else {
                FriendshipLink friendshipLink = friendshipLinkMapper.selectByPrimaryKey(id);
                if(friendshipLink == null){
                    code = "-4";
                    msg = "友情链接不存在";
                    result.setCode(code);
                    result.setMsg(msg);
                    return result;
                }else {
                    friendshipLink.setDelFlag("0");
                    friendshipLinkMapper.updateByPrimaryKeySelective(friendshipLink);
                    code = Constants.SUCCESS;
                    msg = "启用成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result delete(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键id不能为空";
            }else {
                friendshipLinkMapper.deleteByPrimaryKey(id);
                    code = Constants.SUCCESS;
                    msg = "删除成功";
                }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

}
