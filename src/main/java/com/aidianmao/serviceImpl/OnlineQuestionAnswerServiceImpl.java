package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Member;
import com.aidianmao.entity.OnlineQuestionAnswer;
import com.aidianmao.mapper.MemberMapper;
import com.aidianmao.mapper.OnlineQuestionAnswerMapper;
import com.aidianmao.service.OnlineQuestionAnswerService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.ReactiveSetCommands;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.ModelMap;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class OnlineQuestionAnswerServiceImpl implements OnlineQuestionAnswerService{

    @Autowired
    private OnlineQuestionAnswerMapper onlineQuestionAnswerMapper;

    @Autowired
    private MemberMapper memberMapper;

    @Override
    public Result pageList(Integer pageNum, Integer pageSize,Map<String, Object> params){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            PageHelperNew.startPage(pageNum,pageSize);
            List<ModelMap> AnswerList = onlineQuestionAnswerMapper.selectAll(params);
            PageInfo<ModelMap> page = new PageInfo<>(AnswerList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result insert(OnlineQuestionAnswer onlineQuestionAnswer,String aidianmaoAdminToken){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if(StringUtils.isBlank(onlineQuestionAnswer.getTitle())){
                code = "-3";
                msg = "提问标题不能为空";
            }else if (StringUtils.isBlank(onlineQuestionAnswer.getQuestion())){
                code = "-4";
                msg = "提问内容不能为空";
            } else {
                if(StringUtils.isNotBlank(onlineQuestionAnswer.getMemberId())){
                    Member member = memberMapper.selectByPrimaryKey(onlineQuestionAnswer.getMemberId());
                    if(member == null){
                        code = "-5";
                        msg = "提问人不存在";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    }
                    onlineQuestionAnswer.setAdminId(adminId);
                    onlineQuestionAnswer.setStatus("0");
                    onlineQuestionAnswer.setId(CommonUtil.getUUID());
                    onlineQuestionAnswer.setDelFlag("0");
                    onlineQuestionAnswer.setCreateTime(new Date());
                   onlineQuestionAnswerMapper.insert(onlineQuestionAnswer);
                   code = Constants.SUCCESS;
                   msg = "成功";
            }
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

   @Override
    public Result update(OnlineQuestionAnswer onlineQuestionAnswer,String aidianmaoAdminToken){
       Result result = new Result();
       String code = Constants.FAIL;
       String msg = "初始化";
       try {
           String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
           if (StringUtils.isBlank(onlineQuestionAnswer.getId())) {
               code = "-3";
               msg = "主键不能为空";
           } else if (StringUtils.isBlank(onlineQuestionAnswer.getAnswer())) {
               code = "-4";
               msg = "回答内容不能为空";
           }else if(StringUtils.isBlank(onlineQuestionAnswer.getTitle())){
               code = "-5";
               msg = "提问标题不能为空";
           }else if(StringUtils.isBlank(onlineQuestionAnswer.getQuestion())){
               code = "-6";
               msg = "提问问题不能为空";
           } else{
               OnlineQuestionAnswer questionAnswer = onlineQuestionAnswerMapper.selectByPrimaryKey(onlineQuestionAnswer.getId());
               if (questionAnswer == null){
                   code = "-5";
                   msg = "修改的问题不存在";
               }else {
                   onlineQuestionAnswer.setAdminId(adminId);
                   onlineQuestionAnswer.setStatus("1");
                   onlineQuestionAnswer.setAnswerTime(new Date());
                   onlineQuestionAnswerMapper.updateByPrimaryKeySelective(onlineQuestionAnswer);
                   code = Constants.SUCCESS;
                   msg = "修改成功";
               }
           }
       } catch (Exception e) {
           e.printStackTrace();
           code = Constants.ERROR;
           msg = "系统繁忙";
       }
       result.setCode(code);
       result.setMsg(msg);
       return result;
   }

    @Override
    public Result deleteQuestion(String id,String aidianmaoAdminToken){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "请选择删除对象";
            }else {
                OnlineQuestionAnswer onlineAnswer = onlineQuestionAnswerMapper.selectByPrimaryKey(id);
                if(onlineAnswer == null){
                    code = "-4";
                    msg = "删除对象不存在";
                }else {
                    onlineAnswer.setAdminId(adminId);
                    onlineAnswer.setDelFlag("1");
                    onlineQuestionAnswerMapper.updateByPrimaryKeySelective(onlineAnswer);
                    code = Constants.SUCCESS;
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result detail(String id){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "查看提问信息不存在";
            }else {
                Map<String,Object> map = onlineQuestionAnswerMapper.selectById(id);
                result.setData(map);
                code = Constants.SUCCESS;
                msg = "查询成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
