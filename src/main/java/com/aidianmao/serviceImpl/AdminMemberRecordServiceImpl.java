package com.aidianmao.serviceImpl;

import com.aidianmao.entity.AdminMemberRecord;
import com.aidianmao.mapper.AdminMemberRecordMapper;
import com.aidianmao.service.AdminMemberRecordService;
import com.aidianmao.utils.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class AdminMemberRecordServiceImpl implements AdminMemberRecordService {
    @Autowired
    private AdminMemberRecordMapper adminMemberRecordMapper;

    @Override
    public Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum,pageSize);
            List<Map<String, Object>> list = adminMemberRecordMapper.selectAll(params);
            PageInfo<Map<String,Object>> page = new PageInfo<>(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result insert(String memberId,Integer operationType,String aidianmaoAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if(StringUtils.isBlank(memberId)){
                code = "-3";
                msg = "会员ID不能为空";
            }else if(operationType==null|| (operationType!=1&&operationType!=2)){
                code = "-3";
                msg = "操作类型不能为空或类型错误";
            } else {
                AdminMemberRecord adminMemberRecord = new AdminMemberRecord();
                if(operationType==1){
                    adminMemberRecord.setImpactTime(new Date());
                }else if (operationType==2){
                    adminMemberRecord.setIngoingTime(new Date());
                }
                adminMemberRecord.setMemberId(memberId);
                adminMemberRecord.setAdminId(adminId);
                adminMemberRecord.setId(CommonUtil.getUUID());
                adminMemberRecord.setCreateTime(new Date());
                adminMemberRecord.setDelFlag("0");
                adminMemberRecordMapper.insertSelective(adminMemberRecord);
                code = Constants.SUCCESS;
                msg = "新增成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
