package com.aidianmao.serviceImpl;

import com.aidianmao.entity.TmallCostRule;
import com.aidianmao.mapper.TmallCostRuleMapper;
import com.aidianmao.service.TmallCostRuleService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class TmallCostRuleServiceImpl implements TmallCostRuleService {
    @Autowired
    private TmallCostRuleMapper tmallCostRuleMapper;
    @Override
    public Result pageList(Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum,pageSize);
            List<TmallCostRule> list = tmallCostRuleMapper.selectAll();
            PageInfo<Map<String,Object>> page = new PageInfo(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result detail(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            }else {
                TmallCostRule tmallCostRule = tmallCostRuleMapper.selectByPrimaryKey(id);
                if(tmallCostRule != null){
                    result.setData(tmallCostRule);
                    code = Constants.SUCCESS;
                    msg = "查询成功";
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result delete(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            }else {
                TmallCostRule tmallCostRule = tmallCostRuleMapper.selectByPrimaryKey(id);
                if(tmallCostRule != null){
                    tmallCostRuleMapper.deleteByPrimaryKey(id);
                    code = Constants.SUCCESS;
                    msg = "删除成功";
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result update(TmallCostRule record) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(record.getId())){
                code = "-3";
                msg = "主键ID不能为空";
            }else {
                TmallCostRule tmallCostRule = tmallCostRuleMapper.selectByPrimaryKey(record.getId());
                if(tmallCostRule != null){
                    if(record.getBuyerRate()!=null && !(record.getBuyerRate().compareTo(BigDecimal.ZERO)>=0 && record.getBuyerRate().compareTo(BigDecimal.ONE)<=0)){
                        code = "-4";
                        msg = "买家费率不能小于0大于1";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    tmallCostRuleMapper.updateByPrimaryKeySelective(record);
                    code = Constants.SUCCESS;
                    msg = "更新成功";
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result insert(TmallCostRule record) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(record.getBuyerDiscount()==null || record.getBuyerDiscount().compareTo(BigDecimal.ZERO)<0 || record.getBuyerDiscount().compareTo(BigDecimal.ONE)>0){
                code = "-3";
                msg = "买家折扣不能为空并且不能小于1大于0";
            }else if(record.getSellerDiscount() == null || record.getSellerDiscount().compareTo(BigDecimal.ZERO)<0 ||record.getSellerDiscount().compareTo(BigDecimal.ONE) >0){
                code = "-4";
                msg = "卖家折扣不能为空并且不能小于1大于0";
           }else if(StringUtils.isBlank(record.getNum())){
                code = "-5";
                msg = "序号不能为空";
            }else {
                TmallCostRule costRule = tmallCostRuleMapper.selectByDiscount(record);
                if(costRule != null){
                    code = "-6";
                    msg = "该规则已存在";
                    result.setCode(code);
                    result.setMsg(msg);
                    return result;
                }
                TmallCostRule tmallCostRule = tmallCostRuleMapper.selectByNum(record.getNum());
                if(tmallCostRule != null){
                    code = "-6";
                    msg = "该序号存在，请重新输入";
                    result.setCode(code);
                    result.setMsg(msg);
                    return result;
                }
                record.setId(CommonUtil.getUUID());
                record.setCreateTime(new Date());
                record.setDelFlag("0");
                tmallCostRuleMapper.insertSelective(record);
                code = Constants.SUCCESS;
                msg = "新增成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
