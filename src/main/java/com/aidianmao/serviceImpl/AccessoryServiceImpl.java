package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Accessory;
import com.aidianmao.mapper.AccessoryMapper;
import com.aidianmao.service.AccessoryService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.Result;
import com.github.tobato.fastdfs.domain.StorePath;
import com.github.tobato.fastdfs.proto.storage.DownloadByteArray;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Date;
import java.util.UUID;

@Transactional
@Service
public class AccessoryServiceImpl implements AccessoryService{
    @Autowired
    private FastFileStorageClient fastFileStorageClient;
    @Autowired
    private AccessoryMapper accessoryMapper;
    @Override
    public Result addArticleCover(MultipartFile file) {
        Result result = new Result();
        String msg = "初始化";
        String code = Constants.FAIL;
        try {
            if(file!=null && !file.isEmpty()){
                StorePath storePath = fastFileStorageClient.uploadFile(file.getInputStream(), file.getSize(), FilenameUtils.getExtension(file.getOriginalFilename()), null);
                String fileUrl = storePath.getFullPath();
                String[] fileName = fileUrl.split("/");
                String name= fileName[fileName.length-1];
                String prefix = name.substring(name.lastIndexOf(".") + 1);
                if (CommonUtil.isImage(prefix)) {
                    Accessory accessory = new Accessory();
                    accessory.setId(UUID.randomUUID().toString().replace("-", ""));
                    accessory.setDelFlag("0");
                    accessory.setName(name);
                    accessory.setCreateTime(new Date());
                    accessory.setFilePath(storePath.getPath());
                    accessory.setGroupName(storePath.getGroup());
                    accessory.setFileRealName(file.getOriginalFilename());
                    accessory.setExt(fileUrl.substring(fileUrl.lastIndexOf(".") + 1));
                    accessoryMapper.insertSelective(accessory);
                    result.setData(accessory);
                    code = "0";
                    msg = "成功";
                } else {
                    code = "-3";
                    msg = "只能上传图片";
                }
            }else{
                code = "-4";
                msg = "请选择上传图片";
            }
        } catch (Exception e) {
            code = "-2";
            msg = "上传出错";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

	@Override
	public Result addSelfProductDetailPhoto(MultipartFile file) {
		 Result result = new Result();
	        String msg = "初始化";
	        String code = Constants.FAIL;
	        try {
	            String fileName = file.getOriginalFilename();
	            String prefix = fileName.substring(fileName.lastIndexOf(".") + 1);
	            if (CommonUtil.isImage(prefix)) {
	                StorePath storePath = fastFileStorageClient.uploadFile(file.getInputStream(), file.getSize(), FilenameUtils.getExtension(file.getOriginalFilename()), null);
	                String fileUrl = storePath.getFullPath();
	                result.setData(Constants.FASTDFS_SERVER_IP+"/"+fileUrl);
	                code = "0";
	                msg = "成功";
	            } else {
	                code = "-3";
	                msg = "只能上传图片";
	            }
	        } catch (Exception e) {
	            code = "-2";
	            msg = "上传出错";
	            e.printStackTrace();
	        }
	        result.setMsg(msg);
	        result.setCode(code);
	        return result;
	}

    @Override
    public Result downloads(String accessoryId, HttpServletResponse response) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        OutputStream outputStream = null;
        byte[] b= new byte[1024];
        int len = 0;
        try {
            Accessory accessory = accessoryMapper.selectByPrimaryKey(accessoryId);
            if (accessory != null){
                byte[] bytes = null;
                bytes  = fastFileStorageClient.downloadFile(accessory.getGroupName(), accessory.getFilePath(), new DownloadByteArray());
                InputStream inputStream = new ByteArrayInputStream(bytes);
                outputStream = response.getOutputStream();
                String filename = accessory.getFileRealName();
                response.addHeader("Content-Disposition","attachment; filename=" + URLEncoder.encode(filename, "UTF-8"));

                while((len = inputStream.read(b)) != -1){
                    outputStream.write(b, 0, len);
                }
                code = Constants.SUCCESS;
                msg = "成功";
            } else {
                code = "-3";
                msg = "该附件不存在";
            }
        } catch (Exception e){
            code = Constants.ERROR;
            msg = "系统繁忙";
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
