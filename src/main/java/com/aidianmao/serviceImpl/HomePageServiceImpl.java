package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Member;
import com.aidianmao.entity.PlatformFundDetail;
import com.aidianmao.entity.WithdrawDeposit;
import com.aidianmao.mapper.*;
import com.aidianmao.service.HomePageService;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class HomePageServiceImpl implements HomePageService {

    @Autowired
    private TmallOrderMapper tmallOrderMapper;
    @Autowired
    private TmallMapper tmallMapper;
    @Autowired
    private MemberMapper memberMapper;
    @Autowired
    private PlatformFundDetailMapper platformFundDetailMapper;
    @Autowired
    private WithdrawDepositMapper withdrawDepositMapper;


    @Override
    public Result operationalData() {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
           Map<String,Object> map = new HashMap<>();
           Integer orderDayData = tmallOrderMapper.selectByDay();
           Integer orderMonthDate = tmallOrderMapper.selectMonth();
           Integer orderUnpaidDate = tmallOrderMapper.selectUnpaid();
           map.put("orderDayData",orderDayData);
           map.put("orderMonthDate",orderMonthDate);
           map.put("orderUnpaidDate",orderUnpaidDate);
           Integer tmallDayData = tmallMapper.selectByDay();
           Integer tmallMonthData = tmallMapper.selectByMonth();
           Integer tmallDayYesterday = tmallMapper.selectByYesterday();
           map.put("tmallDayData",tmallDayData);
           map.put("tmallMonthData",tmallMonthData);
           map.put("tmallDayYesterday",tmallDayYesterday);
           Integer memberCount = memberMapper.selectByCount();
           Integer memberMonth = memberMapper.selectByMonth();
           Integer memberWeek = memberMapper.selectByDays();
           Integer memberDay = memberMapper.selectByDay();
           map.put("memberCount",memberCount);
           map.put("memberMonth",memberMonth);
           map.put("memberWeek",memberWeek);
           map.put("memberDay",memberDay);
           result.setData(map);
           code = Constants.SUCCESS;
           msg = "查询成功";
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result  finance(Map<String,Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            Map<String,Object> map = new HashMap<>();
            List<PlatformFundDetail> list = platformFundDetailMapper.selectAll(params);
            List<Member> members = memberMapper.selectAll(params);
            List<WithdrawDeposit> withdrawDeposits = withdrawDepositMapper.selectAllWithdraw(params);
            //ServiceCharge:服务费,CommissionRebate:佣金返利,LiquidatedDamages:违约金,AdjustmentLiquidatedDamages:违约金调整返还,
            // PresentationFee:提现手续费,BankCharges:银行手续费,TotalIncome:总收入.
            BigDecimal ServiceCharge = BigDecimal.ZERO;
            BigDecimal CommissionRebate = BigDecimal.ZERO;
            BigDecimal LiquidatedDamages = BigDecimal.ZERO;
            BigDecimal AdjustmentLiquidatedDamages = BigDecimal.ZERO;
            BigDecimal PresentationFee = BigDecimal.ZERO;
            BigDecimal BankCharges = BigDecimal.ZERO;
            BigDecimal TotalIncome = BigDecimal.ZERO;
            BigDecimal memberMoney = BigDecimal.ZERO;
            BigDecimal memberFreezeMoney = BigDecimal.ZERO;
            BigDecimal withdrawMoney = BigDecimal.ZERO;
            if(list.size()>0){
                for (PlatformFundDetail platformFundDetail: list) {
                    if(platformFundDetail.getMoneyScene()==null||platformFundDetail.getAmountMoney()==null){
                        code = Constants.ERROR;
                        msg = "平台收支场景或平台收支金额为空";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    //场景:0:服务费 1:提现手续费 2:违约金 3:违约金调整返还 4:佣金返利 5:银行手续费
                    switch (Integer.valueOf(platformFundDetail.getMoneyScene())){
                        case 0: ServiceCharge = ServiceCharge.add(platformFundDetail.getAmountMoney());
                            break;
                        case 1: PresentationFee = PresentationFee.add(platformFundDetail.getAmountMoney());
                            break;
                        case 2: LiquidatedDamages = LiquidatedDamages.add(platformFundDetail.getAmountMoney());
                            break;
                        case 3: AdjustmentLiquidatedDamages = AdjustmentLiquidatedDamages.add(platformFundDetail.getAmountMoney());
                            break;
                        case 4: CommissionRebate = CommissionRebate.add(platformFundDetail.getAmountMoney());
                            break;
                        case 5: BankCharges = BankCharges.add(platformFundDetail.getAmountMoney());
                            break;
                    }
                }
                //公司总收入=服务费+违约金+提现手续费-佣金返利-违约金调整返还-银行手续费
                TotalIncome = ServiceCharge.add(LiquidatedDamages).add(PresentationFee).subtract(CommissionRebate).subtract(AdjustmentLiquidatedDamages).subtract(BankCharges);
            }
            map.put("TotalIncome",TotalIncome);
            map.put("ServiceCharge",ServiceCharge);
            map.put("PresentationFee",PresentationFee);
            map.put("LiquidatedDamages",LiquidatedDamages);
            map.put("AdjustmentLiquidatedDamages",AdjustmentLiquidatedDamages);
            map.put("CommissionRebate",CommissionRebate);
            map.put("BankCharges",BankCharges);

            if(members.size()>0){
             for(Member member : members){
                 if(member.getMoney() == null){
                     code = Constants.ERROR;
                     msg = "会员资金为空";
                     result.setCode(code);
                     result.setMsg(msg);
                     return result;
                 }
                 if(member.getFreezeMoney() == null){
                     code = Constants.ERROR;
                     msg = "会员冻结资金为空";
                     result.setCode(code);
                     result.setMsg(msg);
                     return result;
                 }
               memberMoney = memberMoney.add(member.getMoney());
               memberFreezeMoney =memberFreezeMoney.add(member.getFreezeMoney());
             }
            }
            map.put("memberMoney",memberMoney);
            map.put("memberFreezeMoney",memberFreezeMoney);
            if(withdrawDeposits.size()>0){
                for(WithdrawDeposit withdrawDeposit : withdrawDeposits){
                    if(withdrawDeposit.getMoney() == null){
                        code = Constants.ERROR;
                        msg = "提现金额为空";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    withdrawMoney = withdrawMoney.add(withdrawDeposit.getMoney());
                }
            }
            map.put("withdrawMoney",withdrawMoney);
            result.setData(map);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


}
