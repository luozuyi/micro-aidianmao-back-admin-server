package com.aidianmao.serviceImpl;


import com.aidianmao.entity.BusinessCooperation;
import com.aidianmao.mapper.BusinessCooperationMapper;
import com.aidianmao.mapper.CitysMapper;
import com.aidianmao.service.BusinessCooperationService;
import com.aidianmao.utils.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class BusinessCooperationServiceImpl implements BusinessCooperationService {

    @Autowired
    private BusinessCooperationMapper businessCooperationMapper;

    @Autowired
    private CitysMapper citysMapper;
    @Override
    public Result pageList(Integer pageNum, Integer pageSize, Map<String,Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum,pageSize);
            List<ModelMap> list = businessCooperationMapper.selectAll(params);
            PageInfo<ModelMap> page = new PageInfo(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }




    @Override
    public Result detail(String id){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code ="-3";
                msg = "查询对象不存在";
            }else {
                Map<String,Object> map = businessCooperationMapper.selectById(id);
                result.setData(map);
                code = Constants.SUCCESS;
                msg = "查询成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

}
