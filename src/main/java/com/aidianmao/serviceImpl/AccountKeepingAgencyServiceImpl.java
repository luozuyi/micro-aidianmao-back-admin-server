package com.aidianmao.serviceImpl;

import com.aidianmao.entity.AccountKeepingAgency;
import com.aidianmao.mapper.AccountKeepingAgencyMapper;
import com.aidianmao.service.AccountKeepingAgencyService;
import com.aidianmao.utils.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.ModelMap;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class AccountKeepingAgencyServiceImpl implements AccountKeepingAgencyService {
    @Autowired
    private AccountKeepingAgencyMapper accountKeepingAgencyMapper;
    @Override
    public Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<ModelMap> list = accountKeepingAgencyMapper.selectAll(params);
            PageInfo<ModelMap> page = new PageInfo<>(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result detail(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            }else {
                AccountKeepingAgency accountKeepingAgency = accountKeepingAgencyMapper.selectByPrimaryKey(id);
                if(accountKeepingAgency!=null){
                    result.setData(accountKeepingAgency);
                    code = Constants.SUCCESS;
                    msg = "查询成功";
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result update(AccountKeepingAgency record) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(record.getId())){
                code = "-3";
                msg = "主键ID不能为空";
            }else if(StringUtils.isBlank(record.getPhone())){
                code = "-4";
                msg = "手机号不能为空";
            }else if(StringUtils.isBlank(record.getCitysName())){
                code = "-5";
                msg = "城市名称不能为空";
            }else if (!PatternUtil.patternString(record.getPhone(),"mobile")){
                code = "-6";
                msg = "手机号格式不正确";
            }
            else {
                AccountKeepingAgency accountKeepingAgency = accountKeepingAgencyMapper.selectByPrimaryKey(record.getId());
                if(accountKeepingAgency!=null){
                    accountKeepingAgencyMapper.updateByPrimaryKeySelective(record);
                    code = Constants.SUCCESS;
                    msg = "更新成功";
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result insert(AccountKeepingAgency record){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            if(StringUtils.isBlank(record.getPhone())){
                code="-3";
                msg="手机号不能为空";
            }else if(StringUtils.isBlank(record.getCitysName())){
                code="-4";
                msg="城市名称不能为空";
            }else {
                if(!PatternUtil.patternString(record.getPhone(),"mobile")){
                    code="-5";
                    msg="手机号格式不正确";
                }
                record.setId(CommonUtil.getUUID());
                record.setCreateTime(new Date());
                record.setDelFlag("0");
                accountKeepingAgencyMapper.insertSelective(record);
                code = Constants.SUCCESS;
                msg = "新增成功";
            }
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result update(String[] list){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(list == null || list.length == 0){
                code = "-3";
                msg = "没有交接的信息";
            }
                for (String id : list){
                    AccountKeepingAgency agency = accountKeepingAgencyMapper.selectByPrimaryKey(id);
                        agency.setIsTakeOver("0");
                        accountKeepingAgencyMapper.updateByPrimaryKeySelective(agency);
                        code = Constants.SUCCESS;
                        msg = "交接成功";
                }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result cancelTakeOver(String[] list){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
              if(list == null || list.length ==0){
                  code = "-4";
                  msg = "没有取消交接的信息";
              }
            for (String id : list) {
                AccountKeepingAgency agency = accountKeepingAgencyMapper.selectByPrimaryKey(id);
                    agency.setIsTakeOver("1");
                    accountKeepingAgencyMapper.updateByPrimaryKeySelective(agency);
                    code = Constants.SUCCESS;
                    msg = "取消交接成功";
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
