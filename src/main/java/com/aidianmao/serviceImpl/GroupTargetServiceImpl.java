package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Admin;
import com.aidianmao.entity.Group;
import com.aidianmao.entity.GroupTarget;
import com.aidianmao.mapper.AdminMapper;
import com.aidianmao.mapper.DepartmentTargetMapper;
import com.aidianmao.mapper.GroupMapper;
import com.aidianmao.mapper.GroupTargetMapper;
import com.aidianmao.service.GroupTargetService;
import com.aidianmao.utils.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class GroupTargetServiceImpl implements GroupTargetService {
    @Autowired
    private GroupTargetMapper groupTargetMapper;
    @Autowired
    private GroupMapper groupMapper;
    @Autowired
    private AdminMapper adminMapper;


    @Override
    public Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
                PageHelperNew.startPage(pageNum,pageSize);
                List<Map<String, Object>> list = groupTargetMapper.selectAll(params);
                PageInfo<Map<String,Object>> page = new PageInfo<>(list);
                result.setData(page);
                code = Constants.SUCCESS;
                msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result detail(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            }else {
                GroupTarget groupTarget = groupTargetMapper.selectByPrimaryKey(id);
                if(groupTarget!=null){
                    Map<String,Object> map = groupTargetMapper.selectById(id);
                    result.setData(map);
                    code = Constants.SUCCESS;
                    msg = "查询成功";
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result update(GroupTarget record) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(record.getId())) {
                code = "-3";
                msg = "主键ID不能为空";

            }else if(record.getMonthGoal()==null || record.getMonthGoal().compareTo(BigDecimal.ZERO)<=0){
                code = "-4";
                msg = "小组月目标不能为空并且不能为非正数";
            }else if(StringUtils.isBlank(record.getMonth())){
                code = "-5";
                msg = "月份不能为空";
            } else {
                GroupTarget groupTarget = groupTargetMapper.selectByPrimaryKey(record.getId());
                if(groupTarget!=null){
                    groupTargetMapper.updateByPrimaryKeySelective(record);
                    code = Constants.SUCCESS;
                    msg = "更新成功";
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result delete(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            }else {
                GroupTarget groupTarget = groupTargetMapper.selectByPrimaryKey(id);
                if(groupTarget!=null){
                    groupTargetMapper.deleteByPrimaryKey(id);
                    code = Constants.SUCCESS;
                    msg = "删除成功";
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result insert(GroupTarget record,String groupName) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(groupName)){
                code = "-3";
                msg = "组长名不能为空";
            }else if(record.getMonthGoal()==null || record.getMonthGoal().compareTo(BigDecimal.ZERO)<=0){
                code = "-4";
                msg = "小组月目标不能为空并且不能为非正数";
            }else if(StringUtils.isBlank(record.getMonth())){
                code = "-5";
                msg = "月份不能为空";
            }else {
                record.setId(CommonUtil.getUUID());
                record.setCreateTime(new Date());
                record.setDelFlag("0");
                record.setYear(RandomCodeUtil.dateToYear(new Date()));
                Admin admin = adminMapper.selectByAdminName(groupName);
                if(admin == null){
                    code = "-6";
                    msg = "组长信息不存在，请输入正确的信息";
                    result.setCode(code);
                    result.setMsg(msg);
                    return result;
                }
                List<Group> list = groupMapper.selectByLeaderId(admin.getId());
                if(list.size()==1){
                    Group group = list.get(0);
                    record.setLeaderId(group.getHeadManAdminId());
                    record.setGroupId(group.getId());
                    groupTargetMapper.insertSelective(record);
                    code = Constants.SUCCESS;
                    msg = "新增成功";
                }else {
                    code = "-7";
                    msg = "组长的小组信息有问题，小组信息为空或拥有多个小组";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
