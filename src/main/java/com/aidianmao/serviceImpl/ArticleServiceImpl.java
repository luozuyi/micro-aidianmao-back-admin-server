package com.aidianmao.serviceImpl;

import com.aidianmao.entity.*;
import com.aidianmao.mapper.*;
import com.aidianmao.service.ArticleService;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.ModelMap;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;


@Transactional
@Service
public class  ArticleServiceImpl implements ArticleService{
    @Autowired
    private ArticleMapper articleMapper;
    @Autowired
    private ChannelMapper channelMapper;
    @Autowired
    private ChannelArticleTemplateMapper channelArticleTemplateMapper;
    @Autowired
    private ArticleContentMapper articleContentMapper;
    @Autowired
    private AccessoryMapper accessoryMapper;

    @Override
    public Result listPageBySelection(Integer pageNum,Integer pageSize,Map<String,Object> params){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            params.put("delFlag","0");
            PageHelperNew.startPage(pageNum,pageSize);
            List<ModelMap> modelMapList = articleMapper.selectAllBySelection(params);
            for(ModelMap modelData : modelMapList){
                String coverId = modelData.get("coverId")+"";
                if(StringUtils.isNotBlank(coverId)){
                    Accessory cover= accessoryMapper.selectByPrimaryKey(coverId);
                    modelData.addAttribute("cover",cover);
                }
                String articleTemplateId = modelData.get("channelArticleTemplateId")+"";
                if(StringUtils.isNotBlank(articleTemplateId)){
                    ChannelArticleTemplate channelArticleTemplate = channelArticleTemplateMapper.selectByPrimaryKey(articleTemplateId);
                    modelData.addAttribute("channelArticleTemplate",channelArticleTemplate);
                }

            }
            PageInfo<ModelMap> page = new PageInfo<>(modelMapList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result delById(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "请选择要删除的对象";
            }else{
                Article article_db = articleMapper.selectByPrimaryKey(id);
                if(article_db == null){
                    code = "-4";
                    msg = "删除的对象不存在";
                }else{
                    article_db.setDelFlag("1");
                    articleMapper.updateByPrimaryKeySelective(article_db);
                    code = Constants.SUCCESS;
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result addArticle(Article article, String content) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(article.getChannelId())){
                code = "-3";
                msg = "栏目不能为空";
            }else if(StringUtils.isBlank(article.getChannelArticleTemplateId())){
                code = "-4";
                msg = "模板不能为空";
            }else{
                Channel channel = channelMapper.selectByPrimaryKey(article.getChannelId());
                ChannelArticleTemplate channelArticleTemplate = channelArticleTemplateMapper.selectByPrimaryKey(article.getChannelArticleTemplateId());
                if(channel == null){
                    code = "-5";
                    msg = "选择的栏目不存在";
                }else if(channelArticleTemplate == null){
                    code = "-6";
                    msg = "选择的模板不存在";
                }else if(StringUtils.isBlank(content)){
                    code = "-7";
                    msg = "内容不能为空";
                }else if(StringUtils.isBlank(article.getTitle())){
                    code = "-8";
                    msg = "标题不能为空";
                /*}else if(StringUtils.isBlank(article.getCoverId())){
                    code = "-9";
                    msg = "请上传封面";*/
                }else{
                    article.setId(UUID.randomUUID().toString().replace("-", ""));
                    article.setDelFlag("0");
                    article.setCreateTime(new Date());

                    String articleContentId = UUID.randomUUID().toString().replace("-", "");
                    article.setArticleContentId(articleContentId);

                    ArticleContent articleContent = new ArticleContent();
                    articleContent.setId(articleContentId);
                    articleContent.setContent(content);
                    articleContentMapper.insertSelective(articleContent);

                    articleMapper.insertSelective(article);

                    code = Constants.SUCCESS;
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result updateArticle(Article article, String content) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(article.getChannelId())){
                code = "-3";
                msg = "栏目不能为空";
            }else if(StringUtils.isBlank(article.getChannelArticleTemplateId())){
                code = "-4";
                msg = "模板不能为空";
            }else{
                Channel channel = channelMapper.selectByPrimaryKey(article.getChannelId());
                ChannelArticleTemplate channelArticleTemplate = channelArticleTemplateMapper.selectByPrimaryKey(article.getChannelArticleTemplateId());
                Accessory cover = accessoryMapper.selectByPrimaryKey(article.getCoverId());
                if(channel == null){
                    code = "-5";
                    msg = "选择的栏目不存在";
                }else if(channelArticleTemplate == null){
                    code = "-6";
                    msg = "选择的模板不存在";
                }else if(StringUtils.isBlank(content)){
                    code = "-7";
                    msg = "内容不能为空";
                }else if(StringUtils.isBlank(article.getTitle())){
                    code = "-8";
                    msg = "标题不能为空";
                }else if(StringUtils.isBlank(article.getId())){
                    code = "-9";
                    msg = "主键不能为空";
                }else if(cover == null){
                    code = "-11";
                    msg = "请上传封面";
                }else{
                    Article article1_db = articleMapper.selectByPrimaryKey(article.getId());
                    if(article1_db == null){
                        code = "-10";
                        msg = "修改的文章对象不存在";
                    }else{
                        articleMapper.updateByPrimaryKeySelective(article);
                        String articleContentId = article1_db.getArticleContentId();
                        ArticleContent articleContent = articleContentMapper.selectByPrimaryKey(articleContentId);
                        articleContent.setContent(content);
                        articleContentMapper.updateByPrimaryKeySelective(articleContent);
                        code = Constants.SUCCESS;
                        msg = "成功";
                    }
                }
            }
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result toDetailArticle(String id){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            Article article = articleMapper.selectByPrimaryKey(id);
            if (null != article){
                ModelMap modelData= new ModelMap();
                modelData.addAttribute("article",article);

                String channelId = article.getChannelId();
                if(StringUtils.isNotBlank(channelId)){
                    Channel channel = channelMapper.selectByPrimaryKey(channelId);
                    modelData.addAttribute("channel",channel);
                }else {
                    modelData.addAttribute("channel",null);
                }

                String articleTemplateId = article.getChannelArticleTemplateId();
                if(StringUtils.isNotBlank(articleTemplateId)){
                    ChannelArticleTemplate channelArticleTemplate = channelArticleTemplateMapper.selectByPrimaryKey(articleTemplateId);
                    modelData.addAttribute("channelArticleTemplate",channelArticleTemplate);
                }else {
                    modelData.addAttribute("channelArticleTemplate",null);
                }

                String coverId = article.getCoverId();
                if(StringUtils.isNotBlank(coverId)){
                    Accessory cover = accessoryMapper.selectByPrimaryKey(coverId);
                    modelData.addAttribute("cover",cover);
                }else {
                    modelData.addAttribute("cover",null);
                }

                String articleContentId = article.getArticleContentId();
                if(StringUtils.isNotBlank(articleContentId)){
                    ArticleContent articleContent = articleContentMapper.selectByPrimaryKey(articleContentId);
                    modelData.addAttribute("articleContent",articleContent);
                }else {
                    modelData.addAttribute("articleContent",null);
                }

                code = Constants.SUCCESS;
                msg = "成功";
                result.setData(modelData);
            }else {
                msg = "记录不存在";
            }
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
