package com.aidianmao.serviceImpl;

import com.aidianmao.entity.AdminMember;
import com.aidianmao.entity.BarginPrice;
import com.aidianmao.entity.SalespersonConfig;
import com.aidianmao.mapper.AdminMemberMapper;
import com.aidianmao.mapper.BarginPriceMapper;
import com.aidianmao.mapper.SalespersonConfigMapper;
import com.aidianmao.service.BarginPriceService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class BarginPriceServiceImpl implements BarginPriceService {
    @Autowired
    private BarginPriceMapper barginPriceMapper;
    @Autowired
    private AdminMemberMapper adminMemberMapper;
    @Autowired
    private SalespersonConfigMapper salespersonConfigMapper;
    @Override
    public Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum,pageSize);
            List<Map<String, Object>> list = barginPriceMapper.selectAll(params);
            PageInfo<Map<String,Object>> page = new PageInfo<>(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result detail(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            }else {
                BarginPrice barginPrice = barginPriceMapper.selectByPrimaryKey(id);
                if(barginPrice!=null){
                    Map<String,Object> map = barginPriceMapper.selectById(id);
                    result.setData(map);
                    code = Constants.SUCCESS;
                    msg = "查询成功";
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result myRelation(String id,String remark,String aidianmaoAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            }else {
                BarginPrice barginPrice = barginPriceMapper.selectByPrimaryKey(id);
                if(barginPrice==null){
                    code = "-4";
                    msg = "当前数据不存在或主键ID已被修改";
                }else {
                    SalespersonConfig salespersonConfig = salespersonConfigMapper.selectByAdminId(adminId);
                    if(salespersonConfig == null){
                        code = "-4";
                        msg = "请给该业务员加最大客户数";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    AdminMember adminMember = adminMemberMapper.selectByMemberId(barginPrice.getMemberId());
                    if(adminMember != null){
                        code = "-5";
                        msg = "该会员已有业务员";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                        AdminMember adminMember1 = new AdminMember();
                        adminMember1.setId(CommonUtil.getUUID());
                        adminMember1.setDelFlag("0");
                        adminMember1.setCreateTime(new Date());
                        adminMember1.setMemberId(barginPrice.getMemberId());
                        adminMember1.setAdminId(adminId);
                        adminMember1.setRemark(remark);
                        Integer count = adminMemberMapper.selectCount(adminId);
                        if (count < salespersonConfig.getMaximunNum()) {
                            adminMemberMapper.insertSelective(adminMember1);
                            code = Constants.SUCCESS;
                            msg = "我去联系成功";
                        } else {
                            code = "-5";
                            msg = "您的客户数已达到您的最大客户数";
                        }
                    }
            }
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
