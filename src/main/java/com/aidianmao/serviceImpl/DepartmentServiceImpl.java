package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Admin;
import com.aidianmao.entity.Department;
import com.aidianmao.mapper.AdminMapper;
import com.aidianmao.mapper.DepartmentMapper;
import com.aidianmao.service.DepartmentService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class DepartmentServiceImpl implements DepartmentService{
    @Autowired
    private DepartmentMapper departmentMapper;
    @Autowired
    private AdminMapper adminMapper;
    @Override
    public Result addDepartment(Department department) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(department.getDeptName())){
                code = "-3";
                msg = "部门名称不能为空";
            }else{
                if(StringUtils.isNotBlank(department.getHeadManAdminId())){
                    Admin admin_db = adminMapper.selectByPrimaryKey(department.getHeadManAdminId());
                    if(admin_db == null){
                        code = "-4";
                        msg = "选择的部门部长不存在";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                }
                department.setId(CommonUtil.getUUID());
                department.setCreateTime(new Date());
                department.setDelFlag("0");
                departmentMapper.insertSelective(department);
                code = Constants.SUCCESS;
                msg = "添加部门成功";
            }

        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result pageList(Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<Map<String,Object>> mapList = departmentMapper.selectAll();
            PageInfo page = new PageInfo(mapList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result listName() {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            List<Map<String,Object>> list = departmentMapper.selectLeaderName();
            PageInfo page = new PageInfo(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
