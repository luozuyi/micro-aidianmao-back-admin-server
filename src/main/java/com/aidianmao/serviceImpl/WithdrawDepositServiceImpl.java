package com.aidianmao.serviceImpl;

import com.aidianmao.entity.*;
import com.aidianmao.mapper.*;
import com.aidianmao.service.WithdrawDepositService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
@Transactional
@Service
public class WithdrawDepositServiceImpl implements WithdrawDepositService {
    @Autowired
    private WithdrawDepositMapper withdrawDepositMapper;
    @Autowired
    private PlatformMessageMapper platformMessageMapper;
    @Autowired
    private MemberMapper memberMapper;
    @Autowired
    private IncomeExpensesDetailMapper incomeExpensesDetailMapper;
    @Autowired
    private PlatformFundDetailMapper platformFundDetailMapper;
    @Autowired
    private WithdrawFreezeDetailMapper withdrawFreezeDetailMapper;
    @Override
    public Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum,pageSize);
            List<Map<String, Object>> list = withdrawDepositMapper.selectAll(params);
            PageInfo<Map<String,Object>> page = new PageInfo<>(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result detail(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            }else {
                WithdrawDeposit withdrawDeposit = withdrawDepositMapper.selectByPrimaryKey(id);
                if (withdrawDeposit!=null){
                    Map<String,Object> map = withdrawDepositMapper.selectById(id);
                    result.setData(map);
                    code = Constants.SUCCESS;
                    msg = "查询成功";
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result update(WithdrawDeposit record,Integer checkResult,Integer checkType,String aidianmaoAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if(StringUtils.isBlank(record.getId())){
                code = "-3";
                msg = "主键ID不能为空";
            }else if(checkResult == null || (checkResult!=Constants.CheckResult.TYPE1.getType()&&checkResult!=Constants.CheckResult.TYPE2.getType())){
                code = "-4";
                msg = "审核结果不能为空或审核结果错误";
            } else if(checkType == null || (checkType!=Constants.CheckType.TYPE1.getType()&&checkType!=Constants.CheckType.TYPE2.getType())){
                code = "-5";
                msg = "审核类型不能为空或审核类型错误";
            }else {
                WithdrawDeposit withdrawDeposit = withdrawDepositMapper.selectByPrimaryKey(record.getId());
                if(withdrawDeposit==null){
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                    result.setCode(code);
                    result.setMsg(msg);
                    return result;
                }
                if(withdrawDeposit.getMoney()==null || withdrawDeposit.getServiceFee()==null){
                    code = Constants.ERROR;
                    msg = "提现金额为空或提现手续费为空";
                    result.setCode(code);
                    result.setMsg(msg);
                    return result;
                }
                if(withdrawDeposit.getMoney().compareTo(BigDecimal.ZERO)<=0 || withdrawDeposit.getServiceFee().compareTo(BigDecimal.ZERO)<0){
                    code = Constants.ERROR;
                    msg = "提现金额小于或等于0或者提现手续费小于0";
                    result.setCode(code);
                    result.setMsg(msg);
                    return result;
                }
                List<WithdrawFreezeDetail> list = withdrawFreezeDetailMapper.selectByWithdrawDepositId(record.getId());
                if(list!=null&&list.size()==1){
                    if (!StringUtils.isBlank(withdrawDeposit.getMemberId())){
                        Member member = memberMapper.selectMemberById(withdrawDeposit.getMemberId());
                        if(member==null){
                            code = Constants.ERROR;
                            msg = "会员ID不存在或已被修改";
                            result.setCode(code);
                            result.setMsg(msg);
                            return result;
                        }
                        if(checkType == Constants.CheckType.TYPE1.getType()){
                            if(Constants.WithdrawDepositStatus.TYPE0.getType().equals(withdrawDeposit.getStatus())) {
                                if (checkResult == Constants.CheckResult.TYPE1.getType()) {
                                    record.setStatus(Constants.WithdrawDepositStatus.TYPE1.getType());
                                    msg = "更新成功,第一次审核通过";
                                } else if (checkResult == Constants.CheckResult.TYPE2.getType()) {
                                    record.setStatus(Constants.WithdrawDepositStatus.TYPE3.getType());
                                    //添加站内信表信息
                                    PlatformMessage platformMessage=new PlatformMessage();
                                    platformMessage.setId(CommonUtil.getUUID());
                                    platformMessage.setCreateTime(new Date());
                                    platformMessage.setDelFlag(Constants.DelFlag.NORMAL.getType());
                                    platformMessage.setTitle("抱歉！您的提现申请未通过审核");
                                    platformMessage.setContent("尊敬的爱店猫用户：\n" +
                                            "　 抱歉！您的提现申请未通过审核，如有疑问请咨询在线客服，或拨打4008359100。");
                                    platformMessage.setStatus(Constants.PlatformMessageStatus.TYPE0.getType());
                                    platformMessageMapper.insertSelective(platformMessage);

                                    //添加会员收支明细表信息
                                    //提现
                                    IncomeExpensesDetail incomeExpensesDetail = new IncomeExpensesDetail();
                                    incomeExpensesDetail.setId(CommonUtil.getUUID());
                                    incomeExpensesDetail.setCreateTime(new Date());
                                    incomeExpensesDetail.setDelFlag(Constants.DelFlag.NORMAL.getType());
                                    //收支类型 0:收入 1:支出
                                    incomeExpensesDetail.setType(Constants.IncomeExpensesType.TYPE0.getType());
                                    //场景 0:充值 1:tmall订单支付 2:提现 3:tmall店铺售出 4:tmall订单交易成功 5:提现手续费
                                    incomeExpensesDetail.setMoneyScene(Constants.IncomeExpensesMoneyScene.TYPE2.getType());
                                    incomeExpensesDetail.setAmountMoney(withdrawDeposit.getMoney());
                                    incomeExpensesDetail.setMemberId(withdrawDeposit.getMemberId());
                                    incomeExpensesDetailMapper.insertSelective(incomeExpensesDetail);

                                    //修改会员资金
                                    BigDecimal freezeMoney = member.getFreezeMoney().subtract(withdrawDeposit.getMoney());
                                    BigDecimal newMoney = member.getMoney().add(withdrawDeposit.getMoney());
                                    member.setFreezeMoney(freezeMoney);
                                    member.setMoney(newMoney);
                                    memberMapper.updateCustomerSelective(member);

                                    //冻结金额记录表
                                    WithdrawFreezeDetail withdrawFreezeDetail = list.get(0);
                                    withdrawFreezeDetail.setStatus(Constants.WithdrawFreezeDetailStatus.TYPE1.getType());
                                    withdrawFreezeDetailMapper.updateByPrimaryKeySelective(withdrawFreezeDetail);
                                    msg = "更新成功,第一次审核不通过";
                                }
                                record.setAdminId(adminId);
                                withdrawDepositMapper.updateByPrimaryKeySelective(record);
                                code = Constants.SUCCESS;
                            }else {
                                code = Constants.ERROR;
                                msg = "当前数据已被审核或者状态错误";
                            }
                        }else if (checkType == Constants.CheckType.TYPE2.getType()){
                            if (Constants.WithdrawDepositStatus.TYPE1.getType().equals(withdrawDeposit.getStatus())){
                                if (checkResult == Constants.CheckResult.TYPE1.getType()) {
                                    record.setStatus(Constants.WithdrawDepositStatus.TYPE2.getType());

                                    //添加会员收支明细表信息
                                    //提现
                                    IncomeExpensesDetail incomeExpensesDetail = new IncomeExpensesDetail();
                                    incomeExpensesDetail.setId(CommonUtil.getUUID());
                                    incomeExpensesDetail.setCreateTime(new Date());
                                    incomeExpensesDetail.setDelFlag(Constants.DelFlag.NORMAL.getType());
                                    //收支类型 0:收入 1:支出
                                    incomeExpensesDetail.setType(Constants.IncomeExpensesType.TYPE1.getType());
                                    //场景 0:充值 1:tmall订单支付 2:提现 3:tmall店铺售出 4:tmall订单交易成功 5:提现手续费
                                    incomeExpensesDetail.setMoneyScene(Constants.IncomeExpensesMoneyScene.TYPE2.getType());
                                    incomeExpensesDetail.setAmountMoney(withdrawDeposit.getActualAppropriation());
                                    incomeExpensesDetail.setMemberId(withdrawDeposit.getMemberId());
                                    incomeExpensesDetailMapper.insertSelective(incomeExpensesDetail);

                                    Map<String,Object> map = withdrawDepositMapper.selectBankCardAddress(withdrawDeposit.getId());
                                    BigDecimal bankRate = BigDecimal.ZERO;
                                    if("湖北".equals(map.get("provincesName").toString())&&"武汉市".equals(map.get("citysName").toString())){
                                        bankRate = new BigDecimal("1.00");
                                    }else if (withdrawDeposit.getMoney().compareTo(BigDecimal.ZERO)>0 && withdrawDeposit.getMoney().compareTo(new BigDecimal("10000.00"))<=0){
                                        bankRate = new BigDecimal("4.00");
                                    }else if (withdrawDeposit.getMoney().compareTo(new BigDecimal("10000.00"))>0 && withdrawDeposit.getMoney().compareTo(new BigDecimal("100000.00"))<=0){
                                        bankRate = new BigDecimal("8.00");
                                    }else if (withdrawDeposit.getMoney().compareTo(new BigDecimal("100000.00"))>0 && withdrawDeposit.getMoney().compareTo(new BigDecimal("500000.00"))<=0){
                                        bankRate = new BigDecimal("12.00");
                                    }else if (withdrawDeposit.getMoney().compareTo(new BigDecimal("500000.00"))>0 && withdrawDeposit.getMoney().compareTo(new BigDecimal("1000000.00"))<=0){
                                        bankRate = new BigDecimal("16.00");
                                    }else if (withdrawDeposit.getMoney().compareTo(new BigDecimal("1000000.00"))>0 && withdrawDeposit.getMoney().compareTo(new BigDecimal("10000000.00"))<=0){
                                        bankRate = withdrawDeposit.getMoney().multiply(new BigDecimal("0.000016"));
                                    }else if (withdrawDeposit.getMoney().compareTo(new BigDecimal("10000000.00"))>0){
                                        bankRate = new BigDecimal("160.00");
                                    }
                                    //前台提现时减少可用资金，增加冻结金额；后台提现时不处理可用资金，审核成功后减少冻结资金
                                    /*BigDecimal serviceFee = withdrawDeposit.getServiceFee();
                                    BigDecimal amountMoney = serviceFee.subtract(bankRate);
                                    System.out.println(amountMoney);
                                    amountMoney = amountMoney.multiply(new BigDecimal("-1"));*/

                                    //添加平台收支明细信息(平台支出银行手续费)
                                    PlatformFundDetail platformFundDetail = new PlatformFundDetail();
                                    platformFundDetail.setId(CommonUtil.getUUID());
                                    platformFundDetail.setCreateTime(new Date());
                                    platformFundDetail.setDelFlag(Constants.DelFlag.NORMAL.getType());
                                    platformFundDetail.setRelationId(withdrawDeposit.getId());
                                    //关联类型 0:订单id 1:提现id
                                    platformFundDetail.setRelationType(Constants.PlatformFundDetailRelationType.TYPE1.getType());
                                    //收支类型 0:收入 1:支出
                                    platformFundDetail.setType(Constants.PlatformFundDetailType.TYPE1.getType());
                                    //场景:0:服务费 1:提现手续费 2:违约金 3:违约金调整返还 4:佣金返利 5:银行手续费
                                    platformFundDetail.setMoneyScene(Constants.PlatformFundDetailMoneyScene.TYPE5.getType());
                                    platformFundDetail.setAmountMoney(bankRate);
                                    platformFundDetailMapper.insertSelective(platformFundDetail);

                                    if(withdrawDeposit.getServiceFee().compareTo(BigDecimal.ZERO)!=0){
                                        //有提现手续费
                                        IncomeExpensesDetail incomeExpensesDetail1 = new IncomeExpensesDetail();
                                        incomeExpensesDetail1.setId(CommonUtil.getUUID());
                                        incomeExpensesDetail1.setCreateTime(new Date());
                                        incomeExpensesDetail1.setDelFlag(Constants.DelFlag.NORMAL.getType());
                                        //收支类型 0:收入 1:支出
                                        incomeExpensesDetail1.setType(Constants.IncomeExpensesType.TYPE1.getType());
                                        //场景 0:充值 1:tmall订单支付 2:提现 3:tmall店铺售出 4:tmall订单交易成功 5:提现手续费
                                        incomeExpensesDetail1.setMoneyScene(Constants.IncomeExpensesMoneyScene.TYPE5.getType());
                                        incomeExpensesDetail1.setAmountMoney(withdrawDeposit.getServiceFee());
                                        incomeExpensesDetail1.setMemberId(withdrawDeposit.getMemberId());
                                        incomeExpensesDetailMapper.insertSelective(incomeExpensesDetail1);

                                        //添加平台收支明细信息(平台收入提现手续费)
                                        PlatformFundDetail platformFundDetail1 = new PlatformFundDetail();
                                        platformFundDetail1.setId(CommonUtil.getUUID());
                                        platformFundDetail1.setCreateTime(new Date());
                                        platformFundDetail1.setDelFlag(Constants.DelFlag.NORMAL.getType());
                                        platformFundDetail1.setRelationId(withdrawDeposit.getId());
                                        //关联类型 0:订单id 1:提现id
                                        platformFundDetail1.setRelationType(Constants.PlatformFundDetailRelationType.TYPE1.getType());
                                        //收支类型 0:收入 1:支出
                                        platformFundDetail1.setType(Constants.PlatformFundDetailType.TYPE0.getType());
                                        //场景 0:服务费 1:佣金返利 2:违约金 3:违约金调整返还 4:提现手续费 5:银行手续费
                                        platformFundDetail1.setMoneyScene(Constants.PlatformFundDetailMoneyScene.TYPE4.getType());
                                        platformFundDetail1.setAmountMoney(withdrawDeposit.getServiceFee());
                                        platformFundDetailMapper.insertSelective(platformFundDetail1);
                                    }

                                    //添加站内信表信息
                                    BigDecimal money = member.getMoney();
                                    PlatformMessage platformMessage=new PlatformMessage();
                                    platformMessage.setId(CommonUtil.getUUID());
                                    platformMessage.setCreateTime(new Date());
                                    platformMessage.setDelFlag(Constants.DelFlag.NORMAL.getType());
                                    platformMessage.setTitle("您的提现申请已经通过审核，成功提现"+withdrawDeposit.getMoney()+"元，当前账户余额"+money+"元。");
                                    platformMessage.setContent("尊敬的爱店猫用户：\n" +
                                            "　 您的提现申请已经通过审核，成功提现"+withdrawDeposit.getMoney()+"元，当前账户余额"+money+"元。");
                                    platformMessage.setStatus(Constants.PlatformMessageStatus.TYPE0.getType());
                                    platformMessageMapper.insertSelective(platformMessage);

                                    //修改会员资金
                                    BigDecimal freezeMoney = member.getFreezeMoney().subtract(withdrawDeposit.getMoney());
                                    member.setFreezeMoney(freezeMoney);
                                    memberMapper.updateCustomerSelective(member);

                                    //冻结金额记录表
                                    WithdrawFreezeDetail withdrawFreezeDetail = list.get(0);
                                    withdrawFreezeDetail.setStatus(Constants.WithdrawFreezeDetailStatus.TYPE1.getType());
                                    withdrawFreezeDetailMapper.updateByPrimaryKeySelective(withdrawFreezeDetail);
                                    msg = "更新成功,第二次审核通过";
                                } else if (checkResult == Constants.CheckResult.TYPE2.getType()) {
                                    record.setStatus(Constants.WithdrawDepositStatus.TYPE3.getType());
                                    //添加站内信表信息
                                    PlatformMessage platformMessage=new PlatformMessage();
                                    platformMessage.setId(CommonUtil.getUUID());
                                    platformMessage.setCreateTime(new Date());
                                    platformMessage.setDelFlag(Constants.DelFlag.NORMAL.getType());
                                    platformMessage.setTitle("抱歉！您的提现申请未通过审核");
                                    platformMessage.setContent("尊敬的爱店猫用户：\n" +
                                            "　 抱歉！您的提现申请未通过审核，如有疑问请咨询在线客服，或拨打4008359100。");
                                    platformMessage.setStatus(Constants.PlatformMessageStatus.TYPE0.getType());
                                    platformMessageMapper.insertSelective(platformMessage);

                                    //添加会员收支明细表信息
                                    //提现
                                    IncomeExpensesDetail incomeExpensesDetail = new IncomeExpensesDetail();
                                    incomeExpensesDetail.setId(CommonUtil.getUUID());
                                    incomeExpensesDetail.setCreateTime(new Date());
                                    incomeExpensesDetail.setDelFlag(Constants.DelFlag.NORMAL.getType());
                                    //收支类型 0:收入 1:支出
                                    incomeExpensesDetail.setType(Constants.IncomeExpensesType.TYPE0.getType());
                                    //场景 0:充值 1:tmall订单支付 2:提现 3:tmall店铺售出 4:tmall订单交易成功 5:提现手续费
                                    incomeExpensesDetail.setMoneyScene(Constants.IncomeExpensesMoneyScene.TYPE2.getType());
                                    incomeExpensesDetail.setAmountMoney(withdrawDeposit.getMoney());
                                    incomeExpensesDetail.setMemberId(withdrawDeposit.getMemberId());
                                    incomeExpensesDetailMapper.insertSelective(incomeExpensesDetail);

                                    //修改会员资金
                                    BigDecimal freezeMoney = member.getFreezeMoney().subtract(withdrawDeposit.getMoney());
                                    BigDecimal newMoney = member.getMoney().add(withdrawDeposit.getMoney());
                                    member.setFreezeMoney(freezeMoney);
                                    member.setMoney(newMoney);
                                    memberMapper.updateCustomerSelective(member);

                                    //冻结金额记录表
                                    WithdrawFreezeDetail withdrawFreezeDetail = list.get(0);
                                    withdrawFreezeDetail.setStatus(Constants.WithdrawFreezeDetailStatus.TYPE1.getType());
                                    withdrawFreezeDetailMapper.updateByPrimaryKeySelective(withdrawFreezeDetail);
                                    msg = "更新成功,第二次审核不通过";
                                }
                                record.setAdminId(adminId);
                                withdrawDepositMapper.updateByPrimaryKeySelective(record);
                                code = Constants.SUCCESS;
                            }else{
                                code = Constants.ERROR;
                                msg = "当前数据已被审核或者状态错误";
                            }
                        }
                    }else {
                        code = Constants.ERROR;
                        msg = "会员ID为空或提现金额为空";
                    }
                }else {
                    code = Constants.ERROR;
                    msg = "提现冻结资金记录错误";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result updateServiceFee(String id,String aidianmaoAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            }else {
                String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
                WithdrawDeposit withdrawDeposit = withdrawDepositMapper.selectByPrimaryKey(id);
                if (withdrawDeposit!=null && !StringUtils.isBlank(withdrawDeposit.getStatus()) && withdrawDeposit.getStatus().equals(Constants.WithdrawDepositStatus.TYPE0.getType())){
                    withdrawDeposit.setServiceFee(new BigDecimal(0));
                    withdrawDeposit.setActualAppropriation(withdrawDeposit.getMoney());
                    withdrawDeposit.setAdminId(adminId);
                    withdrawDepositMapper.updateByPrimaryKeySelective(withdrawDeposit);
                    code = Constants.SUCCESS;
                    msg = "免手续费，更新成功";
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改,当前状态不能执行免手续费操作";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result refuseApply(String id,String refuseReason,String aidianmaoAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键id不能为空";
            } else if (StringUtils.isBlank(refuseReason)) {
                code = "-4";
                msg = "拒绝理由不能为空";
            } else {
                    WithdrawDeposit withdrawDeposit = withdrawDepositMapper.selectByPrimaryKey(id);
                    if (withdrawDeposit == null) {
                        code = "-4";
                        msg = "提现记录不存在";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    if (withdrawDeposit.getMoney() == null || withdrawDeposit.getMoney().compareTo(BigDecimal.ZERO) <= 0) {
                        code = "-5";
                        msg = "提现金额不能为空且不能为负";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    List<WithdrawFreezeDetail> list = withdrawFreezeDetailMapper.selectByWithdrawDepositId(withdrawDeposit.getId());
                    if (list != null && list.size() == 1) {
                        if (!StringUtils.isBlank(withdrawDeposit.getMemberId())) {
                            Member member = memberMapper.selectMemberById(withdrawDeposit.getMemberId());
                            if (member == null) {
                                code = "-6";
                                msg = "提现会员不存在";
                                result.setCode(code);
                                result.setMsg(msg);
                                return result;
                            }
                            // 添加站内表信息
                            PlatformMessage platformMessage = new PlatformMessage();
                            platformMessage.setId(CommonUtil.getUUID());
                            platformMessage.setCreateTime(new Date());
                            platformMessage.setDelFlag("0");
                            platformMessage.setMemberId(withdrawDeposit.getMemberId());
                            platformMessage.setTitle("抱歉！您的提现申请未通过审核");
                            platformMessage.setContent("尊敬的爱店猫用户：\n" +
                                    "　 抱歉！您的提现申请未通过审核，如有疑问请咨询在线客服，或拨打4008359100。");
                            platformMessage.setStatus(Constants.PlatformMessageStatus.TYPE0.getType());
                            platformMessageMapper.insertSelective(platformMessage);

                            //添加会员收支明细表信息
                            IncomeExpensesDetail incomeExpensesDetail = new IncomeExpensesDetail();
                            incomeExpensesDetail.setId(CommonUtil.getUUID());
                            incomeExpensesDetail.setCreateTime(new Date());
                            incomeExpensesDetail.setDelFlag(Constants.DelFlag.NORMAL.getType());
                            //收支类型 0:收入 1:支出
                            incomeExpensesDetail.setType(Constants.IncomeExpensesType.TYPE0.getType());
                            //场景 0:充值 1:tmall订单支付 2:提现 3:tmall店铺售出 4:tmall订单交易成功 5:提现手续费
                            incomeExpensesDetail.setMoneyScene("15");
                            incomeExpensesDetail.setAmountMoney(withdrawDeposit.getMoney());
                            incomeExpensesDetail.setMemberId(withdrawDeposit.getMemberId());
                            incomeExpensesDetailMapper.insertSelective(incomeExpensesDetail);

                            //修改会员资金
                            BigDecimal freezeMoney = member.getFreezeMoney().subtract(withdrawDeposit.getMoney());
                            BigDecimal newMoney = member.getMoney().add(withdrawDeposit.getMoney());
                            member.setFreezeMoney(freezeMoney);
                            member.setMoney(newMoney);
                            memberMapper.updateCustomerSelective(member);

                            //冻结金额记录表
                            WithdrawFreezeDetail withdrawFreezeDetail = list.get(0);
                            withdrawFreezeDetail.setStatus(Constants.WithdrawFreezeDetailStatus.TYPE1.getType());
                            withdrawFreezeDetailMapper.updateByPrimaryKeySelective(withdrawFreezeDetail);

                            withdrawDeposit.setStatus("3");
                            withdrawDeposit.setRefuseReason(refuseReason);
                            withdrawDeposit.setAdminId(adminId);
                            withdrawDepositMapper.updateByPrimaryKeySelective(withdrawDeposit);
                            code = Constants.SUCCESS;
                            msg = "拒绝成功";
                        }else {
                            code = Constants.ERROR;
                            msg = "会员ID为空或提现金额为空";
                        }
                    }else {
                    code = Constants.ERROR;
                     msg = "提现冻结资金记录错误";
                }
                }
              } catch (Exception e) {
                e.printStackTrace();
                 code = Constants.ERROR;
                 TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                 msg = "系统繁忙";
               }
             result.setCode(code);
             result.setMsg(msg);
             return result;
            }

     @Override
     public Result acceptApply(String id,String checkResult,String aidianmaoAdminToken){
         Result result = new Result();
         String code = Constants.FAIL;
         String msg = "初始化";
         try {
             String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
             if(StringUtils.isBlank(id)){
                 code  = "-3";
                 msg = "主键id不能为空";
                 // 0：通过   1：不通过
             }else if(StringUtils.isBlank(checkResult) || (!"0".equals(checkResult)) && !"1".equals(checkResult)) {
                 code = "-4";
                 msg = "申请结果不能为空";
             } else {
                 WithdrawDeposit withdrawDeposit = withdrawDepositMapper.selectByPrimaryKey(id);
                 if (withdrawDeposit == null) {
                     code = "-4";
                     msg = "提现记录不存在";
                     result.setCode(code);
                     result.setMsg(msg);
                     return result;
                 }
                 if (withdrawDeposit.getMoney() == null || withdrawDeposit.getMoney().compareTo(BigDecimal.ZERO) <= 0) {
                     code = "-5";
                     msg = "提现金额不能为空且不能为负";
                     result.setCode(code);
                     result.setMsg(msg);
                     return result;
                 }
                 List<WithdrawFreezeDetail> list = withdrawFreezeDetailMapper.selectByWithdrawDepositId(withdrawDeposit.getId());
                 if (list != null && list.size() == 1) {
                     if (!StringUtils.isBlank(withdrawDeposit.getMemberId())) {
                         Member member = memberMapper.selectMemberById(withdrawDeposit.getMemberId());
                         if (member == null) {
                             code = "-6";
                             msg = "提现会员不存在";
                             result.setCode(code);
                             result.setMsg(msg);
                             return result;
                         }
                         // 通过
                         if (checkResult.equals("0")) {
                             if (withdrawDeposit.getServiceFee().compareTo(BigDecimal.ZERO) != 0) {
                                 //添加平台收支明细信息(平台收入提现手续费)
                                 PlatformFundDetail platformFundDetail1 = new PlatformFundDetail();
                                 platformFundDetail1.setId(CommonUtil.getUUID());
                                 platformFundDetail1.setCreateTime(new Date());
                                 platformFundDetail1.setDelFlag(Constants.DelFlag.NORMAL.getType());
                                 platformFundDetail1.setRelationId(withdrawDeposit.getId());
                                 //关联类型 0:订单id 1:提现id
                                 platformFundDetail1.setRelationType(Constants.PlatformFundDetailRelationType.TYPE1.getType());
                                 //收支类型 0:收入 1:支出
                                 platformFundDetail1.setType(Constants.PlatformFundDetailType.TYPE0.getType());
                                 //场景:0:服务费 1:提现手续费 2:违约金 3:违约金调整返还 4:佣金返利 5:银行手续费
                                 platformFundDetail1.setMoneyScene("1");
                                 platformFundDetail1.setAmountMoney(withdrawDeposit.getServiceFee());
                                 platformFundDetailMapper.insertSelective(platformFundDetail1);
                             } else if (withdrawDeposit.getServiceFee().compareTo(BigDecimal.ZERO) == 0) {
                                 // 添加会员明细记录
                                 IncomeExpensesDetail incomeExpensesDetail1 = new IncomeExpensesDetail();
                                 incomeExpensesDetail1.setId(CommonUtil.getUUID());
                                 incomeExpensesDetail1.setCreateTime(new Date());
                                 incomeExpensesDetail1.setDelFlag(Constants.DelFlag.NORMAL.getType());
                                 //收支类型 0:收入 1:支出
                                 incomeExpensesDetail1.setType(Constants.IncomeExpensesType.TYPE1.getType());
                                 //会员收支明细：场景:0:充值 1:佣金返利 2:差价返利 3:违约定金返还 4:违约金调整返还 5:支付余额返还
                                 //6:全款购买 7:店铺余额 8:定金 9:违约金 10:提现 11:店铺收入 12:违约全款返还 13:差价返利扣除 14:充值调整
                                 // 15:提现退还 16: 提现手续费 17:免提现手续费
                                 incomeExpensesDetail1.setMoneyScene("17");
                                 BigDecimal money1 = BigDecimal.ZERO;
                                 incomeExpensesDetail1.setAmountMoney(money1);
                                 incomeExpensesDetail1.setMemberId(withdrawDeposit.getMemberId());
                                 incomeExpensesDetailMapper.insertSelective(incomeExpensesDetail1);
                             }
                             Map<String, Object> map = withdrawDepositMapper.selectBankCardAddress(withdrawDeposit.getId());
                             BigDecimal bankRate = BigDecimal.ZERO;
                             if ("湖北".equals(map.get("provincesName").toString()) && "武汉市".equals(map.get("citysName").toString())) {
                                 bankRate = new BigDecimal("1.00");
                             } else if (withdrawDeposit.getMoney().compareTo(BigDecimal.ZERO) > 0 && withdrawDeposit.getMoney().compareTo(new BigDecimal("10000.00")) <= 0) {
                                 bankRate = new BigDecimal("4.00");
                             } else if (withdrawDeposit.getMoney().compareTo(new BigDecimal("10000.00")) > 0 && withdrawDeposit.getMoney().compareTo(new BigDecimal("100000.00")) <= 0) {
                                 bankRate = new BigDecimal("8.00");
                             } else if (withdrawDeposit.getMoney().compareTo(new BigDecimal("100000.00")) > 0 && withdrawDeposit.getMoney().compareTo(new BigDecimal("500000.00")) <= 0) {
                                 bankRate = new BigDecimal("12.00");
                             } else if (withdrawDeposit.getMoney().compareTo(new BigDecimal("500000.00")) > 0 && withdrawDeposit.getMoney().compareTo(new BigDecimal("1000000.00")) <= 0) {
                                 bankRate = new BigDecimal("16.00");
                             } else if (withdrawDeposit.getMoney().compareTo(new BigDecimal("1000000.00")) > 0 && withdrawDeposit.getMoney().compareTo(new BigDecimal("10000000.00")) <= 0) {
                                 bankRate = withdrawDeposit.getMoney().multiply(new BigDecimal("0.000016"));
                             } else if (withdrawDeposit.getMoney().compareTo(new BigDecimal("10000000.00")) > 0) {
                                 bankRate = new BigDecimal("160.00");
                             }
                             //添加平台收支明细信息(平台支出银行手续费)
                             PlatformFundDetail platformFundDetail = new PlatformFundDetail();
                             platformFundDetail.setId(CommonUtil.getUUID());
                             platformFundDetail.setCreateTime(new Date());
                             platformFundDetail.setDelFlag(Constants.DelFlag.NORMAL.getType());
                             platformFundDetail.setRelationId(withdrawDeposit.getId());
                             //关联类型 0:订单id 1:提现id
                             platformFundDetail.setRelationType(Constants.PlatformFundDetailRelationType.TYPE1.getType());
                             //收支类型 0:收入 1:支出
                             platformFundDetail.setType(Constants.PlatformFundDetailType.TYPE1.getType());
                             //场景 0:服务费 1:佣金返利 2:违约金 3:违约金调整返还 4:提现手续费 5:银行手续费
                             platformFundDetail.setMoneyScene(Constants.PlatformFundDetailMoneyScene.TYPE5.getType());
                             platformFundDetail.setAmountMoney(bankRate);
                             platformFundDetailMapper.insertSelective(platformFundDetail);

                             //修改会员资金
                             BigDecimal freezeMoney = member.getFreezeMoney().subtract(withdrawDeposit.getMoney());
                             member.setFreezeMoney(freezeMoney);
                             memberMapper.updateCustomerSelective(member);

                             //冻结金额记录表
                             WithdrawFreezeDetail withdrawFreezeDetail = list.get(0);
                             withdrawFreezeDetail.setStatus(Constants.WithdrawFreezeDetailStatus.TYPE1.getType());
                             withdrawFreezeDetailMapper.updateByPrimaryKeySelective(withdrawFreezeDetail);
                             msg = "提现申请审核通过";
                             // 修改提现状态为成功
                             withdrawDeposit.setStatus("2");
                             withdrawDeposit.setAdminId(adminId);
                             withdrawDepositMapper.updateByPrimaryKeySelective(withdrawDeposit);
                             code = Constants.SUCCESS;
                             //添加站内信表信息
                             BigDecimal money = member.getMoney();
                             PlatformMessage platformMessage = new PlatformMessage();
                             platformMessage.setId(CommonUtil.getUUID());
                             platformMessage.setCreateTime(new Date());
                             platformMessage.setMemberId(withdrawDeposit.getMemberId());
                             platformMessage.setDelFlag(Constants.DelFlag.NORMAL.getType());
                             platformMessage.setTitle("您的提现申请已经通过审核，成功提现" + withdrawDeposit.getMoney() + "元，当前账户余额" + money + "元。");
                             platformMessage.setContent("尊敬的爱店猫用户：\n" +
                                     "　 您的提现申请已经通过审核，成功提现" + withdrawDeposit.getMoney() + "元，当前账户余额" + money + "元。");
                             platformMessage.setStatus(Constants.PlatformMessageStatus.TYPE0.getType());
                             platformMessageMapper.insertSelective(platformMessage);
                             code = Constants.SUCCESS;
                             msg = "申请请求成功";
                             // 不通过
                         } else if (checkResult.equals("1")) {
                             //添加站内信表信息
                             PlatformMessage platformMessage = new PlatformMessage();
                             platformMessage.setId(CommonUtil.getUUID());
                             platformMessage.setCreateTime(new Date());
                             platformMessage.setDelFlag(Constants.DelFlag.NORMAL.getType());
                             platformMessage.setMemberId(withdrawDeposit.getMemberId());
                             platformMessage.setTitle("抱歉！您的提现申请未通过审核");
                             platformMessage.setContent("尊敬的爱店猫用户：\n" +
                                     "　 抱歉！您的提现申请未通过审核，如有疑问请咨询在线客服，或拨打4008359100。");
                             platformMessage.setStatus(Constants.PlatformMessageStatus.TYPE0.getType());
                             platformMessageMapper.insertSelective(platformMessage);

                             //添加会员收支明细表信息
                             IncomeExpensesDetail incomeExpensesDetail = new IncomeExpensesDetail();
                             incomeExpensesDetail.setId(CommonUtil.getUUID());
                             incomeExpensesDetail.setCreateTime(new Date());
                             incomeExpensesDetail.setDelFlag(Constants.DelFlag.NORMAL.getType());
                             //收支类型 0:收入 1:支出
                             incomeExpensesDetail.setType(Constants.IncomeExpensesType.TYPE0.getType());
                             //会员收支明细：场景:0:充值 1:佣金返利 2:差价返利 3:违约定金返还 4:违约金调整返还 5:支付余额返还
                             //6:全款购买 7:店铺余额 8:定金 9:违约金 10:提现 11:店铺收入 12:违约全款返还 13:差价返利扣除 14:充值调整
                             // 15:提现退还 16: 提现手续费 17:免提现手续费
                             incomeExpensesDetail.setMoneyScene("15");
                             incomeExpensesDetail.setAmountMoney(withdrawDeposit.getMoney());
                             incomeExpensesDetail.setMemberId(withdrawDeposit.getMemberId());
                             incomeExpensesDetailMapper.insertSelective(incomeExpensesDetail);

                             //修改会员资金
                             BigDecimal freezeMoney = member.getFreezeMoney().subtract(withdrawDeposit.getMoney());
                             BigDecimal newMoney = member.getMoney().add(withdrawDeposit.getMoney());
                             member.setFreezeMoney(freezeMoney);
                             member.setMoney(newMoney);
                             memberMapper.updateCustomerSelective(member);

                             //冻结金额记录表
                             WithdrawFreezeDetail withdrawFreezeDetail = list.get(0);
                             withdrawFreezeDetail.setStatus(Constants.WithdrawFreezeDetailStatus.TYPE1.getType());
                             withdrawFreezeDetailMapper.updateByPrimaryKeySelective(withdrawFreezeDetail);
                             // 修改提现状态为失败
                             withdrawDeposit.setStatus(Constants.WithdrawDepositStatus.TYPE3.getType());
                             withdrawDeposit.setAdminId(adminId);
                             withdrawDepositMapper.updateByPrimaryKeySelective(withdrawDeposit);
                             code = Constants.SUCCESS;
                             msg = "第二次审核不通过";
                         }
                     }else {
                         code = Constants.ERROR;
                         msg = "会员ID为空或提现金额为空";
                     }
                 }else {
                     code = Constants.ERROR;
                     msg = "提现冻结资金记录错误";
                 }
             }
             } catch(Exception e){
                         e.printStackTrace();
                         code = Constants.ERROR;
                         TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                         msg = "系统繁忙";
                     }
                     result.setCode(code);
                     result.setMsg(msg);
                     return result;
                 }


    @Override
    public Result processing(String id,String aidianmaoAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            }else {
                String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
                WithdrawDeposit withdrawDeposit = withdrawDepositMapper.selectByPrimaryKey(id);
                if (withdrawDeposit==null){
                   code = "-4";
                   msg = "提现不能为空";
                }else if(!withdrawDeposit.getStatus().equals("0")){
                   code = "-5";
                   msg = "只有审核中的状态才能接受申请";
                }else {
                    withdrawDeposit.setAdminId(adminId);
                    withdrawDeposit.setStatus("1");
                    withdrawDepositMapper.updateByPrimaryKeySelective(withdrawDeposit);
                    code = Constants.SUCCESS;
                    msg = "接受申请成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
