package com.aidianmao.serviceImpl;

import com.aidianmao.entity.IncomeExpensesDetail;
import com.aidianmao.entity.Member;
import com.aidianmao.entity.PlatformMessage;
import com.aidianmao.entity.Remittance;
import com.aidianmao.mapper.IncomeExpensesDetailMapper;
import com.aidianmao.mapper.MemberMapper;
import com.aidianmao.mapper.PlatformMessageMapper;
import com.aidianmao.mapper.RemittanceMapper;
import com.aidianmao.service.RemittanceService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
@Transactional
@Service
public class RemittanceServiceImpl implements RemittanceService {
    @Autowired
    private RemittanceMapper remittanceMapper;
    @Autowired
    private PlatformMessageMapper platformMessageMapper;
    @Autowired
    private MemberMapper memberMapper;
    @Autowired
    private IncomeExpensesDetailMapper incomeExpensesDetailMapper;
    @Override
    public Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum,pageSize);
            List<Map<String, Object>> list = remittanceMapper.selectAll(params);
            PageInfo<Map<String,Object>> page = new PageInfo<>(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result detail(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            }else {
                Remittance remittance = remittanceMapper.selectByPrimaryKey(id);
                if(remittance!=null){
                    Map<String,Object> map = remittanceMapper.selectById(id);
                    result.setData(map);
                    code = Constants.SUCCESS;
                    msg = "查询成功";
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result update(Remittance record,Integer checkResult,String aidianmaoAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if(StringUtils.isBlank(record.getId())){
                code = "-3";
                msg = "主键ID不能为空";
            }else if(checkResult == null || (checkResult!=Constants.CheckResult.TYPE1.getType()&&checkResult!=Constants.CheckResult.TYPE2.getType())){
                code = "-4";
                msg = "审核结果不能为空或审核结果错误";
            } else {
                Remittance remittance = remittanceMapper.selectByPrimaryKey(record.getId());
                if(remittance!=null){
                    Member member = memberMapper.selectByPrimaryKey(remittance.getMemberId());
                    if(member==null){
                        code = Constants.ERROR;
                        msg = "会员ID不存在或已被修改";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    if(Constants.RemittanceStatus.TYPE0.getType().equals(remittance.getStatus())){
                        if(checkResult==Constants.CheckResult.TYPE1.getType()){
                            record.setStatus(Constants.RemittanceStatus.TYPE1.getType());

                            //添加收支明细表信息
                            IncomeExpensesDetail incomeExpensesDetail = new IncomeExpensesDetail();
                            incomeExpensesDetail.setId(CommonUtil.getUUID());
                            incomeExpensesDetail.setCreateTime(new Date());
                            incomeExpensesDetail.setDelFlag(Constants.DelFlag.NORMAL.getType());
                            //收支类型 0:收入 1:支出
                            incomeExpensesDetail.setType(Constants.IncomeExpensesType.TYPE0.getType());
                            //场景 0:充值 1:tmall订单支付 2:提现 3:tmall店铺售出 4:tmall订单交易成功 5:提现手续费
                            incomeExpensesDetail.setMoneyScene(Constants.IncomeExpensesMoneyScene.TYPE0.getType());
                            incomeExpensesDetail.setAmountMoney(remittance.getMoney());
                            incomeExpensesDetail.setMemberId(remittance.getMemberId());
                            incomeExpensesDetailMapper.insertSelective(incomeExpensesDetail);

                            //添加站内信表信息
                            //可用资金更新
                            BigDecimal newMoney = member.getMoney().add(remittance.getMoney());
                            PlatformMessage platformMessage=new PlatformMessage();
                            platformMessage.setId(CommonUtil.getUUID());
                            platformMessage.setMemberId(member.getId());
                            platformMessage.setCreateTime(new Date());
                            platformMessage.setDelFlag(Constants.DelFlag.NORMAL.getType());
                            platformMessage.setTitle(member.getUserName()+"用户，您的账户已成功充值"+remittance.getMoney()+"元人民币");
                            platformMessage.setContent("尊敬的爱店猫用户：\n" +
                                    "　 "+member.getUserName()+"，您的账户已成功充值"+remittance.getMoney()+"元人民币，账户可用余额"+newMoney+"元，可用优惠券0元。");
                            platformMessage.setStatus(Constants.PlatformMessageStatus.TYPE0.getType());
                            platformMessageMapper.insertSelective(platformMessage);

                            //修改会员账户资金
                            member.setMoney(newMoney);
                            memberMapper.updateCustomerSelective(member);
                            code = Constants.SUCCESS;
                            msg = "充值审核成功";
                        }else if(checkResult==Constants.CheckResult.TYPE2.getType()){
                            record.setStatus(Constants.RemittanceStatus.TYPE2.getType());
                            code = Constants.SUCCESS;
                            msg = "充值审核失败";
                        }
                        record.setAdminId(adminId);
                        remittanceMapper.updateByPrimaryKeySelective(record);
                    }else {
                        code = Constants.ERROR;
                        msg = "当前数据已被审核或者状态错误";
                    }
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
