package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Channel;
import com.aidianmao.mapper.ChannelMapper;
import com.aidianmao.service.ChannelService;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.ModelMap;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Transactional
@Service
public class ChannelServiceImpl implements ChannelService {
    @Autowired
    private ChannelMapper channelMapper;

    @Override
    public Result listPageBySelection(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            params.put("delFlag","0");
            PageHelperNew.startPage(pageNum,pageSize);
            List<ModelMap> modelMapList = channelMapper.selectAllBySelection(params);
            PageInfo<ModelMap> page = new PageInfo<>(modelMapList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result addChannel(Channel channel) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String channelName = channel.getChannelName();
            Channel channel_db = channelMapper.selectByChannelName(channelName);
            if(StringUtils.isBlank(channel.getChannelName())){
                code = "-3";
                msg = "栏目名称不能为空";
            }else if(StringUtils.isBlank(channel.getTplChannel())){
                code = "-4";
                msg = "栏目模板不能为空";
            }else if(StringUtils.isBlank(channel.getImageAddress())){
                code = "-5";
                msg = "请上传图片";
            }else if(channel_db != null){
                code = "-6";
                msg = "该栏目已存在";
            }else{
                channel.setId(UUID.randomUUID().toString().replace("-", ""));
                channel.setDelFlag("0");
                channel.setCreateTime(new Date());
                channelMapper.insertSelective(channel);
                code = Constants.SUCCESS;
                msg = "成功";
            }
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    public Result delChannel(String id){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "请选择要删除的栏目";
            }else{
                Channel channel = channelMapper.selectByPrimaryKey(id);
                List<Channel> channelList = channelMapper.selectByParentId(id);
                if (channel == null) {
                    code = "-4";
                    msg = "删除的栏目不存在";
                }else if (!channelList.isEmpty()){
                    code = "-5";
                    msg = "请先删除子栏目";
                }else{
                    channel.setDelFlag("1");
                    channelMapper.updateByPrimaryKeySelective(channel);
                    code = Constants.SUCCESS;
                    msg = "成功";
                }
            }
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return  result;
    }

    public Result getList(){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            List<Channel> channelList = channelMapper.selectAll();
            result.setData(channelList);
            code = Constants.SUCCESS;
            msg = "成功";
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    public Result getParentChannel(){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            List<Channel> channelList = channelMapper.selectParentChannel();
            result.setData(channelList);
            code = Constants.SUCCESS;
            msg = "成功";
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
