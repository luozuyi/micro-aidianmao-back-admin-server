package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Admin;
import com.aidianmao.entity.Department;
import com.aidianmao.entity.Group;
import com.aidianmao.mapper.AdminMapper;
import com.aidianmao.mapper.DepartmentMapper;
import com.aidianmao.mapper.GroupMapper;
import com.aidianmao.service.GroupService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class GroupServiceImpl implements GroupService{
    @Autowired
    private GroupMapper groupMapper;
    @Autowired
    private DepartmentMapper departmentMapper;
    @Autowired
    private AdminMapper adminMapper;
    @Override
    public Result addGroup(Group group) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(group.getGroupName())){
                code = "-3";
                msg = "部门名称不能为空";
            }else{
                if(StringUtils.isNotBlank(group.getDeptId())){
                    Department department_db = departmentMapper.selectByPrimaryKey(group.getDeptId());
                    if(department_db == null){
                        code = "-4";
                        msg = "选择的部门不存在";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                }
                if(StringUtils.isNotBlank(group.getHeadManAdminId())){
                    Admin admin_db = adminMapper.selectByPrimaryKey(group.getHeadManAdminId());
                    if(admin_db == null){
                        code = "-5";
                        msg = "选择的小组组长不存在";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                }
                group.setId(CommonUtil.getUUID());
                group.setCreateTime(new Date());
                group.setDelFlag("0");
                groupMapper.insertSelective(group);
                code = Constants.SUCCESS;
                msg = "添加小组成功";
            }

        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result pageList(Integer pageSize, Integer pageNum) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<Map<String,Object>> mapList = groupMapper.selectAll();
            PageInfo page = new PageInfo(mapList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result pageListLeader() {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            List<Map<String,Object>> list = groupMapper.selectLeader();
            PageInfo page = new PageInfo(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
