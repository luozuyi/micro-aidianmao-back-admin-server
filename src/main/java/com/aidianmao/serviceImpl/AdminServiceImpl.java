package com.aidianmao.serviceImpl;

import com.aidianmao.entity.SalespersonConfig;
import com.aidianmao.mapper.SalespersonConfigMapper;
import com.github.pagehelper.PageInfo;
import com.aidianmao.entity.Admin;
import com.aidianmao.entity.SysRole;
import com.aidianmao.mapper.AdminMapper;
import com.aidianmao.mapper.SysRoleMapper;
import com.aidianmao.service.AdminService;
import com.aidianmao.utils.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class AdminServiceImpl implements AdminService{
    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private SysRoleMapper sysRoleMapper;
    @Autowired
    private SalespersonConfigMapper salespersonConfigMapper;
    @Override
    public Result addAdmin(String adminName,String realName,String qq, String password,String roleId,String isDelFlag,String surePassword,Integer maximunNum) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(isDelFlag)){
                isDelFlag = "0";
            }
            if(StringUtils.isBlank(adminName)){
                code = "-3";
                msg = "管理员登陆名不能为空";
            }else if(StringUtils.isBlank(password)){
                code = "-4";
                msg = "密码不能为空";
            }else if(StringUtils.isBlank(surePassword)){
                code = "-5";
                msg = "确认密码不能为空";
            }else if(!PatternUtil.patternString(password, "password")){
                code = "-6";
                msg = "密码格式不正确";
            }else if(!password.equals(surePassword)){
                code = "-7";
                msg = "两次密码不一致";
            }else if(!isDelFlag.equals(Constants.AdminIsDisable.DISABLE.getIsDisable()) && !isDelFlag.equals(Constants.AdminIsDisable.UNDISABLE.getIsDisable())){
                code = "-10";
                msg = "请正确选择是否禁用";
            }else if(StringUtils.isBlank(realName)){
                code = "-3";
                msg = "管理员姓名不能为空";
            } else{
                Admin admin_db = adminMapper.selectByAdminName(adminName);
                if(admin_db != null){
                    code = "-8";
                    msg = "已存在该管理员";
                }else{
                    if(StringUtils.isNotBlank(roleId)){
                        SysRole sysRole = sysRoleMapper.selectByPrimaryKey(roleId);
                        if(sysRole == null){
                            code = "-9";
                            msg = "要分配的角色不存在";
                        }else{
                            if("客服".equals(sysRole.getRoleName())){
                                if(StringUtils.isBlank(qq)){
                                    code = "-10";
                                    msg = "角色为客服，qq不能为空";
                                    result.setCode(code);
                                    result.setMsg(msg);
                                    return result;
                                }
                            }
                            Admin admin = new Admin();
                            String uuid = CommonUtil.getUUID();
                            admin.setAdminName(adminName);
                            admin.setRealName(realName);
                            admin.setCreateTime(new Date());
                            admin.setPassword(password);
                            admin.setId(uuid);
                            admin.setSysRoleId(roleId);
                            admin.setLoginCount(0L);
                            admin.setDelFlag(isDelFlag);
                            admin.setQq(qq);
                            adminMapper.insertSelective(admin);
                            code = Constants.SUCCESS;
                            msg = "成功";
                            if(("初级销售").equals(sysRole.getRoleName())||("中级销售").equals(sysRole.getRoleName())||("高级销售").equals(sysRole.getRoleName())||
                                    ("组长").equals(sysRole.getRoleName())||("部长").equals(sysRole.getRoleName())||("销售").equals(sysRole.getRoleName())){
                                    SalespersonConfig salespersonConfig = new SalespersonConfig();
                                    salespersonConfig.setId(CommonUtil.getUUID());
                                    salespersonConfig.setCreateTime(new Date());
                                    salespersonConfig.setDelFlag("0");
                                    salespersonConfig.setMaximunNum(maximunNum);
                                    salespersonConfig.setAdminId(uuid);
                                    salespersonConfigMapper.insertSelective(salespersonConfig);
                                    code = Constants.SUCCESS;
                                    msg = "成功";
                            }
                        }
                    }else{
                        code ="-10";
                        msg = "分配角色不能为空";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result pageList(Integer pageNum, Integer pageSize,Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<Map<String,Object>> mapList = adminMapper.selectAll(params);
            PageInfo page = new PageInfo(mapList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result findById(String adminId) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(adminId)){
                code = "-3";
                msg = "主键id不能为空";
            }else{
                Map<String,Object> map = adminMapper.selectAdminAndRoleByPrimaryKey(adminId);
                result.setData(map);
                code = Constants.SUCCESS;
                msg = "查询成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result update(String adminId, String password, String roleId, String isDelFlag, String surePassword) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(adminId)){
                code = "-3";
                msg = "主键id不能为空";
            }else if(StringUtils.isBlank(password)){
                code = "-4";
                msg = "密码不能为空";
            }else if(StringUtils.isBlank(surePassword)){
                code = "-5";
                msg = "确认密码不能为空";
            }else if(StringUtils.isBlank(isDelFlag)){
                code = "-6";
                msg = "选择是否禁用";
            }else if(!PatternUtil.patternString(password, "password")){
                code = "-7";
                msg = "密码格式不正确";
            }else if(!password.equals(surePassword)){
                code = "-8";
                msg = "两次密码不一致";
            }else if(!isDelFlag.equals(Constants.AdminIsDisable.DISABLE.getIsDisable()) && !isDelFlag.equals(Constants.AdminIsDisable.UNDISABLE.getIsDisable())){
                code = "-9";
                msg = "请正确选择是否禁用";
            }else{
                Admin admin_db = adminMapper.selectByPrimaryKey(adminId);
                if(admin_db == null){
                    code = "-10";
                    msg = "不存在该管理员";
                    result.setCode(code);
                    result.setMsg(msg);
                    return result;
                }
                admin_db.setPassword(password);
                admin_db.setSysRoleId(roleId);
                admin_db.setDelFlag(isDelFlag);
                adminMapper.updateByPrimaryKeySelective(admin_db);
                code = Constants.SUCCESS;
                msg = "修改成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result pageListGroup(String aidianmaoAdminToken,Integer pageNum,Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            PageHelperNew.startPage(pageNum, pageSize);
            List<Admin> list = adminMapper.selectByGroup(adminId);
            PageInfo<Admin> page = new PageInfo(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result updateGroup(String adminId, String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            }else if(StringUtils.isBlank(adminId)){
                code = "-4";
                msg = "组长ID不能为空";
            }else{
                Admin admin = adminMapper.selectByPrimaryKey(id);
                Admin adminGroup = adminMapper.selectById(adminId);
                if(admin!=null&&adminGroup!=null){
                    adminMapper.updateByGroup(adminId,id);
                    code = Constants.SUCCESS;
                    msg = "修改成功";
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result updateDel(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键id不能为空";
            }else{
                Admin admin = adminMapper.selectByPrimaryKey(id);
               if(admin == null){
                   code = "-4";
                   msg = "管理员不存在";
               }else if(!"1".equals(admin.getDelFlag())){
                   code = "-5";
                   msg = "只有禁止的账号才能启用";
               } else {
                   admin.setDelFlag("0");
                   adminMapper.updateByPrimaryKeySelective(admin);
                   code = Constants.SUCCESS;
                   msg = "启用成功";
               }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result delFlag(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键id不能为空";
            }else{
                Admin admin = adminMapper.selectByPrimaryKey(id);
                if(admin == null){
                    code = "-4";
                    msg = "管理员不存在";
                }else if(!"0".equals(admin.getDelFlag())){
                    code = "-5";
                    msg = "只有正常的账号才能禁用";
                } else {
                    admin.setDelFlag("1");
                    adminMapper.updateByPrimaryKeySelective(admin);
                    code = Constants.SUCCESS;
                    msg = "禁用成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }



    @Override
    public Result delete(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键id不能为空";
            }else{
                  adminMapper.deleteByPrimaryKey(id);
                    code = Constants.SUCCESS;
                    msg = "删除成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }




}
