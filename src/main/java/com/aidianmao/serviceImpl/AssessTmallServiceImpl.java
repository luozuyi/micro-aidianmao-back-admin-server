package com.aidianmao.serviceImpl;

import com.aidianmao.entity.AssessTmall;
import com.aidianmao.mapper.AssessTmallMapper;
import com.aidianmao.service.AssessTmallService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
@Transactional
@Service
public class AssessTmallServiceImpl implements AssessTmallService{

    @Autowired
     private AssessTmallMapper assessTmallMapper;
 @Override
 public Result pageAssessTmallList(Integer pageNum, Integer pageSize, Map<String, Object> params){
     Result result = new Result();
     String code = Constants.FAIL;
     String msg = "初始化";
     try {
         PageHelperNew.startPage(pageNum,pageSize);
         List<Map<String,Object>> list = assessTmallMapper.selectAll(params);
         PageInfo<Map<String,Object>> page = new PageInfo<>(list);
         result.setData(page);
         code = Constants.SUCCESS;
         msg = "查询成功";
     } catch (Exception e) {
         e.printStackTrace();
         code = Constants.ERROR;
         msg = "系统繁忙";
     }
     result.setCode(code);
     result.setMsg(msg);
     return result;
 }

    @Override
    public Result detail(String id){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            if (StringUtils.isBlank(id)){
                code = "-3";
                msg = "查看估价结果不存在";
            }else {
                Map<String,Object> map = assessTmallMapper.selectById(id);
                result.setData(map);
                code = Constants.SUCCESS;
                msg = "查询成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

  @Override
  public Result updateAssessResult(AssessTmall assessTmall,String aidianmaoAdminToken) {
      Result result = new Result();
      String code = Constants.FAIL;
      String msg = "初始化";
      try{
          String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
          if(StringUtils.isBlank(assessTmall.getId())){
              code = "-3";
              msg = "主键id不能为空";
          }else if(StringUtils.isBlank(assessTmall.getAssessResult().toString()) || assessTmall.getAssessResult().compareTo(BigDecimal.ZERO) <= 0){
              code = "-4";
              msg = "估价结果不能为空且估价结果大于1";
          }else {
              AssessTmall assessTmall1 = assessTmallMapper.selectByPrimaryKey(assessTmall.getId());
              if(assessTmall1 == null){
                  code = "-5";
                  msg = "估价的店铺不存在";
              }else {
                  assessTmall.setCustomServiceAdminId(adminId);
                  assessTmall.setAdminId(adminId);
                  assessTmallMapper.updateByPrimaryKeySelective(assessTmall);
                  code = Constants.SUCCESS;
                  msg = "估价成功";
              }
          }
      } catch (Exception e) {
          e.printStackTrace();
          code = Constants.ERROR;
          msg = "系统繁忙";
      }
      result.setCode(code);
      result.setMsg(msg);
      return result;
  }

}
