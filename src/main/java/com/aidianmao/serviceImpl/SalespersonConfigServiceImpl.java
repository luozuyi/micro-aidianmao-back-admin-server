package com.aidianmao.serviceImpl;


import com.aidianmao.entity.*;
import com.aidianmao.mapper.*;
import com.aidianmao.service.SalespersonConfigService;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.ListPageUtil;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class SalespersonConfigServiceImpl implements SalespersonConfigService{

    @Autowired
    private SalespersonConfigMapper salespersonConfigMapper;
    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private SysRoleMapper sysRoleMapper;
    @Autowired
    private GroupMapper groupMapper;
    @Autowired
    private DepartmentMapper departmentMapper;
    @Autowired
    private SalespersonTargetMapper salespersonTargetMapper;

    @Override
    public Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params,String adminId) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum,pageSize);
            List<Map<String, Object>> list = salespersonConfigMapper.selectAll(params);
            PageInfo<Map<String,Object>> page = new PageInfo<>(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result divideGroup(String groupName,String[] list) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            Admin admin = adminMapper.selectByRealName(groupName);
            if(admin == null){
                code = "-3";
                msg = "没有此组长，请输入正确信息";
            }
            if(list == null || list.length == 0){
                code = "-3";
                msg = "没有分配的信息";
            }
            for (String id : list){
               SalespersonConfig salespersonConfig = salespersonConfigMapper.selectByPrimaryKey(id);
               Admin admin1 = adminMapper.selectByPrimaryKey(salespersonConfig.getAdminId());
               admin1.setGroupId(admin.getId());
               adminMapper.updateByPrimaryKeySelective(admin1);
                code = Constants.SUCCESS;
                msg = "分配成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result update(String id, Integer maximunNum, String roleName) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
           if(StringUtils.isBlank(id)){
               code = "-3";
               msg = "主键不能为空";
           }else if(maximunNum == null){
               code = "-4";
               msg = "最大限制数不能为空";
           }else if(StringUtils.isBlank(roleName)){
               code = "-5";
               msg = "销售级别不能为空";
            }else {
               SalespersonConfig salespersonConfig = salespersonConfigMapper.selectByPrimaryKey(id);
               if(salespersonConfig == null){
                   code = "-6";
                   msg = "修改的信息不存在";
                   result.setCode(code);
                   result.setMsg(msg);
                   return result;
               }
               Admin admin = adminMapper.selectByPrimaryKey(salespersonConfig.getAdminId());
               if(admin == null){
                   code = "-7";
                   msg = "该业务员不存在";
                   result.setCode(code);
                   result.setMsg(msg);
                   return result;
               }
               SysRole sysRole = sysRoleMapper.selectByRoleName(roleName);
               if(sysRole == null){
                   code = "-8";
                   msg = "没有此级别";
                   result.setCode(code);
                   result.setMsg(msg);
                   return result;
               }
               salespersonConfig.setMaximunNum(maximunNum);
               salespersonConfigMapper.updateByPrimaryKeySelective(salespersonConfig);
               admin.setSysRoleId(sysRole.getId());
               adminMapper.updateByPrimaryKeySelective(admin);
               code = Constants.SUCCESS;
               msg = "修改成功";
           }
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }



    @Override
    public Result update(String id,String departName) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            Admin admin = adminMapper.selectByRealName(departName);
            if(admin == null){
                code = "-3";
                msg = "没有此部长，请输入正确信息";
                result.setCode(code);
                result.setMsg(msg);
                return result;
            }
            Department department = departmentMapper.selectByHeadAdminId(admin.getId());
            if(department == null){
                code = "-4";
                msg = "部门信息有误";
                result.setCode(code);
                result.setMsg(msg);
                return result;
            }
                SalespersonConfig salespersonConfig = salespersonConfigMapper.selectByPrimaryKey(id);
              if(salespersonConfig == null){
                  code = "-5";
                  msg = "业务员信息不存在";
                  result.setCode(code);
                  result.setMsg(msg);
                  return result;
              }
                Admin admin1 = adminMapper.selectByPrimaryKey(salespersonConfig.getAdminId());
                  if(admin1 == null){
                      code = "-6";
                      msg = "业务员信息部存在";
                      result.setCode(code);
                      result.setMsg(msg);
                      return result;
                  }
                Group group = groupMapper.selectByPrimaryKey(admin1.getGroupId());
                  if(group == null){
                      code = "-7";
                      msg = "小组信息不存在";
                      result.setCode(code);
                      result.setMsg(msg);
                      return result;
                  }
                if(!admin1.getId().equals(group.getHeadManAdminId())){
                    code = "-8";
                    msg = "只有组长才能分配到部门";
                    result.setCode(code);
                    result.setMsg(msg);
                    return result;
                }
                group.setDeptId(department.getId());
                adminMapper.updateByPrimaryKeySelective(admin1);
                code = Constants.SUCCESS;
                msg = "分配成功";
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result detail(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
           if(StringUtils.isBlank(id)){
               code = "-3";
               msg = "主键id不能为空";
           }else {
               SalespersonConfig salespersonConfig = salespersonConfigMapper.selectByPrimaryKey(id);
               if(salespersonConfig == null){
                   code = "-4";
                   msg = "查询的信息不存在";
               }else {
                   Map<String, Object> sales = salespersonConfigMapper.selectDetailById(id);
                   result.setData(sales);
                   code = Constants.SUCCESS;
                   msg = "查询成功";
               }
           }
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

}
