package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Achievement;
import com.aidianmao.entity.Admin;
import com.aidianmao.entity.AdminMember;
import com.aidianmao.entity.TmallOrder;
import com.aidianmao.mapper.AchievementMapper;
import com.aidianmao.mapper.AdminMapper;
import com.aidianmao.mapper.TmallOrderMapper;
import com.aidianmao.service.AchievementService;
import com.aidianmao.utils.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class AchievementServiceImpl implements AchievementService {
    @Autowired
    private AchievementMapper achievementMapper;
    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private TmallOrderMapper tmallOrderMapper;
    @Override
    public Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum,pageSize);
            List<Map<String, Object>> list = achievementMapper.selectAll(params);
            PageInfo<Map<String,Object>> page = new PageInfo<>(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result detail(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            }else {
                Achievement achievement = achievementMapper.selectByPrimaryKey(id);
                if(achievement!=null){
                    Map<String,Object> map = achievementMapper.selectById(id);
                    result.setData(map);
                    code = Constants.SUCCESS;
                    msg = "查询成功";
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result update(Achievement record,String note,Integer checkType,Integer checkResult,String aidianmaoAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if (StringUtils.isBlank(record.getId())) {
                code = "-3";
                msg = "主键ID不能为空";
            } else if (checkType == null || (checkType!=1&&checkType!=2&&checkType!=3)) {
                code = "-4";
                msg = "审核类型不能为空或审核类型错误";
            } else {
                Achievement achievement = achievementMapper.selectByPrimaryKey(record.getId());
                if(achievement!=null){
                    if (checkType == 1){
                        if("0".equals(achievement.getStatus())){
                            record.setTime(new Date());
                            record.setStatus("1");
                            achievementMapper.updateByPrimaryKeySelective(record);
                            code = Constants.SUCCESS;
                            msg = "自己作废，更新成功";
                        }else {
                            code = Constants.ERROR;
                            msg = "当前数据已被操作(作废或已审核)或者状态错误";
                        }
                    }else if(checkType == 2){
                        if (checkResult == null || (checkResult!=1&&checkResult!=2)) {
                            code = "-4";
                            msg = "审核结果不能为空或审核结果错误";
                        } else if("0".equals(achievement.getStatus())){
                            if (checkResult == 1) {
                                record.setTime(new Date());
                                record.setStatus("2");
                                record.setOperateGroupId(adminId);
                                record.setNote(note);
                                achievementMapper.updateByPrimaryKeySelective(record);
                                code = Constants.SUCCESS;
                                msg = "组长审核通过，更新成功";
                            }else if (checkResult == 2) {
                                record.setTime(new Date());
                                record.setStatus("3");
                                record.setOperateGroupId(adminId);
                                record.setNote(note);
                                achievementMapper.updateByPrimaryKeySelective(record);
                                code = Constants.SUCCESS;
                                msg = "组长审核不通过，更新成功";
                            }
                        }else {
                            code = Constants.ERROR;
                            msg = "当前数据已被操作(作废或已审核)或者状态错误";
                        }
                    }else if(checkType == 3){
                        if (checkResult == null || (checkResult!=1&&checkResult!=2)) {
                            code = "-4";
                            msg = "审核结果不能为空或审核结果错误";
                        } else if("2".equals(achievement.getStatus())){
                            if (checkResult == 1) {
                                record.setStatus("4");
                                record.setNote(note);
                                achievementMapper.updateByPrimaryKeySelective(record);
                                code = Constants.SUCCESS;
                                msg = "经理核对通过，更新成功";
                            }else if (checkResult == 2) {
                                record.setStatus("5");
                                record.setNote(note);
                                achievementMapper.updateByPrimaryKeySelective(record);
                                code = Constants.SUCCESS;
                                msg = "经理核对不通过，更新成功";
                            }
                        }else {
                            code = Constants.ERROR;
                            msg = "当前数据已审核或者状态错误";
                        }
                    }
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result insert(Achievement record,String buyerAdminName,String sellerAchieveName,String aidianmaoAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if(record.getBuyerAchieve()==null || record.getBuyerAchieve().compareTo(BigDecimal.ZERO)<=0){
                code = "-4";
                msg = "申报人销售业绩不能为空并且不能为非整数";
            }else if(record.getTotalAchieve()==null || record.getTotalAchieve().compareTo(BigDecimal.ZERO)<=0){
                code = "-5";
                msg = "总业绩不能为空并且不能为非正数";
            }else if(StringUtils.isBlank(record.getAchieveOrigin())){
                code = "-6";
                msg = "业绩来源不能为空";
            }else if(StringUtils.isBlank(buyerAdminName)){
                code = "-7";
                msg = "申报人不能为空";
            }else if (StringUtils.isBlank(record.getOrderCode())) {
                code = "-8";
                msg = "订单号不能为空";
            }else {
                TmallOrder tmallOrder = tmallOrderMapper.selectByCode(record.getOrderCode());
                if (tmallOrder == null) {
                    code = "-9";
                    msg = "申报业绩的订单不存在";
                    result.setCode(code);
                    result.setMsg(msg);
                    return result;
                }
                Admin admin = adminMapper.selectByAdminName(buyerAdminName);
                if (admin == null) {
                    code = "-10";
                    msg = "申报人不存在";
                    result.setCode(code);
                    result.setMsg(msg);
                    return result;
                }
                if (record.getSellerAchieve() != null) {
                    if(StringUtils.isBlank(sellerAchieveName)){
                        code = "-13";
                        msg = "分成人不能为空";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    Admin admin1 = adminMapper.selectByAdminName(sellerAchieveName);
                    if (admin1 == null) {
                        code = "-11";
                        msg = "分成人不存在";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    record.setId(CommonUtil.getUUID());
                    record.setCreateTime(new Date());
                    record.setDelFlag("0");
                    record.setBuyerAdminId(admin.getId());
                    record.setSellerAdminId(admin1.getId());
                    record.setStatus("0");
                    BigDecimal newTotalAchieve = record.getBuyerAchieve().add(record.getSellerAchieve());
                    if (newTotalAchieve.compareTo(record.getTotalAchieve()) != 0) {
                        code = "-12";
                        msg = "申报的业绩有问题";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    achievementMapper.insertSelective(record);
                    code = Constants.SUCCESS;
                    msg = "新增成功，买家销售业绩加上卖家销售业绩等于总业绩";
                }else if(record.getSellerAchieve() == null){
                    record.setId(CommonUtil.getUUID());
                    record.setCreateTime(new Date());
                    record.setDelFlag("0");
                    record.setBuyerAdminId(admin.getId());
                    record.setStatus("0");
                    if (record.getBuyerAchieve().compareTo(record.getTotalAchieve()) != 0) {
                        code = "-12";
                        msg = "申报的业绩有问题";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    achievementMapper.insertSelective(record);
                    code = Constants.SUCCESS;
                    msg = "新增成功，买家销售业绩加上卖家销售业绩等于总业绩";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result cancellation(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            }else {
                Achievement achievement = achievementMapper.selectByPrimaryKey(id);
                if(achievement==null){
                    code = "-4";
                    msg = "业绩不存在";
                }else if (!achievement.getStatus().equals("0")){
                    code = "-5";
                    msg = "只有审核中的业绩才能作废";
                }else {
                    achievement.setStatus("1");
                    achievementMapper.updateByPrimaryKeySelective(achievement);
                    code = Constants.SUCCESS;
                    msg = "业绩已作废";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public  Result checkAchieve(String id,String checkType,String note,String aidianmaoAdminToken){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            }else if(StringUtils.isBlank(checkType)|| (checkType.equals("0")&& checkType.equals("1"))) {
                code = "-3";
                msg = "审批结果不能为空,审批结果只能是通过和不通过";
            } else {
                Achievement achievement = achievementMapper.selectByPrimaryKey(id);
                if(achievement==null){
                    code = "-4";
                    msg = "业绩不存在";
                }else if (!achievement.getStatus().equals("0")){
                    code = "-5";
                    msg = "只有审核中的业绩才能审批";
                }else {
                    if(checkType.equals("0")){
                        achievement.setTime(new Date());
                        achievement.setStatus("3");
                        achievement.setOperateGroupId(adminId);
                        achievement.setNote(note);
                        achievementMapper.updateByPrimaryKeySelective(achievement);
                        code = Constants.SUCCESS;
                        msg = "组长审核不通过，更新成功";
                    }else if (checkType.equals("1")){
                        achievement.setTime(new Date());
                        achievement.setStatus("2");
                        achievement.setOperateGroupId(adminId);
                        achievement.setNote(note);
                        achievementMapper.updateByPrimaryKeySelective(achievement);
                        code = Constants.SUCCESS;
                        msg = "组长审核通过，更新成功";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public  Result verifyAchieve(String id,String checkType,String note){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            }else if(StringUtils.isBlank(checkType)|| (checkType.equals("0")&& checkType.equals("1"))) {
                code = "-4";
                msg = "核实结果不能为空,核实结果只能是通过和不通过";
            } else {
                Achievement achievement = achievementMapper.selectByPrimaryKey(id);
                if(achievement==null){
                    code = "-4";
                    msg = "业绩不存在";
                }else if (!achievement.getStatus().equals("2")){
                    code = "-5";
                    msg = "只有组长审核通过的业绩才能核实";
                }else {
                    if(checkType.equals("0")){
                        achievement.setTime(new Date());
                        achievement.setStatus("5");
                        achievement.setNote(note);
                        achievementMapper.updateByPrimaryKeySelective(achievement);
                        code = Constants.SUCCESS;
                        msg = "经理核实失败";
                    }else if (checkType.equals("1")){
                        achievement.setTime(new Date());
                        achievement.setStatus("4");
                        achievement.setNote(note);
                        achievementMapper.updateByPrimaryKeySelective(achievement);
                        code = Constants.SUCCESS;
                        msg = "经理核实成功";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public  Result updateAchieve(String id, String checkType){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            } else {
                Achievement achievement = achievementMapper.selectByPrimaryKey(id);
                if(achievement==null){
                    code = "-4";
                    msg = "业绩不存在";
                }else if (!"4".equals(achievement.getStatus()) && !"5".equals(achievement.getStatus())){
                    code = "-5";
                    msg = "只有经理审核过的业绩才能修改";
                }else {
                    if(checkType.equals("0")){
                        achievement.setTime(new Date());
                        achievement.setStatus("5");
                        achievementMapper.updateByPrimaryKeySelective(achievement);
                        code = Constants.SUCCESS;
                        msg = "修改成功";
                    }else if (checkType.equals("1")){
                        achievement.setTime(new Date());
                        achievement.setStatus("4");
                        achievementMapper.updateByPrimaryKeySelective(achievement);
                        code = Constants.SUCCESS;
                        msg = "修改成功";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


}
