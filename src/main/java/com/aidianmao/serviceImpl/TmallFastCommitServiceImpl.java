package com.aidianmao.serviceImpl;

import com.aidianmao.mapper.TmallFastCommitMapper;
import com.aidianmao.service.TmallFastCommitService;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;
@Transactional
@Service
public class TmallFastCommitServiceImpl implements TmallFastCommitService {

    @Autowired
    private TmallFastCommitMapper tmallFastCommitMapper;

    @Override
    public Result listPageBySelection(Integer pageNum, Integer pageSize, Map<String, Object> params){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            PageHelperNew.startPage(pageNum,pageSize);
            List<ModelMap> fastCommit = tmallFastCommitMapper.selectAll(params);
            PageInfo<ModelMap> page = new PageInfo<>(fastCommit);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


}
