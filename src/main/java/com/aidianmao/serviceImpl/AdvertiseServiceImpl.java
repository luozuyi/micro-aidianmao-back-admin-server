package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Advertise;
import com.aidianmao.mapper.AdvertiseMapper;
import com.aidianmao.service.AdvertiseService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import java.util.Date;
import java.util.List;
import java.util.Map;
@Transactional
@Service
public class AdvertiseServiceImpl implements AdvertiseService {
    @Autowired
    private AdvertiseMapper advertiseMapper;


    @Override
    public Result pageListAdvertise(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
           List<ModelMap> advertise = advertiseMapper.selectAdvertiseAll(params);
            PageInfo<ModelMap> page = new PageInfo<>(advertise);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
   public Result updateDel(String id,String aidianmaoAdminToken){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键id不能为空";
            }else {
                Advertise advertise = advertiseMapper.selectByPrimaryKey(id);
                if(advertise == null){
                    code = "-4";
                    msg = "广告信息不存在";
                }
                advertise.setAdminId(adminId);
                advertise.setDelFlag("1");
                advertiseMapper.updateByPrimaryKeySelective(advertise);
                code = Constants.SUCCESS;
                msg = "禁用成功";
            }

        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result updateDelFlag(String id,String aidianmaoAdminToken){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键id不能为空";
            }else {
                Advertise advertise = advertiseMapper.selectByPrimaryKey(id);
                if(advertise == null){
                    code = "-4";
                    msg = "广告信息不存在";
                }
                advertise.setAdminId(adminId);
                advertise.setDelFlag("0");
                advertiseMapper.updateByPrimaryKeySelective(advertise);
                code = Constants.SUCCESS;
                msg = "启用成功";
            }

        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result updateAdvertise(Advertise advertise,String aidianmaoAdminToken){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if(StringUtils.isBlank(advertise.getId())){
                code = "-3";
                msg = "主键id不能为空";
            }else if(StringUtils.isBlank(advertise.getName())) {
              code = "-4";
              msg = "广告名字不能为空";
            } else if (StringUtils.isBlank(advertise.getType())) {
                code = "-5";
                msg = "广告类型不能为空";
            } else if(StringUtils.isBlank(advertise.getImageSource())) {
               code = "-6";
               msg = "广告图片路径不能为空";
            } else {
                advertise.setAdminId(adminId);
                advertise.setUpdateTime(new Date());
                advertiseMapper.updateByPrimaryKeySelective(advertise);
                code = Constants.SUCCESS;
                msg = "修改成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result deleteAdvertise(String id){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键id不能为空";
            }else {
                Advertise advertise = advertiseMapper.selectByPrimaryKey(id);
                if(advertise == null){
                    code = "-4";
                    msg = "广告已删除或不存在";
                }else {
                    advertiseMapper.deleteByPrimaryKey(id);
                    code = Constants.SUCCESS;
                    msg = "删除成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result addAdvertise(Advertise advertise,String aidianmaoAdminToken){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if(StringUtils.isBlank(advertise.getName())) {
                code = "-3";
                msg = "广告名字不能为空";
            } else if (StringUtils.isBlank(advertise.getType())) {
                code = "-4";
                msg = "广告类型不能为空";
            } else if(StringUtils.isBlank(advertise.getImageSource())) {
                code = "-5";
                msg = "广告图片路径不能为空";
            } else {
                advertise.setId(CommonUtil.getUUID());
                advertise.setDelFlag("0");
                advertise.setCreateTime(new Date());
                advertise.setAdminId(adminId);
                advertise.setUpdateTime(new Date());
                advertiseMapper.insertSelective(advertise);
                code = Constants.SUCCESS;
                msg = "添加成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result detail(String id){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键id不能为空";
            }else {
                Advertise advertise = advertiseMapper.selectByPrimaryKey(id);
                if (advertise == null){
                    code = "-4";
                    msg = "查询的广告不存在";
                }else {
                    result.setData(advertise);
                    code = Constants.SUCCESS;
                    msg = "查询成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
