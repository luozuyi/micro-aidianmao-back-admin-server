package com.aidianmao.serviceImpl;

import com.aidianmao.mapper.ProvincesMapper;
import com.aidianmao.service.ProvincesService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class ProvincesServiceImpl extends BaseServiceImpl<ProvincesMapper> implements ProvincesService {
	

}
