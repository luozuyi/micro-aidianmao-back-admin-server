package com.aidianmao.serviceImpl;

import com.aidianmao.entity.PlatformFundDetail;
import com.aidianmao.mapper.PlatformFundDetailMapper;
import com.aidianmao.service.PlatformFundDetailService;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class PlatformFundDetailServiceImpl implements PlatformFundDetailService{
    @Autowired
    private PlatformFundDetailMapper platformFundDetailMapper;
    @Override
    public Result pageList(Integer pageNum, Integer pageSize, Map<String,Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<PlatformFundDetail> list = platformFundDetailMapper.selectAll(params);
            PageInfo page = new PageInfo(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result pageListTotalIncome(Integer pageNum, Integer pageSize, Map<String,Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            Map<String,Object> map = new HashMap<>();
            List<PlatformFundDetail> list = platformFundDetailMapper.selectAll(params);
            PageInfo<Map<String,Object>> page = new PageInfo(list);
            //ServiceCharge:服务费,CommissionRebate:佣金返利,LiquidatedDamages:违约金,AdjustmentLiquidatedDamages:违约金调整返还,
            // PresentationFee:提现手续费,BankCharges:银行手续费,TotalIncome:总收入.
            BigDecimal ServiceCharge = BigDecimal.ZERO;
            BigDecimal CommissionRebate = BigDecimal.ZERO;
            BigDecimal LiquidatedDamages = BigDecimal.ZERO;
            BigDecimal AdjustmentLiquidatedDamages = BigDecimal.ZERO;
            BigDecimal PresentationFee = BigDecimal.ZERO;
            BigDecimal BankCharges = BigDecimal.ZERO;
            BigDecimal TotalIncome = BigDecimal.ZERO;
            if(list.size()>0){
                for (PlatformFundDetail platformFundDetail: list) {
                    System.out.println(platformFundDetail.getMoneyScene());
                    System.out.println(platformFundDetail.getAmountMoney());
                    if(platformFundDetail.getMoneyScene()==null||platformFundDetail.getAmountMoney()==null){
                        code = Constants.ERROR;
                        msg = "平台收支场景或平台收支金额为空";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    //场景 0:服务费 1:佣金返利 2:违约金 3:违约金调整返还 4:提现手续费 5:银行手续费
                    switch (Integer.valueOf(platformFundDetail.getMoneyScene())){
                        case 0: ServiceCharge = ServiceCharge.add(platformFundDetail.getAmountMoney());
                            break;
                        case 1: CommissionRebate = CommissionRebate.add(platformFundDetail.getAmountMoney());
                            break;
                        case 2: LiquidatedDamages = LiquidatedDamages.add(platformFundDetail.getAmountMoney());
                            break;
                        case 3: AdjustmentLiquidatedDamages = AdjustmentLiquidatedDamages.add(platformFundDetail.getAmountMoney());
                            break;
                        case 4: PresentationFee = PresentationFee.add(platformFundDetail.getAmountMoney());
                            break;
                        case 5: BankCharges = BankCharges.add(platformFundDetail.getAmountMoney());
                            break;
                    }
                }
                //公司总收入=服务费+违约金+提现手续费-佣金返利-违约金调整返还-银行手续费
                TotalIncome = ServiceCharge.add(LiquidatedDamages).add(PresentationFee).subtract(CommissionRebate).subtract(AdjustmentLiquidatedDamages).subtract(BankCharges);
            }
            map.put("TotalIncome",TotalIncome);

            map.put("page",page);
            result.setData(map);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
