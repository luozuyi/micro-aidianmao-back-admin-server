package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Admin;
import com.aidianmao.entity.Department;
import com.aidianmao.entity.DepartmentTarget;
import com.aidianmao.mapper.AdminMapper;
import com.aidianmao.mapper.DepartmentMapper;
import com.aidianmao.mapper.DepartmentTargetMapper;
import com.aidianmao.service.DepartmentTargetService;
import com.aidianmao.utils.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
@Transactional
@Service
public class DepartmentTargetServiceImpl implements DepartmentTargetService {
    @Autowired
    private DepartmentTargetMapper departmentTargetMapper;
    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private DepartmentMapper departmentMapper;
    @Override
    public Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum,pageSize);
            List<Map<String, Object>> list = departmentTargetMapper.selectAll(params);
            PageInfo<Map<String,Object>> page = new PageInfo<>(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result detail(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            }else {
                DepartmentTarget departmentTarget = departmentTargetMapper.selectByPrimaryKey(id);
                if(departmentTarget!=null){
                    Map<String,Object> map = departmentTargetMapper.selectById(id);
                    result.setData(map);
                    code = Constants.SUCCESS;
                    msg = "查询成功";
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result update(DepartmentTarget record) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(record.getId())) {
                code = "-3";
                msg = "主键ID不能为空";

            }else if(record.getMonthGoal()==null || record.getMonthGoal().compareTo(BigDecimal.ZERO)<=0){
                code = "-4";
                msg = "部门月目标不能为空并且不能为非正数";
            }else if(StringUtils.isBlank(record.getMonth())){
                code = "-5";
                msg = "月份不能为空";
            } else {
                DepartmentTarget departmentTarget = departmentTargetMapper.selectByPrimaryKey(record.getId());
                if(departmentTarget!=null){
                    departmentTargetMapper.updateByPrimaryKeySelective(record);
                    code = Constants.SUCCESS;
                    msg = "更新成功";
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result delete(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            }else {
                DepartmentTarget departmentTarget = departmentTargetMapper.selectByPrimaryKey(id);
                if(departmentTarget!=null){
                    departmentTargetMapper.deleteByPrimaryKey(id);
                    code = Constants.SUCCESS;
                    msg = "删除成功";
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result insert(DepartmentTarget record,String departName) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(departName)){
                code = "-3";
                msg = "部长名字不能为空";
            }else if(record.getMonthGoal()==null || record.getMonthGoal().compareTo(BigDecimal.ZERO)<=0){
                code = "-4";
                msg = "部门月目标不能为空并且不能为非正数";
            }else if(StringUtils.isBlank(record.getMonth())){
                code = "-5";
                msg = "月份不能为空";
            }else {
                record.setId(CommonUtil.getUUID());
                record.setCreateTime(new Date());
                record.setDelFlag("0");
                Admin admin = adminMapper.selectByAdminName(departName);
                if(admin == null){
                    code = "-6";
                    msg = "部长信息不存在，请输入正确的信息";
                    result.setCode(code);
                    result.setMsg(msg);
                    return result;
                }
                List<Department> depart = departmentMapper.selectByHeadManAdminId(admin.getId());
                if(depart.size() ==1){
                    Department department = depart.get(0);
                    record.setDepartmentId(department.getId());
                    record.setYear(RandomCodeUtil.dateToYear(new Date()));
                    departmentTargetMapper.insertSelective(record);
                    code = Constants.SUCCESS;
                    msg = "新增成功";
                }else {
                    code = "-7";
                    msg = "部长信息有误";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
