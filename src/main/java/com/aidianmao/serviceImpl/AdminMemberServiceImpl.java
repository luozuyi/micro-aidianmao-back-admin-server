package com.aidianmao.serviceImpl;

import com.aidianmao.entity.AdminMember;
import com.aidianmao.entity.Member;
import com.aidianmao.entity.MemberRemark;
import com.aidianmao.entity.SalespersonConfig;
import com.aidianmao.mapper.AdminMemberMapper;
import com.aidianmao.mapper.MemberMapper;
import com.aidianmao.mapper.MemberRemarkMapper;
import com.aidianmao.mapper.SalespersonConfigMapper;
import com.aidianmao.service.AdminMemberRecordService;
import com.aidianmao.service.AdminMemberService;
import com.aidianmao.service.MemberRemarkService;
import com.aidianmao.utils.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class AdminMemberServiceImpl implements AdminMemberService {
    @Autowired
    private AdminMemberMapper adminMemberMapper;
    @Autowired
    private AdminMemberRecordService adminMemberRecordService;
    @Autowired
    private MemberRemarkService memberRemarkService;
    @Autowired
    private MemberMapper memberMapper;
    @Autowired
    private MemberRemarkMapper memberRemarkMapper;
   @Autowired
   private SalespersonConfigMapper salespersonConfigMapper;
    @Override
    public Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum,pageSize);
            List<Map<String, Object>> list = adminMemberMapper.selectAll(params);
            PageInfo<Map<String,Object>> page = new PageInfo<>(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result pageListByAdmin(Integer pageNum, Integer pageSize, Map<String, Object> params,String aidianmaoAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            params.put("adminId",adminId);
            PageHelperNew.startPage(pageNum,pageSize);
            List<Map<String, Object>> list = adminMemberMapper.selectByAdminId(params);
            PageInfo<Map<String,Object>> page = new PageInfo<>(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result delete(String id,String content,String memberId,Integer operationType,String aidianmaoAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            }else if(StringUtils.isBlank(content)){
                code = "-4";
                msg = "备注信息不能为空";
            }else if(StringUtils.isBlank(memberId)){
                code = "-5";
                msg = "会员ID不能为空";
            }else if(operationType==null|| (operationType!=1&&operationType!=2)){
                code = "-6";
                msg = "操作类型不能为空或类型错误";
            }else {
                AdminMember adminMember = adminMemberMapper.selectByPrimaryKey(id);
                if(adminMember!=null){
                    adminMemberMapper.deleteByPrimaryKey(id);
                    memberRemarkService.insert(content,memberId,aidianmaoAdminToken);
                    adminMemberRecordService.insert(memberId,operationType,aidianmaoAdminToken);
                    code = Constants.SUCCESS;
                    msg = "删除成功";
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result insert(AdminMember record,String memberId,Integer operationType,String aidianmaoAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if(StringUtils.isBlank(record.getMemberId())){
                code = "-3";
                msg = "会员ID不能为空";
            }else {
                record.setAdminId(adminId);
                record.setId(CommonUtil.getUUID());
                record.setCreateTime(new Date());
                record.setDelFlag("0");
                adminMemberMapper.insertSelective(record);
                adminMemberRecordService.insert(memberId,operationType,aidianmaoAdminToken);
                code = Constants.SUCCESS;
                msg = "新增成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result selectMember(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum,pageSize);
            List<Map<String, Object>> list = adminMemberMapper.selectMember(params);
            PageInfo<Map<String,Object>> page = new PageInfo<>(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result addMember(String id, String aidianmaoAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
             if (StringUtils.isBlank(id)){
                 code = "-3";
                 msg = "会员id不能为空";
                 result.setCode(code);
                 result.setMsg(msg);
                 return result;
             }
             AdminMember adminMember1 = adminMemberMapper.selectByMemberId(id);
             if(adminMember1 != null){
                 code = "-4";
                 msg = "该会员已有业务员";
                 result.setCode(code);
                 result.setMsg(msg);
                 return result;
             }
            SalespersonConfig salespersonConfig = salespersonConfigMapper.selectByAdminId(adminId);
             if(salespersonConfig == null){
                 code = "-4";
                 msg = "请给该业务员加最大客户数";
                 result.setCode(code);
                 result.setMsg(msg);
                 return result;
             }
             Member member = memberMapper.selectByPrimaryKey(id);
             if(member == null){
                 code = "-5";
                 msg = "该会员不存在";
             }else {
                 AdminMember adminMember = new AdminMember();
                 adminMember.setId(CommonUtil.getUUID());
                 adminMember.setDelFlag("0");
                 adminMember.setCreateTime(new Date());
                 adminMember.setAdminId(adminId);
                 adminMember.setMemberId(member.getId());
                 Integer count = adminMemberMapper.selectCount(adminId);
             if(count < salespersonConfig.getMaximunNum()){
                 adminMemberMapper.insertSelective(adminMember);
                 code = Constants.SUCCESS;
                 msg = "成功加入我的客户组";
             }else {
                 code = "-5";
                 msg = "您的客户数已达到您的最大客户数";
             }
             }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
      }

    @Override
    public Result insertMember(String phone) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(phone)){
                code = "-3";
                msg = "会员手机号不能为空";
            }else if(!PatternUtil.patternString(phone,"mobile")){
                code = "-4";
                msg = "请你输入正确的手机格式";
            }else {
                List<Member> list = memberMapper.selectByPhone(phone);
                if (list.size() != 0) {
                    code = "-5";
                    msg = "会员已存在";
                } else {
                    Member member = new Member();
                    member.setId(CommonUtil.getUUID());
                    member.setDelFlag("0");
                    member.setCreateTime(new Date());
                    member.setPhone(phone);
                    memberMapper.insertSelective(member);
                    code = Constants.SUCCESS;
                    msg = "添加会员成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result selectAdminMember(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum,pageSize);
            List<Map<String, Object>> list = adminMemberMapper.selectAdminMember(params);
            PageInfo<Map<String,Object>> page = new PageInfo<>(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result deleteMany(String ids) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String[] admin = ids.split(",");
            String[] Strings = new String[admin.length];
            for (int i = 0; i< admin.length; i++){
                Strings[i] = admin[i];
            }
            Map<String,Object> map = new HashMap<>();
            map.put("list",Strings);
            adminMemberMapper.deleteMany(map);
            code = Constants.SUCCESS;
            msg = "移除成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result selectSellAdminAll(Integer pageNum, Integer pageSize, Map<String, Object> params,String aidianmaoAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            params.put("adminId",adminId);
            PageHelperNew.startPage(pageNum,pageSize);
            List<Map<String, Object>> list = adminMemberMapper.selectSellAdminAll(params);
            PageInfo<Map<String,Object>> page = new PageInfo<>(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result selectSellById(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
              if(StringUtils.isBlank(id)){
                  code = "-3";
                  msg = "主键id不能为空";
                  result.setCode(code);
                  result.setMsg(msg);
                  return result;
              }else {
                  AdminMember adminMember = adminMemberMapper.selectByPrimaryKey(id);
                  if(adminMember == null){
                      code = "-4";
                      msg = "会员与销售信息不能为空";
                      result.setCode(code);
                      result.setMsg(msg);
                      return result;
                  }else {
                      Map<String, Object> list =  adminMemberMapper.selectSellById(id);
                      result.setData(list);
                      code = Constants.SUCCESS;
                      msg = "查询成功";
                  }
              }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result updateAdminMember(String id,String identity,String importanceDegree) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键id不能为空";
            }else if(StringUtils.isBlank(identity)){
                code = "-4";
                msg = "会员身份不能为空";
            }else if(StringUtils.isBlank(importanceDegree)){
                code = "-5";
                msg = "会员等级不能为空";
            }
            else {
                AdminMember adminMember = adminMemberMapper.selectByPrimaryKey(id);
                if (adminMember != null){
                    Member member = memberMapper.selectByPrimaryKey(adminMember.getMemberId());
                    if(member != null){
                        member.setIdentity(identity);
                        member.setImportanceDegree(importanceDegree);
                        memberMapper.updateByPrimaryKeySelective(member);
                        code = Constants.SUCCESS;
                        msg = "修改成功";
                    }else {
                        code = "-6";
                        msg = "会员信息不存在";
                    }
                }else {
                    code = "-7";
                    msg = "会员与销售信息不存在";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result insertContent(String id,String content,String aidianmaoAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "会员id不能为空";
            }else if(StringUtils.isBlank(content)){
                code = "-4";
                msg = "备注不能为空";
            } else {
             AdminMember adminMember = adminMemberMapper.selectByPrimaryKey(id);
                  if(adminMember == null){
                      code = "-5";
                      msg = "会员与销售信息已不存在";
                      result.setCode(code);
                      result.setMsg(msg);
                      return result;
                  }
                        MemberRemark memberRemark = new MemberRemark();
                        memberRemark.setId(CommonUtil.getUUID());
                        memberRemark.setDelFlag("0");
                        memberRemark.setCreateTime(new Date());
                        memberRemark.setAdminId(adminId);
                        memberRemark.setContent(content);
                        memberRemark.setMemberId(adminMember.getMemberId());
                        memberRemarkMapper.insertSelective(memberRemark);
                        code = Constants.SUCCESS;
                        msg = "添加成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result delete(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键id不能为空";
            }else {
                   adminMemberMapper.deleteByPrimaryKey(id);
                    code = Constants.SUCCESS;
                    msg = "移除成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


}
