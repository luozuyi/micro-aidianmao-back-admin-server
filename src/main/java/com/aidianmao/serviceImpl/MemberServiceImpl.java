package com.aidianmao.serviceImpl;

import com.aidianmao.entity.IncomeExpensesDetail;
import com.aidianmao.entity.Member;
import com.aidianmao.entity.MemberFundAdjust;
import com.aidianmao.entity.MemberRemark;
import com.aidianmao.esEntity.EsMember;
import com.aidianmao.mapper.IncomeExpensesDetailMapper;
import com.aidianmao.mapper.MemberFundAdjustMapper;
import com.aidianmao.mapper.MemberMapper;
import com.aidianmao.mapper.MemberRemarkMapper;
import com.aidianmao.repository.MemberRepository;
import com.aidianmao.service.MemberService;
import com.aidianmao.utils.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.ModelMap;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static org.elasticsearch.index.query.QueryBuilders.rangeQuery;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;


@Transactional
@Service
public class MemberServiceImpl implements MemberService {

    @Autowired
    private MemberMapper memberMapper;
    @Autowired
    private MemberRemarkMapper memberRemarkMapper;
    @Autowired
    private MemberFundAdjustMapper memberFundAdjustMapper;
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;
    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private IncomeExpensesDetailMapper incomeExpensesDetailMapper;


    @Override
    public Result pageList(Integer pageNum, Integer pageSize,Map<String, Object> params){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<ModelMap> members = memberMapper.selectAllMember(params);
            PageInfo<ModelMap> page = new PageInfo(members);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result update(Member member){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(member.getId())) {
                code = "-3";
                msg = "主键ID不能为空";
            } else {
                Member member1 = memberMapper.selectByPrimaryKey(member.getId());
                if (member1 == null){
                    code = "-4";
                    msg = "修改的会员不存在";
                }else {
                    memberMapper.updateByPrimaryKeySelective(member);
                    code = Constants.SUCCESS;
                    msg = "修改成功";
                }
            }
            }catch(Exception e){
                e.printStackTrace();
                code = Constants.ERROR;
                msg = "系统繁忙";
            }
            result.setCode(code);
            result.setMsg(msg);
            return result;
        }

    @Override
    public Result detailIncomeExpenses(String id,Integer pageNum, Integer pageSize,Map<String, Object> params){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
                params.put("id",id);
                PageHelperNew.startPage(pageNum, pageSize);
                List<ModelMap> income = memberMapper.selectIncomeExpensesDetailById(params);
                PageInfo<ModelMap> page = new PageInfo(income);
                result.setData(page);
                code = Constants.SUCCESS;
                msg = "查询成功";

        }catch(Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }



    @Override
    public Result detailMember(String id){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键ID不能为空";
            }else {
                Member members = memberMapper.selectMemberById(id);
                result.setData(members);
                code = Constants.SUCCESS;
                msg = "查询成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }



    @Override
    public Result fundAdjust(Member member, BigDecimal adjustMoney,String note,String aidianmaoAdminToken){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            MemberFundAdjust memberFundAdjust = new MemberFundAdjust();
            if(StringUtils.isBlank(member.getId())){
                code ="-3";
                msg = "会员不能为空";
            }else if (adjustMoney == null  || adjustMoney.compareTo(BigDecimal.ZERO)<=0) {
                code = "-4";
                msg = "调整后的资金不能为空不能为负";
            }else if(StringUtils.isBlank(note)){
                code = "-5";
                msg = "调整说明不能为空";
            } else {
                Member member1 = memberMapper.selectByPrimaryKey(member.getId());
                if (member1 == null) {
                    code = "-6";
                    msg = "资金调整的会员不存在";
                } else {
                    BigDecimal money1 = adjustMoney.subtract(member.getMoney());
                    if (money1.compareTo(BigDecimal.ZERO) >= 0) {
                        memberFundAdjust.setId(CommonUtil.getUUID());
                        memberFundAdjust.setCreateTime(new Date());
                        memberFundAdjust.setDelFlag("0");
                        memberFundAdjust.setMoney(money1);
                        memberFundAdjust.setFundType("0");
                        memberFundAdjust.setIncomeType("0");
                        memberFundAdjust.setMemberId(member.getId());
                        memberFundAdjust.setAdminId(adminId);
                        memberFundAdjust.setNote(note);
                        memberFundAdjustMapper.insertSelective(memberFundAdjust);
                        code = Constants.SUCCESS;
                        msg = "资金调整成功";
                    } else {
                        BigDecimal money2 = member.getMoney().subtract(adjustMoney);
                        memberFundAdjust.setId(CommonUtil.getUUID());
                        memberFundAdjust.setCreateTime(new Date());
                        memberFundAdjust.setDelFlag("0");
                        memberFundAdjust.setMoney(money2);
                        memberFundAdjust.setFundType("0");
                        memberFundAdjust.setIncomeType("1");
                        memberFundAdjust.setMemberId(member.getId());
                        memberFundAdjust.setAdminId(adminId);
                        memberFundAdjust.setNote(note);
                        memberFundAdjustMapper.insertSelective(memberFundAdjust);
                        code = Constants.SUCCESS;
                        msg = "资金调整成功";
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
      public Result listCustomer(Member member,Integer pageNum, Integer pageSize){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        Integer size = memberMapper.selectMemberCount();
        try {
            TermQueryBuilder termQueryBuilder1 = null;
            //new 一个BoolQueryBuilder，类似于StringBuffer，然后判断是否为空，将条件拼接
            BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
            if(StringUtils.isNotBlank(member.getPhone())){
                termQueryBuilder1 = termQuery("phone",member.getPhone());
                boolQueryBuilder.must(termQueryBuilder1);
            }
            if(StringUtils.isNotBlank(member.getIdentity())){
                termQueryBuilder1 = termQuery("identity",member.getIdentity());
                boolQueryBuilder.must(termQueryBuilder1);
            }
            if(StringUtils.isNotBlank(member.getImportanceDegree())){
                termQueryBuilder1 = termQuery("importanceDegree",member.getImportanceDegree());
                boolQueryBuilder.must(termQueryBuilder1);
            }
            //将拼接的条件直接放入查询
            NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder().withQuery(boolQueryBuilder);
            Pageable pageable = PageRequest.of(0,size);
            nativeSearchQueryBuilder.withPageable(pageable).withSort(SortBuilders.fieldSort("createTime").order(SortOrder.DESC));
            //得到的最终的结果进行build创建一个query语句
            SearchQuery searchQuery = nativeSearchQueryBuilder.withPageable(pageable).build();
            List<EsMember> members = elasticsearchTemplate.queryForList(searchQuery,EsMember.class);
            //自定义分页
            ListPageUtil page = new ListPageUtil(members.size(),pageNum,pageSize,members);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }




    @Override
    public Result pageListCustomer(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            Pageable pageable = new PageRequest(pageNum,pageSize);
            //PageHelperNew.startPage(pageNum, pageSize);
            TermQueryBuilder termQueryBuilder=null;
            //new 一个BoolQueryBuilder，类似于StringBuffer，然后判断是否为空，将条件拼接
            BoolQueryBuilder boolQueryBuilder=new BoolQueryBuilder();
            if(params!=null){
                if (params.get("userName")!=null&&!StringUtils.isEmpty(params.get("userName").toString())){
                    //termQuery是完全匹配，
                    termQueryBuilder=termQuery("userName", params.get("userName"));
                    boolQueryBuilder.must(termQueryBuilder);
                }
                if (params.get("phone")!=null&&!StringUtils.isEmpty(params.get("phone").toString())){
                    termQueryBuilder=termQuery("phone",params.get("phone"));
                    boolQueryBuilder.must(termQueryBuilder);
                }
            }
            //将拼接的条件直接放入查询
            NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder().withQuery(boolQueryBuilder).withPageable(pageable);
            //得到的最终的结果进行build创建一个query语句
            SearchQuery searchQuery=nativeSearchQueryBuilder.build();
            List<EsMember> list = elasticsearchTemplate.queryForList(searchQuery, EsMember.class);
            //List<Map<String,Object>> list = memberMapper.selectAllCustomer(params);
            //PageInfo<EsMember> page = new PageInfo<>(list);
            result.setData(list);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result detailCustomer(String memberId) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(memberId)){
                code = "-3";
                msg = "主键ID不能为空";
            }else {
                Member member = memberMapper.selectMemberById(memberId);
                if(member!=null){
                    Map<String,Object> map = memberMapper.selectCustomerById(memberId);
                    List<Map<String,Object>> list = memberRemarkMapper.selectByMemberId(memberId);
                    list.add(0,map);
                    result.setData(list);
                    code = Constants.SUCCESS;
                    msg = "查询成功";
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result updateCustomer(Member record,String content,String aidianmaoAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(record.getId())) {
                code = "-3";
                msg = "主键ID不能为空";
            } else if (StringUtils.isBlank(record.getIdentity())){
                code = "-4";
                msg = "会员身份不能为空";
            }else if(StringUtils.isBlank(record.getImportanceDegree())){
                code = "-5";
                msg = "会员等级不能为空";
            }else {
                Member member = memberMapper.selectMemberById(record.getId());
                if(member!=null){
                    if(!StringUtils.isBlank(content)){
                        String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
                        MemberRemark memberRemark = new MemberRemark();
                        memberRemark.setId(CommonUtil.getUUID());
                        memberRemark.setCreateTime(new Date());
                        memberRemark.setDelFlag("0");
                        memberRemark.setAdminId(adminId);
                        memberRemark.setMemberId(record.getId());
                        memberRemark.setContent(content);
                        memberRemarkMapper.insertSelective(memberRemark);
                        memberMapper.updateCustomerSelective(record);
                        code = Constants.SUCCESS;
                        msg = "备注信息添加成功，更新成功";
                    }else {
                        memberMapper.updateCustomerSelective(record);
                        code = Constants.SUCCESS;
                        msg = "更新成功";
                    }
                }else {
                    code = Constants.ERROR;
                    msg = "当前数据不存在或主键ID已被修改";
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result pageListCustomerWeek(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            Date currentLoginTimeEnd = new Date();
            Date currentLoginTimeStart = RandomCodeUtil.dateSubWeek(currentLoginTimeEnd);
            params.put("currentLoginTimeStart",currentLoginTimeStart);
            params.put("currentLoginTimeEnd",currentLoginTimeEnd);
            PageHelperNew.startPage(pageNum, pageSize);
            List<Map<String,Object>> list = memberMapper.selectByWeek(params);
            PageInfo<Map<String,Object>> page = new PageInfo<>(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
