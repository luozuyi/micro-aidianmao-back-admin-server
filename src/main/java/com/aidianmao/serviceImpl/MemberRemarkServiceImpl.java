package com.aidianmao.serviceImpl;

import com.aidianmao.entity.MemberRemark;
import com.aidianmao.mapper.MemberRemarkMapper;
import com.aidianmao.service.MemberRemarkService;
import com.aidianmao.utils.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class MemberRemarkServiceImpl implements MemberRemarkService {
    @Autowired
    private MemberRemarkMapper memberRemarkMapper;
    @Override
    public Result pageList(String memberId) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            List<Map<String, Object>> list = memberRemarkMapper.selectByMemberId(memberId);
            result.setData(list);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result insert(String content,String memberId,String aidianmaoAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if(StringUtils.isBlank(memberId)){
                code = "-3";
                msg = "会员ID不能为空";
            }else if(StringUtils.isBlank(content)){
                code = "-4";
                msg = "备注内容不能为空";
            }else {
                MemberRemark memberRemark = new MemberRemark();
                memberRemark.setId(CommonUtil.getUUID());
                memberRemark.setCreateTime(new Date());
                memberRemark.setDelFlag("0");
                memberRemark.setAdminId(adminId);
                memberRemark.setMemberId(memberId);
                memberRemark.setContent(content);
                memberRemarkMapper.insertSelective(memberRemark);
                code = Constants.SUCCESS;
                msg = "新增成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result detail(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
          if(StringUtils.isBlank(id)){
              code ="-3";
              msg = "主键id不能为空";
          }else {
             MemberRemark memberRemark = memberRemarkMapper.selectByPrimaryKey(id);
             if (memberRemark == null){
                 code = "-4";
                 msg = "备注信息不存在";
                 result.setCode(code);
                 result.setMsg(msg);
                 return result;
             }
             Map<String,Object> remark = memberRemarkMapper.selectById(id);
              result.setData(remark);
              code = Constants.SUCCESS;
              msg = "查询成功";
          }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
