package com.aidianmao.serviceImpl;

import com.aidianmao.entity.*;
import com.aidianmao.esEntity.EsTmallOrder;
import com.aidianmao.mapper.*;
import com.aidianmao.service.TmallOrderService;
import com.aidianmao.utils.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;



import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static org.elasticsearch.index.query.QueryBuilders.rangeQuery;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;


@Transactional
@Service
public class TmallOrderServiceImpl implements TmallOrderService {
    @Autowired
    private TmallOrderMapper tmallOrderMapper;
    @Autowired
    private TmallMapper tmallMapper;
    @Autowired
    private MemberMapper memberMapper;
    @Autowired
    private TmallAbnormalOrderRecordMapper tmallAbnormalOrderRecordMapper;
    @Autowired
    private PlatformFundDetailMapper platformFundDetailMapper;
    @Autowired
    private IncomeExpensesDetailMapper incomeExpensesDetailMapper;
    @Autowired
    private PlatformMessageMapper platformMessageMapper;
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;
    @Autowired
    private TmallOrderFreezeMapper tmallOrderFreezeMapper;

    @Override
    public Result pageList(String pageNum, String pageSize, EsTmallOrder order) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        Integer size = tmallOrderMapper.selectTmallOrderCount();
        try {
            if (StringUtils.isBlank(pageNum)){
                pageNum="1";
            }
            if (StringUtils.isBlank(pageSize)){
                pageSize="10";
            }
            TermQueryBuilder termQueryBuilder1 = null;
            //new 一个BoolQueryBuilder，类似于StringBuffer，然后判断是否为空，将条件拼接
            BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
            if (StringUtils.isNotBlank(order.getCode())) {
                //boolQueryBuilder.must(matchQuery("code", order.getCode()));
                termQueryBuilder1 = termQuery("code", order.getCode());
                boolQueryBuilder.must(termQueryBuilder1);
            }

            if (StringUtils.isNotBlank(order.getBuyName())) {
                //boolQueryBuilder.must(matchQuery("buyName", order.getBuyName()));
               termQueryBuilder1 = termQuery("buyName", order.getBuyName());
               boolQueryBuilder.must(termQueryBuilder1);
            }
            if (StringUtils.isNotBlank(order.getShopName())) {
              //  boolQueryBuilder.must(matchQuery("shopName", order.getShopName()));
                termQueryBuilder1 = termQuery("shopName",order.getShopName());
                boolQueryBuilder.must(termQueryBuilder1);
            }
            if (StringUtils.isNotBlank(order.getSaleName())) {
               // boolQueryBuilder.must(matchQuery("saleName", order.getSaleName()));
               termQueryBuilder1 = termQuery("saleName", order.getSaleName());
               boolQueryBuilder.must(termQueryBuilder1);
            }
            if (StringUtils.isNotBlank(order.getBeginPrice())) {
                boolQueryBuilder.filter(rangeQuery("price").gte(order.getBeginPrice()));
            }
            if (StringUtils.isNotBlank(order.getEndPrice())) {
                boolQueryBuilder.filter(rangeQuery("price").lte(order.getEndPrice()));
            }
            if (StringUtils.isNotBlank(order.getBeginTime())) {
                boolQueryBuilder.filter(rangeQuery("createTime").gte(order.getBeginTime()));
            }
            if (StringUtils.isNotBlank(order.getEndTime())) {
                boolQueryBuilder.filter(rangeQuery("createTime").lte(order.getEndTime()));
            }
            //将拼接的条件直接放入查询
            NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder().withQuery(boolQueryBuilder);
             Pageable pageable = PageRequest.of(0,size);
             nativeSearchQueryBuilder.withPageable(pageable).withSort(SortBuilders.fieldSort("createTime").order(SortOrder.DESC));
            //得到的最终的结果进行build创建一个query语句
            SearchQuery searchQuery = nativeSearchQueryBuilder.withPageable(pageable).build();
            List<EsTmallOrder> tmallOrders = elasticsearchTemplate.queryForList(searchQuery, EsTmallOrder.class);
            //自定义分页
           ListPageUtil page = new ListPageUtil(tmallOrders.size(),Integer.parseInt(pageNum),Integer.parseInt(pageSize),tmallOrders);
         //  PageInfo page = new PageInfo(tmallOrders);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result detailById(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(id)) {
                code = "-3";
                msg = "查看对象不存在";
            } else {
                TmallOrder order = tmallOrderMapper.selectByPrimaryKey(id);
                if(order == null){
                    code = "-4";
                    msg = "订单信息不存在";
                } else {
                    Map<String, Object> list = tmallOrderMapper.detailById(id);
                    result.setData(list);
                    code = Constants.SUCCESS;
                    msg = "查询成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result paymentDetailById(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(id)) {
                code = "-3";
                msg = "查看对象不存在";
            } else {
                TmallOrder order = tmallOrderMapper.selectByPrimaryKey(id);
                if(order == null){
                    code = "-4";
                    msg = "订单信息不存在";
                } else {
                    Map<String, Object> list = tmallOrderMapper.paymentDetailById(id);
                    result.setData(list);
                    code = Constants.SUCCESS;
                    msg = "查询成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result payDetailById(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(id)) {
                code = "-3";
                msg = "查看对象不存在";
            } else {
                TmallOrder order = tmallOrderMapper.selectByPrimaryKey(id);
                if(order == null){
                    code = "-4";
                    msg = "订单信息不存在";
                }else if(!order.getStatus().equals("1")){
                    code = "-5";
                    msg = "此为支付定金订单详情";
                } else {
                    Map<String, Object> list = tmallOrderMapper.payDetailById(id);
                    result.setData(list);
                    code = Constants.SUCCESS;
                    msg = "查询成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result statusDetailById(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(id)) {
                code = "-3";
                msg = "查看对象不存在";
            } else {
                TmallOrder order = tmallOrderMapper.selectByPrimaryKey(id);
                if(order == null){
                    code = "-4";
                    msg = "订单信息不存在";
                }else if(!order.getStatus().equals("0") && !order.getStatus().equals("4")){
                    code = "-5";
                    msg = "此为取消订单或待付款的订单的详情";
                }else {
                    Map<String, Object> list = tmallOrderMapper.statusDetailById(id);
                    result.setData(list);
                    code = Constants.SUCCESS;
                    msg = "查询成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result pageListByStatus0(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            params.put("status", "0");
            PageHelperNew.startPage(pageNum, pageSize);
            List<Map<String, Object>> mapList1 = tmallOrderMapper.selectByStatus(params);
            PageInfo<Map<String, Object>> page = new PageInfo<>(mapList1);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result pageListByStatus1(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            params.put("status", "1");
            PageHelperNew.startPage(pageNum, pageSize);
            List<Map<String, Object>> mapList1 = tmallOrderMapper.selectByStatus(params);
            PageInfo<Map<String, Object>> page = new PageInfo<>(mapList1);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result pageListByStatus3(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            params.put("status", "3");
            PageHelperNew.startPage(pageNum, pageSize);
            List<Map<String, Object>> mapList1 = tmallOrderMapper.selectByStatus(params);
            PageInfo<Map<String, Object>> page = new PageInfo<>(mapList1);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result pageListByStatus4(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            params.put("status", "4");
            PageHelperNew.startPage(pageNum, pageSize);
            List<Map<String, Object>> mapList1 = tmallOrderMapper.selectByStatus(params);
            PageInfo<Map<String, Object>> page = new PageInfo<>(mapList1);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result pageListByStatus6(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            params.put("status", "6");
            PageHelperNew.startPage(pageNum, pageSize);
            List<Map<String, Object>> mapList1 = tmallOrderMapper.selectByStatus(params);
            PageInfo<Map<String, Object>> page = new PageInfo<>(mapList1);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result transferStatus(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<Map<String, Object>> mapList1 = tmallOrderMapper.selectByTransferStatus(params);
            PageInfo<Map<String, Object>> page = new PageInfo<>(mapList1);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result selectFundAdjustAll(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            params.put("status", "7");
            List<Map<String, Object>> fundList = tmallOrderMapper.selectFundAdjustAll(params);
            PageInfo<Map<String, Object>> page = new PageInfo<>(fundList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result transfer(String id, String aidianmaoAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if (StringUtils.isBlank(id)) {
                code = "-3";
                msg = "主键id不能为空";
            } else {
                TmallOrder tmallOrder = tmallOrderMapper.selectByPrimaryKey(id);
                Tmall tmall = tmallMapper.selectByPrimaryKey(tmallOrder.getTmallId());
                if (tmallOrder == null) {
                    code = "-4";
                    msg = "订单不存在";
                }else if (!tmallOrder.getStatus().equals("2")){
                    code = "-5";
                    msg = "只有交付全款的订单才能交接";
                } else {
                    tmallOrder.setTakeOverUserId(adminId);
                    tmallOrder.setStatus("8");
                    tmallOrderMapper.updateByPrimaryKeySelective(tmallOrder);
                    // 修改店铺状态
                    tmall.setStatus("6");
                    tmallMapper.updateByPrimaryKeySelective(tmall);
                    Map<String, Object> takeOverName = tmallOrderMapper.selectTakeOverNameById(id);
                    result.setData(takeOverName);
                    code = Constants.SUCCESS;
                    msg = "交接成功";
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result cancelTransfer(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(id)) {
                code = "-3";
                msg = "主键id不能为空";
            } else {
                    TmallOrder tmallOrder = tmallOrderMapper.selectByPrimaryKey(id);
                    Tmall tmall = tmallMapper.selectByPrimaryKey(tmallOrder.getTmallId());
                    if (tmallOrder == null) {
                        code = "-4";
                        msg = "订单不存在";
                    }else if(!tmallOrder.getStatus().equals("8")){
                        code = "-5";
                        msg = "只有交接中的订单才能取消交接";
                    }else {
                        tmallOrder.setTakeOverUserId(" ");
                        tmallOrder.setStatus("2");
                        tmallOrderMapper.updateByPrimaryKeySelective(tmallOrder);
                        // 修改店铺状态
                        tmall.setStatus("5");
                        tmallMapper.updateByPrimaryKeySelective(tmall);
                        Map<String, Object> takeOverName = tmallOrderMapper.selectTakeOverNameById(id);
                        result.setData(takeOverName);
                        code = Constants.SUCCESS;
                        msg = "取消交接成功";
                    }
                }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result terminationOrder(String id, String aidianmaoAdminToken, String responsibility) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if (StringUtils.isBlank(id)) {
                code = "-3";
                msg = "主键id不能为空";
            } else if (StringUtils.isBlank(responsibility) || (!"0".equals(responsibility) && !"1".equals(responsibility))) {
                // 0：买家责任  1：销售责任
                code = "-4";
                msg = "责任不能为空且只有买家责任和销售责任";
            } else {
                TmallOrder tmallOrder = tmallOrderMapper.selectByPrimaryKey(id);
                if (tmallOrder == null) {
                    code = "-5";
                    msg = "订单不存在";
                } else {
                    Tmall tmall = tmallMapper.selectByPrimaryKey(tmallOrder.getTmallId());
                    Member member = memberMapper.selectByPrimaryKey(tmallOrder.getBuyMemberId());
                    if (tmallOrder.getStatus().equals("1")) {
                        if (responsibility.equals("1")) {
                            String title = "尊敬的爱店猫用户:";
                            String content = "销售责任：：\n" + "您的订单被终止交易，购店资金将返还到账户。如有疑问，请咨询在线客服或拨打咨询热线4008359100。";
                            platformMessageMapper.insertSelective(new PlatformMessage(CommonUtil.getUUID(), new Date(), "0", content, tmallOrder.getSaleMemberId(), title, "0"));
                            // 修改会员资金
                            BigDecimal money = tmall.getShopPrice().multiply(new BigDecimal("0.2"));
                            member.setFreezeMoney(member.getFreezeMoney().subtract(money));
                            member.setMoney(member.getMoney().add(money));
                            memberMapper.updateByPrimaryKeySelective(member);
                            //添加买家收支明细
                            IncomeExpensesDetail incomeExpensesDetail = new IncomeExpensesDetail();
                            incomeExpensesDetail.setId(CommonUtil.getUUID());
                            incomeExpensesDetail.setCreateTime(new Date());
                            incomeExpensesDetail.setDelFlag("0");
                            //收支类型 0:收入 1:支出
                            incomeExpensesDetail.setType("0");
                            //场景:0:充值 1:佣金返利 2:差价返利 3:违约定金返还 4:违约金调整返还 5:支付余额返还
                            //6:全款购买 7:店铺余额 8:定金 9:违约金 10:提现 11:交易成功 12:违约全款返还 13:差价返利扣除 14:充值调整
                            //15:提现退还 16: 提现手续费 17:免提现手续费
                            incomeExpensesDetail.setMoneyScene("3");
                            incomeExpensesDetail.setAmountMoney(money);
                            incomeExpensesDetail.setMemberId(tmallOrder.getBuyMemberId());
                            incomeExpensesDetailMapper.insertSelective(incomeExpensesDetail);
                            // 修改订单冻结表状态为解冻
                            List<TmallOrderFreeze> tmallOrderFreezes = tmallOrderFreezeMapper.selectByTmallOrderId(tmallOrder.getId());
                            for (TmallOrderFreeze tmallOrderFreeze : tmallOrderFreezes) {
                                tmallOrderFreeze.setStatus("1");
                                tmallOrderFreezeMapper.updateByPrimaryKeySelective(tmallOrderFreeze);
                            }
                        } else {
                            BigDecimal money = tmall.getShopPrice().multiply(new BigDecimal("0.2"));
                            String title = "尊敬的爱店猫用户:";
                            String content = "买家责任：：\n" + "您的订单已被终止交易，已从账户扣除" + money + "元预定金部分作为违约金。如有疑问，请咨询在线客服或拨打咨询热线4008359100。";
                            platformMessageMapper.insertSelective(new PlatformMessage(CommonUtil.getUUID(), new Date(), "0", content, tmallOrder.getSaleMemberId(), title, "0"));
                            // 修改会员资金
                            member.setFreezeMoney(member.getFreezeMoney().subtract(money));
                            memberMapper.updateByPrimaryKeySelective(member);
                            // 修改订单冻结表状态为解冻
                            List<TmallOrderFreeze> tmallOrderFreezes = tmallOrderFreezeMapper.selectByTmallOrderId(tmallOrder.getId());
                            for (TmallOrderFreeze tmallOrderFreeze : tmallOrderFreezes) {
                                tmallOrderFreeze.setStatus("1");
                                tmallOrderFreezeMapper.updateByPrimaryKeySelective(tmallOrderFreeze);
                            }
                            //添加买家收支明细
                            IncomeExpensesDetail incomeExpensesDetail = new IncomeExpensesDetail();
                            incomeExpensesDetail.setId(CommonUtil.getUUID());
                            incomeExpensesDetail.setCreateTime(new Date());
                            incomeExpensesDetail.setDelFlag("0");
                            //收支类型 0:收入 1:支出
                            incomeExpensesDetail.setType("1");
                            //场景:0:充值 1:佣金返利 2:差价返利 3:违约定金返还 4:违约金调整返还 5:支付余额返还
                            //6:全款购买 7:店铺余额 8:定金 9:违约金 10:提现 11:交易成功 12:违约全款返还 13:差价返利扣除 14:充值调整
                            //15:提现退还 16: 提现手续费 17:免提现手续费
                            incomeExpensesDetail.setMoneyScene("9");
                            incomeExpensesDetail.setAmountMoney(money);
                            incomeExpensesDetail.setMemberId(tmallOrder.getBuyMemberId());
                            incomeExpensesDetailMapper.insertSelective(incomeExpensesDetail);

                            // 添加平台收支明细
                            PlatformFundDetail platformFundDetail = new PlatformFundDetail();
                            platformFundDetail.setId(CommonUtil.getUUID());
                            platformFundDetail.setCreateTime(new Date());
                            platformFundDetail.setDelFlag("0");
                            platformFundDetail.setRelationId(tmallOrder.getId());
                            //关联类型 0:订单id 1:提现id
                            platformFundDetail.setRelationType("0");
                            //收支类型 0:收入 1:支出
                            platformFundDetail.setType("0");
                            //场景 0:服务费 1:佣金返利 2:违约金 3:违约金调整返还 4:提现手续费 5:银行手续费
                            platformFundDetail.setMoneyScene("2");
                            platformFundDetail.setAmountMoney(money);
                            platformFundDetailMapper.insertSelective(platformFundDetail);
                        }
                        // 添加异常订单
                        TmallAbnormalOrderRecord abnormalOrderRecord = new TmallAbnormalOrderRecord();
                        TmallAbnormalOrderRecord record = tmallAbnormalOrderRecordMapper.selectByOrderId(tmallOrder.getId());
                        if (record == null) {
                            abnormalOrderRecord.setId(CommonUtil.getUUID());
                            abnormalOrderRecord.setDelFlag("0");
                            abnormalOrderRecord.setCreateTime(new Date());
                            abnormalOrderRecord.setAdminId(adminId);
                            abnormalOrderRecord.setOrderId(tmallOrder.getId());
                            abnormalOrderRecord.setResponsibilityType(responsibility);
                            tmallAbnormalOrderRecordMapper.insertSelective(abnormalOrderRecord);
                        }
                        // 订单状态为异常订单（无资金）
                        tmallOrder.setStatus("6");
                        tmallOrderMapper.updateByPrimaryKeySelective(tmallOrder);
                        // 修改店铺状态
                        tmall.setStatus("0");
                        tmallMapper.updateByPrimaryKeySelective(tmall);
                        code = Constants.SUCCESS;
                        msg = "终止交易成功";
                    } else if (tmallOrder.getStatus().equals("8") || tmallOrder.getStatus().equals("2")) {
                        if (responsibility.equals("1")) {
                            String title = "尊敬的爱店猫用户:";
                            String content = "销售责任：\n" + "您的订单被终止交易，购店资金将返还到账户。如有疑问，请咨询在线客服或拨打咨询热线4008359100。";
                            platformMessageMapper.insertSelective(new PlatformMessage(CommonUtil.getUUID(), new Date(), "0", content, tmallOrder.getSaleMemberId(), title, "0"));
                            // 修改会员资金
                            BigDecimal money = tmallOrder.getPrice();
                            member.setFreezeMoney(member.getFreezeMoney().subtract(money));
                            member.setMoney(member.getMoney().add(money));
                            memberMapper.updateByPrimaryKeySelective(member);
                            // 修改订单冻结表状态为解冻
                            List<TmallOrderFreeze> tmallOrderFreezes = tmallOrderFreezeMapper.selectByTmallOrderId(tmallOrder.getId());
                            for (TmallOrderFreeze tmallOrderFreeze : tmallOrderFreezes) {
                                tmallOrderFreeze.setStatus("1");
                                tmallOrderFreezeMapper.updateByPrimaryKeySelective(tmallOrderFreeze);
                            }
                            //添加会员收支明细
                            IncomeExpensesDetail incomeExpensesDetail = new IncomeExpensesDetail();
                            incomeExpensesDetail.setId(CommonUtil.getUUID());
                            incomeExpensesDetail.setCreateTime(new Date());
                            incomeExpensesDetail.setDelFlag("0");
                            //收支类型 0:收入 1:支出
                            incomeExpensesDetail.setType("0");
                            //场景:0:充值 1:佣金返利 2:差价返利 3:违约定金返还 4:违约金调整返还 5:支付余额返还
                            //6:全款购买 7:店铺余额 8:定金 9:违约金 10:提现 11:交易成功 12:违约全款返还 13:差价返利扣除 14:充值调整
                            incomeExpensesDetail.setMoneyScene("12");
                            incomeExpensesDetail.setAmountMoney(money);
                            incomeExpensesDetail.setMemberId(tmallOrder.getBuyMemberId());
                            incomeExpensesDetailMapper.insertSelective(incomeExpensesDetail);
                        } else {
                            BigDecimal money =  tmall.getShopPrice().multiply(new BigDecimal("0.2"));;
                            BigDecimal money1 = tmallOrder.getPrice().subtract(money);
                            String title = "尊敬的爱店猫用户:";
                            String content = "买家责任：：\n" + "您的订单已被终止交易，已从账户扣除" + money + "元预定金部分作为违约金。如有疑问，请咨询在线客服或拨打咨询热线4008359100。";
                            platformMessageMapper.insertSelective(new PlatformMessage(CommonUtil.getUUID(), new Date(), "0", content, tmallOrder.getSaleMemberId(), title, "0"));
                            // 修改会员资金
                            member.setFreezeMoney(member.getFreezeMoney().subtract(tmallOrder.getPrice()));
                            member.setMoney(member.getMoney().add(money1));
                            memberMapper.updateByPrimaryKeySelective(member);
                            //添加会员收支明细
                            IncomeExpensesDetail incomeExpensesDetail = new IncomeExpensesDetail();
                            incomeExpensesDetail.setId(CommonUtil.getUUID());
                            incomeExpensesDetail.setCreateTime(new Date());
                            incomeExpensesDetail.setDelFlag("0");
                            //收支类型 0:收入 1:支出
                            incomeExpensesDetail.setType("0");
                            //场景:0:充值 1:佣金返利 2:差价返利 3:违约定金返还 4:违约金调整返还 5:支付余额返还
                            //6:全款购买 7:店铺余额 8:定金 9:违约金 10:提现 11:交易成功 12:违约全款返还 13:差价返利扣除 14:充值调整
                            incomeExpensesDetail.setMoneyScene("5");
                            incomeExpensesDetail.setAmountMoney(money1);
                            incomeExpensesDetail.setMemberId(tmallOrder.getBuyMemberId());
                            incomeExpensesDetailMapper.insertSelective(incomeExpensesDetail);
                            //添加买家收支明细
                            IncomeExpensesDetail incomeDetail = new IncomeExpensesDetail();
                            incomeDetail.setId(CommonUtil.getUUID());
                            incomeDetail.setCreateTime(new Date());
                            incomeDetail.setDelFlag("0");
                            //收支类型 0:收入 1:支出
                            incomeDetail.setType("1");
                            //场景:0:充值 1:佣金返利 2:差价返利 3:违约定金返还 4:违约金调整返还 5:支付余额返还
                            //6:全款购买 7:店铺余额 8:定金 9:违约金 10:提现 11:交易成功 12:违约全款返还 13:差价返利扣除 14:充值调整
                            //15:提现退还 16: 提现手续费 17:免提现手续费
                            incomeDetail.setMoneyScene("9");
                            incomeDetail.setAmountMoney(money);
                            incomeDetail.setMemberId(tmallOrder.getBuyMemberId());
                            incomeExpensesDetailMapper.insertSelective(incomeDetail);
                            // 修改订单冻结表状态为解冻
                            List<TmallOrderFreeze> tmallOrderFreezes = tmallOrderFreezeMapper.selectByTmallOrderId(tmallOrder.getId());
                            for (TmallOrderFreeze tmallOrderFreeze : tmallOrderFreezes) {
                                tmallOrderFreeze.setStatus("1");
                                tmallOrderFreezeMapper.updateByPrimaryKeySelective(tmallOrderFreeze);
                            }
                            // 添加平台收支明细
                            PlatformFundDetail platformFundDetail = new PlatformFundDetail();
                            platformFundDetail.setId(CommonUtil.getUUID());
                            platformFundDetail.setCreateTime(new Date());
                            platformFundDetail.setDelFlag("0");
                            platformFundDetail.setRelationId(tmallOrder.getId());
                            //关联类型 0:订单id 1:提现id
                            platformFundDetail.setRelationType("0");
                            //收支类型 0:收入 1:支出
                            platformFundDetail.setType("0");
                            //场景 0:服务费 1:佣金返利 2:违约金 3:违约金调整返还 4:提现手续费 5:银行手续费
                            platformFundDetail.setMoneyScene("2");
                            platformFundDetail.setAmountMoney(money);
                            platformFundDetailMapper.insertSelective(platformFundDetail);
                        }
                        // 添加异常订单
                        TmallAbnormalOrderRecord abnormalOrderRecord = new TmallAbnormalOrderRecord();
                        TmallAbnormalOrderRecord record = tmallAbnormalOrderRecordMapper.selectByOrderId(tmallOrder.getId());
                        if (record == null) {
                            abnormalOrderRecord.setId(CommonUtil.getUUID());
                            abnormalOrderRecord.setDelFlag("0");
                            abnormalOrderRecord.setCreateTime(new Date());
                            abnormalOrderRecord.setAdminId(adminId);
                            abnormalOrderRecord.setOrderId(tmallOrder.getId());
                            abnormalOrderRecord.setResponsibilityType(responsibility);
                            tmallAbnormalOrderRecordMapper.insertSelective(abnormalOrderRecord);
                        }
                        // 订单状态为异常订单（无资金）
                        tmallOrder.setStatus("6");
                        tmallOrderMapper.updateByPrimaryKeySelective(tmallOrder);
                        // 修改店铺状态
                        tmall.setStatus("0");
                        tmallMapper.updateByPrimaryKeySelective(tmall);
                        code = Constants.SUCCESS;
                        msg = "终止交易成功";
                    }
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result loanOrder(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(id)) {
                code = "-3";
                msg = "主键id不能为空";
            } else {
                TmallOrder tmallOrder = tmallOrderMapper.selectByPrimaryKey(id);
                if (tmallOrder == null) {
                    code = "-5";
                    msg = "订单不存在";
                } else {
                    if (tmallOrder.getStatus().equals("8")) {
                        Tmall tmall = tmallMapper.selectByPrimaryKey(tmallOrder.getTmallId());
                        Member buyMember = memberMapper.selectByPrimaryKey(tmallOrder.getBuyMemberId());
                        Member saleMember = memberMapper.selectByPrimaryKey(tmallOrder.getSaleMemberId());
                        BigDecimal money = tmall.getShopPrice().multiply(new BigDecimal("0.2"));
                        BigDecimal money1 = tmallOrder.getPrice().subtract(money);
                        // 修改买家会员资金
                        buyMember.setFreezeMoney(buyMember.getFreezeMoney().subtract(tmallOrder.getPrice()));
                        memberMapper.updateByPrimaryKeySelective(buyMember);
                        // 修改卖家会员资金
                        saleMember.setMoney(saleMember.getMoney().add(money1));
                        memberMapper.updateByPrimaryKeySelective(saleMember);
                        //添加卖家收支明细
                        IncomeExpensesDetail incomeExpensesDetail = new IncomeExpensesDetail();
                        incomeExpensesDetail.setId(CommonUtil.getUUID());
                        incomeExpensesDetail.setCreateTime(new Date());
                        incomeExpensesDetail.setDelFlag("0");
                        //收支类型 0:收入 1:支出
                        incomeExpensesDetail.setType("0");
                        //场景:0:充值 1:佣金返利 2:差价返利 3:违约定金返还 4:违约金调整返还 5:支付余额返还
                        //6:全款购买 7:店铺余额 8:定金 9:违约金 10:提现 11:交易成功 12:违约全款返还 13:差价返利扣除 14:充值调整
                        incomeExpensesDetail.setMoneyScene(" 11");
                        incomeExpensesDetail.setAmountMoney(money1);
                        incomeExpensesDetail.setMemberId(tmallOrder.getSaleMemberId());
                        incomeExpensesDetailMapper.insertSelective(incomeExpensesDetail);
                        // 修改订单冻结表状态为解冻
                        List<TmallOrderFreeze> tmallOrderFreezes = tmallOrderFreezeMapper.selectByTmallOrderId(tmallOrder.getId());
                        for (TmallOrderFreeze tmallOrderFreeze : tmallOrderFreezes) {
                            tmallOrderFreeze.setStatus("1");
                            tmallOrderFreezeMapper.updateByPrimaryKeySelective(tmallOrderFreeze);
                        }
                        //修改订单状态为交易成功
                        tmallOrder.setStatus("3");
                        tmallOrder.setUpdateTime(new Date());
                        tmallOrderMapper.updateByPrimaryKeySelective(tmallOrder);
                        // 修改店铺状态
                        tmall.setStatus("7");
                        tmallMapper.updateByPrimaryKeySelective(tmall);
                        // 添加平台收支明细
                        PlatformFundDetail platformFundDetail = new PlatformFundDetail();
                        platformFundDetail.setId(CommonUtil.getUUID());
                        platformFundDetail.setCreateTime(new Date());
                        platformFundDetail.setDelFlag("0");
                        platformFundDetail.setRelationId(tmallOrder.getId());
                        //关联类型 0:订单id 1:提现id
                        platformFundDetail.setRelationType("0");
                        //收支类型 0:收入 1:支出
                        platformFundDetail.setType("0");
                        //场景 0:服务费 1:佣金返利 2:违约金 3:违约金调整返还 4:提现手续费 5:银行手续费
                        platformFundDetail.setMoneyScene("0");
                        platformFundDetail.setAmountMoney(money);
                        platformFundDetailMapper.insertSelective(platformFundDetail);
                        code = Constants.SUCCESS;
                        msg = "成功";
                    }else {
                        code = "-6";
                        msg = "只有交接中的订单才能放款";
                    }
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result saleSule(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(id)) {
                code = "-3";
                msg = "主键id不能为空";
            } else {
                TmallOrder tmallOrder = tmallOrderMapper.selectByPrimaryKey(id);
                if (tmallOrder == null) {
                    code = "-4";
                    msg = "订单不存在";
                } else {
                    // 卖家是否确认 0:未确认 1:已确认
                    tmallOrder.setIsSaleSure("1");
                    tmallOrderMapper.updateByPrimaryKeySelective(tmallOrder);
                    code = Constants.SUCCESS;
                    msg = "卖家待确认成功";
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}

