package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Proposal;
import com.aidianmao.mapper.ProposalMapper;
import com.aidianmao.service.ProposalService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.ModelMap;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class ProposalServiceImpl implements ProposalService {
    @Autowired
    private ProposalMapper proposalMapper;

    @Override
    public Result pagaList(Integer pageNum, Integer pageSize, Map<String,Object> params){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            PageHelperNew.startPage(pageNum,pageSize);
            List<ModelMap> ProposeList = proposalMapper.selectAll(params);
            PageInfo<ModelMap> page = new PageInfo<>(ProposeList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result detail(String id){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "查看建议不存在";
            }else {
                Map<String,Object> map = proposalMapper.selectById(id);
                result.setData(map);
                code = Constants.SUCCESS;
                msg = "查询成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result update(Proposal proposal,String aidianmaoAdminToken){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if (StringUtils.isBlank(proposal.getId())){
                code = "-3";
                msg = "主键id不能为空";
            }else if (StringUtils.isBlank(proposal.getReply())){
                code = "-4";
                msg = "回复内容不能为空";
            }else if (StringUtils.isBlank(proposal.getProposal())){
                code = "-5";
                msg = "建议内容不能为空";
            } else {
                Proposal proposal1 = proposalMapper.selectByPrimaryKey(proposal.getId());
                if(proposal1 == null){
                    code = "-6";
                    msg = "修改的建议不存在";
                }else {
                    proposal.setAdminId(adminId);
                    proposal.setStatus("1");
                    proposal.setReplyTime(new Date());
                    proposalMapper.updateByPrimaryKeySelective(proposal);
                    code = Constants.SUCCESS;
                    msg = "修改成功";
                }
            }
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


        @Override
        public  Result deleteProposal(String id,String aidianmaoAdminToken){
            Result result = new Result();
            String code = Constants.FAIL;
            String msg = "初始化";
            try {
                String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
                if(StringUtils.isBlank(id)){
                    code = "-3";
                    msg = "请选择删除对象";
                }else {
                    Proposal proposal = proposalMapper.selectByPrimaryKey(id);
                    if (proposal == null){
                        code = "-4";
                        msg = "删除对象不存在";
                    }else {
                        proposal.setDelFlag("1");
                        proposal.setAdminId(adminId);
                        proposalMapper.updateByPrimaryKeySelective(proposal);
                        code = Constants.SUCCESS;
                        msg = "删除成功";
                    }
                }
            } catch (Exception e) {
                code = Constants.ERROR;
                msg = "后台繁忙";
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                e.printStackTrace();
            }
            result.setCode(code);
            result.setMsg(msg);
            return result;
        }

}
