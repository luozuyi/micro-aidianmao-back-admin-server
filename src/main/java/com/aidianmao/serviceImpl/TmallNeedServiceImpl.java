package com.aidianmao.serviceImpl;


import com.aidianmao.entity.*;
import com.aidianmao.mapper.*;
import com.aidianmao.service.TmallNeedService;
import com.aidianmao.utils.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.ModelMap;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Transactional
@Service
public class TmallNeedServiceImpl implements TmallNeedService{
   @Autowired
   private TmallNeedMapper tmallNeedMapper;
   @Autowired
   private ProvincesCitysCountrysMapper provincesCitysCountrysMapper;
   @Autowired
   private AdminMemberMapper adminMemberMapper;
   @Autowired
   private PlatformMessageMapper platformMessageMapper;
   @Autowired
   private SalespersonConfigMapper salespersonConfigMapper;
    @Override
  public Result listPageBySelection(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<ModelMap> NeedList = tmallNeedMapper.selectAll(params);
            PageInfo<ModelMap> page = new PageInfo<>(NeedList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result updateTmallNeed(TmallNeed tmallNeed, ProvincesCitysCountrys provincesCitysCountrys,String aidianmaoAdminToken){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if(StringUtils.isBlank(tmallNeed.getId())){
            code ="-3";
            msg = "修改对象不存在";
             }else if(tmallNeed.getCount() == null || tmallNeed.getCount() <= 0){
                code = "-4";
                msg ="求购数量不能为空或少于1";
            }else if (StringUtils.isBlank(tmallNeed.getPrice().toString()) || tmallNeed.getPrice().compareTo(BigDecimal.ZERO) <= 0){
                code ="-5";
                msg ="求购的起始价格不能为空且不能为负";
            }else if (StringUtils.isBlank(tmallNeed.getPhone())){
                code ="-6";
                msg ="求购者电话不能为空";
            }else if (StringUtils.isBlank(tmallNeed.getNeedSubject())){
                code = "-7";
                msg = "求购主题不能为空";
            }else if (StringUtils.isBlank(tmallNeed.getTmallType())){
                code = "-8";
                msg ="天猫商城类型不能为空";
            }else if (StringUtils.isBlank(tmallNeed.getBrandType())){
                code = "-9";
                msg = "商标类型不能为空";
            }else if (StringUtils.isBlank(tmallNeed.getStatus()) || (!"0".equals(tmallNeed.getStatus())&&!"1".equals(tmallNeed.getStatus())&&!"2".equals(tmallNeed.getStatus())&&!"3".equals(tmallNeed.getStatus()))){
                code = "-10";
                msg ="审核状态不能为空或状态错误";
            } else if (StringUtils.isBlank(tmallNeed.getNeedType())){
                code ="-11";
                msg = "求购产品类目不能为空";
            }else if(!PatternUtil.patternString(tmallNeed.getPhone(),"mobile")){
                code = "-12";
                msg = "手机格式不正确";
            } else if (StringUtils.isBlank(tmallNeed.getRemark())){
                code = "-13";
                msg = "买家描述不能为空";
            }else if(StringUtils.isBlank(tmallNeed.getNote())){
                code = "-14";
                msg = "审核备注不能为空";
            }else if(StringUtils.isBlank(tmallNeed.getShopCreateTime())){
              code = "-15";
              msg = "创店时间不能为空";
            } else if(StringUtils.isBlank(tmallNeed.getName())) {
                code = "-16";
                msg = "求购姓名不能为空";
            } else if (StringUtils.isBlank(tmallNeed.getEndPrice().toString()) || tmallNeed.getEndPrice().compareTo(BigDecimal.ZERO) <=0){
                code = "-17";
                msg = "求购的截至价格不能为空且不能为负";
            } else if (StringUtils.isBlank(tmallNeed.getTaxPayerType()) || (!"0".equals(tmallNeed.getTaxPayerType())&&!"1".equals(tmallNeed.getTaxPayerType()))){
                code = "-18";
                msg = "求购者的纳税人的性质不能为空且正确填写纳税人性质类型";
            } else if(StringUtils.isBlank(provincesCitysCountrys.getProvincesId())){
                code = "-19";
                msg = "请选择省";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getCitysId())){
                code = "-20";
                msg = "请选择城市";
            /*}else if(StringUtils.isBlank(provincesCitysCountrys.getProvincesName())){
                code = "-21";
                msg = "省名称";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getCitysName())) {
                code = "-22";
                msg = "市名称";*/
            } else {
                TmallNeed Need1 = tmallNeedMapper.selectByPrimaryKey(tmallNeed.getId());
                if (Need1 == null) {
                    code = "-23";
                    msg = "审核的求购不存在";
                } else {
                    String shopAddressId = Need1.getAddressId();
                    if (StringUtils.isBlank(shopAddressId)) {
                        code = "24";
                        msg = "前台校验有问题，地址ID不能为空";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }

                    ProvincesCitysCountrys provincesCitys = provincesCitysCountrysMapper.selectByPrimaryKey(shopAddressId);
                    if (provincesCitys == null) {
                        code = "-25";
                        msg = "地址主键ID不存在或者已经被修改";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    provincesCitys.setProvincesId(provincesCitysCountrys.getProvincesId());
                    provincesCitys.setCitysId(provincesCitysCountrys.getCitysId());
                    provincesCitysCountrysMapper.updateByPrimaryKeySelective(provincesCitys);
                    // 添加站内信息
                    if ("1".equals(tmallNeed.getStatus())) {
                        String title = "求购需求成功发布";
                        String content = "尊敬的爱店猫用户：\n" + "您的求购需求，经过审核成功发布。如有疑问，请咨询在线客服或拨打咨询热线4008359100。";
                        platformMessageMapper.insertSelective(new PlatformMessage(CommonUtil.getUUID(), new Date(), "0", content, Need1.getMemberId(), title, "0"));
                        code = Constants.SUCCESS;
                        msg = "成功";
                    } else if ("2".equals(tmallNeed.getStatus())) {
                        String title = "求购需求发布失败";
                        String content = "尊敬的爱店猫用户：\n" + "您的求购需求经过审核，不符合要求，发布失败。如有疑问，请咨询在线客服或拨打咨询热线4008359100。";
                        platformMessageMapper.insertSelective(new PlatformMessage(CommonUtil.getUUID(), new Date(), "0", content, Need1.getMemberId(), title, "0"));
                        code = Constants.SUCCESS;
                        msg = "成功";
                    } else if ("3".equals(tmallNeed.getStatus())) {
                        String title = "店铺已下架";
                        String content = "尊敬的爱店猫用户：\n" + "您的店铺已下架。如有疑问，请咨询在线客服或拨打咨询热线4008359100。";
                        platformMessageMapper.insertSelective(new PlatformMessage(CommonUtil.getUUID(), new Date(), "0", content, Need1.getMemberId(), title, "0"));
                        code = Constants.SUCCESS;
                        msg = "成功";
                    }
                    tmallNeed.setAdminId(adminId);
                    tmallNeed.setApproveTime(new Date());
                    tmallNeedMapper.updateByPrimaryKeySelective(tmallNeed);
                    code = Constants.SUCCESS;
                    msg = "修改成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result selectById(String id){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键不能为空";
            }else {
                TmallNeed tmallNeed = tmallNeedMapper.selectByPrimaryKey(id);
                if(tmallNeed == null){
                    code = "-4";
                    msg = "查看求购信息不存在";
                }else {
                    Map<String,Object> map = tmallNeedMapper.selectById(id);
                    result.setData(map);
                    code = Constants.SUCCESS;
                    msg = "查询成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result saleNeed(Integer pageNum, Integer pageSize, Map<String, Object> params){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            PageHelperNew.startPage(pageNum,pageSize);
            List<ModelMap> NeedList = tmallNeedMapper.selectAllSale(params);
            PageInfo<ModelMap> page = new PageInfo<>(NeedList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result saleRelation(String id,String aidianmaoAdminToken){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键不能为空";
            }else {
                TmallNeed tmallNeed = tmallNeedMapper.selectByPrimaryKey(id);
                if (tmallNeed == null) {
                    code = "-4";
                    msg = "查看求购信息不存在";
                } else {
                    SalespersonConfig salespersonConfig = salespersonConfigMapper.selectByAdminId(adminId);
                    if(salespersonConfig == null){
                        code = "-6";
                        msg = "请给该业务员加最大客户数";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    AdminMember adminMember = adminMemberMapper.selectByMemberId(tmallNeed.getMemberId());
                    if(adminMember != null){
                        code = "-5";
                        msg = "该会员已有业务员";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                        AdminMember adminMember1 = new AdminMember();
                        adminMember1.setId(CommonUtil.getUUID());
                        adminMember1.setDelFlag("0");
                        adminMember1.setCreateTime(new Date());
                        adminMember1.setMemberId(tmallNeed.getMemberId());
                        adminMember1.setAdminId(adminId);
                        Integer count = adminMemberMapper.selectCount(adminId);
                        if (count < salespersonConfig.getMaximunNum()) {
                            adminMemberMapper.insertSelective(adminMember1);
                            code = Constants.SUCCESS;
                            msg = "我去联系成功";
                        } else {
                            code = "-7";
                            msg = "您的客户数已达到您的最大客户数";
                        }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
}

    @Override
    public Result saleDetail(String id){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键不能为空";
            }else {
                TmallNeed tmallNeed = tmallNeedMapper.selectByPrimaryKey(id);
                if(tmallNeed == null){
                    code = "-4";
                    msg = "查看求购信息不存在";
                }else {
                    Map<String,Object> map = tmallNeedMapper.selectSaleDetailById(id);
                    result.setData(map);
                    code = Constants.SUCCESS;
                    msg = "查询成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


}
