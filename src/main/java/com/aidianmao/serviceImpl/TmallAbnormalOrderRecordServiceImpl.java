package com.aidianmao.serviceImpl;


import com.aidianmao.mapper.TmallAbnormalOrderRecordMapper;
import com.aidianmao.mapper.TmallOrderMapper;
import com.aidianmao.service.TmallAbnormalOrderRecordService;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;
@Transactional
@Service
public class TmallAbnormalOrderRecordServiceImpl implements TmallAbnormalOrderRecordService {
      @Autowired
     private TmallAbnormalOrderRecordMapper tmallAbnormalOrderRecordMapper;
      @Autowired
      private TmallOrderMapper tmallOrderMapper;

    @Override
    public Result listPage(Integer pageNum, Integer pageSize, Map<String, Object> params){

        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            PageHelperNew.startPage(pageNum,pageSize);
            List<ModelMap> orderRecord = tmallAbnormalOrderRecordMapper.selectAll(params);
            PageInfo<ModelMap> page = new PageInfo<>(orderRecord);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result detail(String id){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "查看订单不存在";
            }else {
                Map<String,Object> map = tmallAbnormalOrderRecordMapper.selectById(id);
                result.setData(map);
                code = Constants.SUCCESS;
                msg = "查询成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }



}
