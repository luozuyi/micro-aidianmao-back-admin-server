package com.aidianmao.serviceImpl;

import com.aidianmao.entity.CustomerServiceQq;
import com.aidianmao.mapper.CustomerServiceQqMapper;
import com.aidianmao.service.CustomerServiceQqService;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.Result;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class CustomerServiceQqServiceImpl implements CustomerServiceQqService{

    @Autowired
    private CustomerServiceQqMapper customerServiceQqMapper;

    @Override
    public Result selectNameByType(String type){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(type)){
                code = "-3";
                msg = "类型不能为空";
            }else {
              List<CustomerServiceQq> name = customerServiceQqMapper.selectNameByType(type);
                result.setData(name);
                code = Constants.SUCCESS;
                msg = "查询成功";
            }
            } catch (Exception e) {
                e.printStackTrace();
                code = Constants.ERROR;
                msg = "系统繁忙";
            }
            result.setCode(code);
            result.setMsg(msg);
            return result;
        }

        @Override
        public Result update(String list[]){
            Result result = new Result();
            String code = Constants.FAIL;
            String msg = "初始化";
            try {
                for (String id : list){
                CustomerServiceQq customerList = customerServiceQqMapper.selectByPrimaryKey(id);
               if("1".equals(customerList.getStatus())){
                  customerList.setStatus("0");
                  customerServiceQqMapper.updateByPrimaryKeySelective(customerList);
              }else if("0".equals(customerList.getStatus())){
                  customerList.setStatus("1");
                  customerServiceQqMapper.updateByPrimaryKeySelective(customerList);
              }
                }
                code = Constants.SUCCESS;
                msg = "分配成功";
            } catch (Exception e) {
                e.printStackTrace();
                code = Constants.ERROR;
                msg = "系统繁忙";
            }
            result.setCode(code);
            result.setMsg(msg);
            return result;
        }

}
