package com.aidianmao.serviceImpl;

import com.aidianmao.entity.*;
import com.aidianmao.mapper.*;
import com.aidianmao.service.TmallService;
import com.aidianmao.utils.*;
import com.github.pagehelper.PageInfo;
import com.github.tobato.fastdfs.domain.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;
import java.util.Map;



@Transactional
@Service
public class TmallServiceImpl implements TmallService {
    @Autowired
    private TmallMapper tmallMapper;
    @Autowired
    private ProvincesCitysCountrysMapper provincesCitysCountrysMapper;
    @Autowired
    private PlatformMessageMapper platformMessageMapper;
    @Autowired
    private TmallCostRuleMapper tmallCostRuleMapper;
    @Autowired
    private FastFileStorageClient fastFileStorageClient;

    private final static Logger log = LoggerFactory.getLogger(TmallServiceImpl.class);
    @Override
    public Result update(Tmall tmall, ProvincesCitysCountrys provincesCitysCountrys,String aidianmaoAdminToken ) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String adminId = CommonUtil.getAidianmaoAdminId(aidianmaoAdminToken);
            if(StringUtils.isBlank(tmall.getId())){
                code = "-50";
                msg = "店铺主键ID不能为空";
            }else if(StringUtils.isBlank(tmall.getShopName())){
                code = "-3";
                msg = "店铺名称不能为空";
            }else if(StringUtils.isBlank(tmall.getShopProfile())){
                code = "-4";
                msg = "店铺描述不能为空";
            }else if(tmall.getShopProfile().length() < 20 || tmall.getShopProfile().length() > 200){
                code = "-5";
                msg = "店铺描述在20-200字符";
            }/*else if(StringUtils.isBlank(tmall.getShopOwner())){
                code = "-6";
                msg = "店铺掌柜名不能为空";
            }else if(tmall.getShopOwner().length() > 20){
                code = "-7";
                msg = "店铺掌柜名为1-20字符之间";
            }*/else if(tmall.getShopPrice() == null){
                code = "-6";
                msg = "请填写网店价格";
            }else if(tmall.getShopDeposit() == null){
                code = "-7";
                msg = "消费者保障金不能为空";
            }else if(!Constants.IsReturnDeposit.RETURN.getIsReturn().equals(tmall.getIsReturnDeposit())
                    && !Constants.IsReturnDeposit.UNRETURN.getIsReturn().equals(tmall.getIsReturnDeposit())){
                code = "-8";
                msg = "请正确选择消费保证金是否需要退还";
            }else if(tmall.getShopTechServiceFee() == null){
                code = "-9";
                msg = "请填写店铺技术年费";
            }else if(!Constants.IsReturnTechServiceFee.RETURN.getIsReturn().equals(tmall.getIsReturnDeposit())
                    && !Constants.IsReturnTechServiceFee.UNRETURN.getIsReturn().equals(tmall.getIsReturnDeposit())){
                code = "-10";
                msg = "请正确选择技术年费是否需要退还";
            }else if(StringUtils.isBlank(tmall.getShopType()) ||
                    !Constants.ShopType.FLAGSHIPSTORE.getType().equals(tmall.getShopType())
                            && !Constants.ShopType.MONOPOLYSTORE.getType().equals(tmall.getShopType())
                            && !Constants.ShopType.MONOSALESTORE.getType().equals(tmall.getShopType())){
                code = "-11";
                msg = "请正确选择商城类型";
            }else if(!Constants.TrademarkType.R.getType().equals(tmall.getTrademarkType())
                    && !Constants.TrademarkType.TM.getType().equals(tmall.getTrademarkType())){
                code = "-12";
                msg = "请正确选择商标类型";
            }else if(!Constants.IsCarryGoods.CARRY.getType().equals(tmall.getIsCarryGoods())
                    && !Constants.IsCarryGoods.UNCARRY.getType().equals(tmall.getIsCarryGoods())){
                code = "-13";
                msg = "请正确选择是否带货";
            }else if(tmall.getShopCollectCount() == null || tmall.getShopCollectCount()<0){
                code = "-14";
                msg = "爱店猫收藏数不能为空或小于零";
            }else if(!Constants.TaxPayerType.GENERAL.getType().equals(tmall.getTaxPayerType())
                    && !Constants.TaxPayerType.SMALLSCALE.getType().equals(tmall.getTaxPayerType())){
                code = "-15";
                msg = "请正确选择纳税人性质";
            }else if(!Constants.IsLoan.YES.getType().equals(tmall.getIsLoan())
                    && !Constants.IsLoan.NO.getType().equals(tmall.getIsLoan())){
                code = "-16";
                msg = "请正确选择是否贷款";
            }else if(StringUtils.isBlank(tmall.getIsLoan())){
                code = "-18";
                msg = "是否贷款不能为空";
            }/*else if(tmall.getCompletedTurnover() == null){
                code = "-19";
                msg = "今年营业额不能为空";
            }*/else if(!Constants.IsCompletedTurnoverIndex.YES.getType().equals(tmall.getIsCompletedTurnoverIndex())
                    && !Constants.IsCompletedTurnoverIndex.NO.getType().equals(tmall.getIsCompletedTurnoverIndex())){
                code = "-17";
                msg = "是否完成营业额指标";
            }/*else if(tmall.getCompanyRegisterMoney() == null){
                code = "-21";
                msg = "请填写公司注册资金";
            }else if(StringUtils.isBlank(tmall.getIndustryType())){
                code = "-18";
                msg = "请填行业类型";
            }else if(!Constants.IndustryType.TYPE0.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE1.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE2.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE3.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE4.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE5.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE6.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE7.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE8.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE9.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE10.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE11.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE12.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE13.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE14.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE15.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE16.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE17.getType().equals(tmall.getIndustryType())
                    ){
                code = "-19";
                msg = "请正确选择行业类型";
            }*/else if(StringUtils.isBlank(tmall.getCurrentMainCamp())){
                code = "-20";
                msg = "请填当前主营行业";
            }else if(!Constants.CurrentMainCamp.TYPE0.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE1.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE2.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE3.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE4.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE5.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE6.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE7.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE8.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE9.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE10.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE11.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE12.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE13.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE14.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE15.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE16.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE17.getType().equals(tmall.getCurrentMainCamp())){
                code = "-21";
                msg = "请正确选择当前主营行业";
            }else if(StringUtils.isBlank(tmall.getShopUrl())){
                code = "-22";
                msg = "店铺网址不能为空";
            }else if(tmall.getShopEnterTime() == null){
                code = "-23";
                msg = "天猫入驻时间不能为空";
            }else if(StringUtils.isBlank(tmall.getGeneralViolationPoint())){
                code = "-24";
                msg = "一般违规扣分不能为空";
            }else if(StringUtils.isBlank(tmall.getSeriousViolationPoint())){
                code = "-25";
                msg = "严重违规扣分不能为空";
            }else if(StringUtils.isBlank(tmall.getCounterfeitViolationPoint())){
                code = "-26";
                msg = "售假违规扣分不能为空";
            }else if(!Constants.IsTransfer.YES.getType().equals(tmall.getIsTransfer())
                    &&!Constants.IsTransfer.NO.getType().equals(tmall.getIsTransfer())){
                code = "-27";
                msg = "请正确选择是否可以过户";
            }else if(!Constants.IsDisputeOverObligation.YES.getType().equals(tmall.getIsTransfer())
                    &&!Constants.IsDisputeOverObligation.NO.getType().equals(tmall.getIsTransfer())){
                code = "-28";
                msg = "请正确选择是否有债务纠纷";
            /*}else if(StringUtils.isBlank(tmall.getShopQualification())){
                code = "-29";
                msg = "请选择店铺资质";
            }else if(StringUtils.isBlank(tmall.getCredentials())){
                code = "-30";
                msg = "请选择可以提供的证件";*/
            }else if((!PatternUtil.patternString(tmall.getProductDescriptionPoint(),"num")) || (Integer.parseInt(tmall.getProductDescriptionPoint())<0)){
                code = "-31";
                msg = "宝贝与描述相符评分不能为空或小于零";
            }else if((!PatternUtil.patternString(tmall.getServiceAttitudePoint(),"num")) || (Integer.parseInt(tmall.getServiceAttitudePoint())<0)){
                code = "-32";
                msg = "卖家服务态度评分不能为空或小于零";
            }else if((!PatternUtil.patternString(tmall.getDeliverySpeedPoint(),"num")) || (Integer.parseInt(tmall.getDeliverySpeedPoint())<0)){
                code = "-33";
                msg = "卖家发货速度评分不能为空或小于零";
            }else if(!Constants.ScoreIcon.TYPE1.getType().equals(tmall.getProductDescriptionPointIco())&&
                      !Constants.ScoreIcon.TYPE2.getType().equals(tmall.getProductDescriptionPointIco())&&
                      !Constants.ScoreIcon.TYPE3.getType().equals(tmall.getProductDescriptionPointIco())){
                code = "-34";
                msg = "请正确选择卖家服务态度评分图标";
            }else if(!Constants.ScoreIcon.TYPE1.getType().equals(tmall.getServiceAttitudePointIco())&&
                    !Constants.ScoreIcon.TYPE2.getType().equals(tmall.getServiceAttitudePointIco())&&
                    !Constants.ScoreIcon.TYPE3.getType().equals(tmall.getServiceAttitudePointIco())){
                code = "-35";
                msg = "请正确选择卖家发货速度评分图标";
            }else if(!Constants.ScoreIcon.TYPE1.getType().equals(tmall.getDeliverySpeedPointIco())&&
                    !Constants.ScoreIcon.TYPE2.getType().equals(tmall.getDeliverySpeedPointIco())&&
                    !Constants.ScoreIcon.TYPE3.getType().equals(tmall.getDeliverySpeedPointIco())){
                code = "-36";
                msg = "请正确选择宝贝与描述相符评分图标";
            }else if(StringUtils.isBlank(tmall.getContactName())){
                code = "-37";
                msg = "联系人姓名不能为空";
            }else if(StringUtils.isBlank(tmall.getContactPhone())){
                code = "-38";
                msg = "联系人电话不能为空";
            }/*else if(StringUtils.isBlank(tmall.getContactQq())){
                code = "-36";
                msg = "联系人QQ不能为空";
            }else if(StringUtils.isBlank(tmall.getUrgentPhone())){
                msg = "紧急联系人手机号不能为空";
            }*/else if(StringUtils.isBlank(provincesCitysCountrys.getProvincesId())){
                code = "-39";
                msg = "请选择省";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getCitysId())){
                code = "-40";
                msg = "请选择城市";
            /*}else if(StringUtils.isBlank(provincesCitysCountrys.getProvincesName())){
                code = "-41";
                msg = "省名称";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getCitysName())){
                code = "-42";
                msg = "市名称";*/
            }else if(StringUtils.isBlank(tmall.getShopImage())){
                code = "-43";
                msg = "天猫图片不能为空";
            }else if(!Constants.Status.ADUITPASS.getType().equals(tmall.getStatus())
                    && !Constants.Status.ADUITNOPASS.getType().equals(tmall.getStatus())
                    && !Constants.Status.ADUITING.getType().equals(tmall.getStatus())
                    && !Constants.Status.DOWN.getType().equals(tmall.getStatus())
                    ){
                code = "-43";
                msg = "审核状态不能为空且要符合条件";
            }else if(StringUtils.isBlank(tmall.getAuditingComment())){
                code = "-43";
                msg = "审核内容不能为空";
            /*}else if(!Constants.IsTransferTrademark.YES.getType().equals(tmall.getIsTrademarkTransfer())
                    && !Constants.IsTransferTrademark.NO.getType().equals(tmall.getIsTrademarkTransfer())){
                code = "-44";
                msg = "商标是否转让";*/
            } else if(!PatternUtil.patternString(tmall.getContactPhone(),"mobile")){
                code= "-45";
                msg = "手机格式不正确";
            } /*else if(!PatternUtil.patternString(tmall.getUrgentPhone(),"mobile")){
                code = "-46";
                msg = "紧急联系人手机格式不正确";
            }*/
            else{
                String tmallId = tmall.getId();
                Tmall tmall_db = tmallMapper.selectByPrimaryKey(tmallId);
                if(tmall_db == null){
                    code = "-47";
                    msg = "审核的店铺不存在";
                }else if(Constants.Status.HASPAY.getType().equals(tmall_db.getStatus())
                        || Constants.Status.PAYMENT.getType().equals(tmall_db.getStatus())
                        || Constants.Status.TRANSFERING.getType().equals(tmall_db.getStatus())
                        || Constants.Status.HASSALE.getType().equals(tmall_db.getStatus())
                        ) {
                    code = "-43";
                    msg = "该店铺交易中或交易完成，无法修改信息";
                }else{
                    String companyAddressId = tmall_db.getCompanyAddressId();
                    //System.out.println(companyAddressId);
                    if(StringUtils.isBlank(companyAddressId)){
                        code = "-48";
                        msg = "前台校验有问题，地址ID不能为空";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    ProvincesCitysCountrys provincesCitysCountrysOld = provincesCitysCountrysMapper.selectByPrimaryKey(companyAddressId);
                    if(provincesCitysCountrysOld==null){
                        code = "-49";
                        msg = "地址主键ID不存在或者已经被修改";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                    provincesCitysCountrysOld.setProvincesId(provincesCitysCountrys.getProvincesId());
                    provincesCitysCountrysOld.setCitysId(provincesCitysCountrys.getCitysId());
                    provincesCitysCountrysMapper.updateByPrimaryKeySelective(provincesCitysCountrysOld);
                    //tmall.setId(tmallId);
                    tmall.setApproveTime(new Date());
                    tmall.setAdminId(adminId);
                    tmallMapper.updateByPrimaryKeySelective(tmall);
                    code = Constants.SUCCESS;
                    msg = "修改成功   ";
                    if ("1".equals(tmall.getStatus())){
                        String title = "挂售店铺已成功上架";
                        String content = "尊敬的爱店猫用户：\n" + "您的店铺已经通过审核，成功上架挂售。如有疑问，请咨询在线客服或拨打咨询热线4008359100。";
                        platformMessageMapper.insertSelective(new PlatformMessage(CommonUtil.getUUID(),new Date(),"0",content,tmall_db.getMemberId(),title,"0"));
                        code = Constants.SUCCESS;
                        msg += "审核通过，挂售店铺已成功上架";
                    }else if ("2".equals(tmall.getStatus())){
                        String title = "挂售店铺上架失败";
                        String content = "尊敬的爱店猫用户：\n" + "您的店铺已经过审核，不符合要求，无法上架。如有疑问，请咨询在线客服或拨打咨询热线4008359100。";
                        platformMessageMapper.insertSelective(new PlatformMessage(CommonUtil.getUUID(),new Date(),"0",content,tmall_db.getMemberId(),title,"0"));
                        code = Constants.SUCCESS;
                        msg += "审核失败，挂售店铺上架失败";
                    }else if("3".equals(tmall.getStatus())){
                        String title = "店铺已下架";
                        String content = "尊敬的爱店猫用户：\n" + "您的店铺已下架。如有疑问，请咨询在线客服或拨打咨询热线4008359100。";
                        platformMessageMapper.insertSelective(new PlatformMessage(CommonUtil.getUUID(),new Date(),"0",content,tmall_db.getMemberId(),title,"0"));
                        code = Constants.SUCCESS;
                        msg += "店铺已下架";
                    }
                }
            }
        } catch (Exception e) {
           log.error("天猫店铺"+e);
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result updateRules(String id,String num) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(id)){
                code = "-3";
                msg = "主键id不能为空";
            }else if(StringUtils.isBlank(num)){
                code = "-4";
                msg = "天猫店铺费率规则序号不能为空";
            }else {
                Tmall tmall = tmallMapper.selectByPrimaryKey(id);
                if(tmall == null){
                    code = Constants.ERROR;
                    msg = "天猫店铺主键id不存在或已被修改";
                    result.setCode(code);
                    result.setMsg(msg);
                    return result;
                }
                TmallCostRule tmallCostRule = tmallCostRuleMapper.selectByNum(num);
                if(tmallCostRule == null){
                    code = Constants.ERROR;
                    msg = "天猫店铺费率规则不存在或已被修改";
                    result.setCode(code);
                    result.setMsg(msg);
                    return result;
                }
                tmall.setTmallCostRuleId(tmallCostRule.getId());
                tmallMapper.updateByPrimaryKeySelective(tmall);
                code = Constants.SUCCESS;
                msg = "成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result detail(String tmallId) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(tmallId)){
                code = "-3";
                msg = "主键id不能为空";
            }else {
                Tmall tmall = tmallMapper.selectByPrimaryKey(tmallId);
                if(tmall != null){
                   TmallDetail map = tmallMapper.selectById(tmallId);
                    result.setData(map);
                    code = Constants.SUCCESS;
                    msg = "查询成功";
                }else {
                    code = Constants.ERROR;
                    msg = "主键id不存在或已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result pageList(Integer pageNum, Integer pageSize,Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum,pageSize);
            List<Map<String,Object>> list = tmallMapper.selectAllTmall(params);
            PageInfo<Map<String,Object>> page = new PageInfo<>(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public  Result addTmallPicture(MultipartFile file){
        Result result = new Result();
        String msg = "初始化";
        String code = Constants.FAIL;
        try {
            String fileName = file.getOriginalFilename();
            long size = file.getSize();
            if (size>2000000L){
                code="-4";
                msg="图片大小不能超过2m";
            }else {
                String prefix = fileName.substring(fileName.lastIndexOf(".") + 1);
                if (CommonUtil.isImage(prefix)) {
                    StorePath storePath = fastFileStorageClient.uploadFile(file.getInputStream(), file.getSize(), FilenameUtils.getExtension(file.getOriginalFilename()), null);
                    String fileUrl = storePath.getFullPath();
                    result.setData(Constants.FASTDFS_IP+"/"+fileUrl);
                    code = "0";
                    msg = "成功";
                } else {
                    code = "-3";
                    msg = "只能上传图片";
                }
            }
        } catch (Exception e) {
            code = "-2";
            msg = "上传出错";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }


    @Override
    public Result detailTmall(String tmallId) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(tmallId)){
                code = "-3";
                msg = "主键id不能为空";
            }else {
                Tmall tmall = tmallMapper.selectByPrimaryKey(tmallId);
                if(tmall != null){
                   Map<String,Object> map = tmallMapper.selectByTmallId(tmallId);
                    result.setData(map);
                    code = Constants.SUCCESS;
                    msg = "查询成功";
                }else {
                    code = Constants.ERROR;
                    msg = "主键id不存在或已被修改";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

}
