package com.aidianmao.repository;

import com.aidianmao.esEntity.EsTmallOrder;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TmallOrderRepository extends ElasticsearchRepository<EsTmallOrder,String> {


}
