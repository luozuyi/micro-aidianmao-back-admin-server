package com.aidianmao.repository;

import com.aidianmao.esEntity.EsMember;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberRepository extends ElasticsearchRepository<EsMember, String> {

}
