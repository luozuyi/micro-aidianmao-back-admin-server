package com.aidianmao.service;

import com.aidianmao.entity.Member;
import com.aidianmao.entity.OnlineQuestionAnswer;
import com.aidianmao.utils.Result;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

public interface OnlineQuestionAnswerService {

    /**
     * 提问管理列表
     * @param pageNum     当前页
     * @param pageSize    一页显示多少条
     * @param params      条件参数
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize,Map<String, Object> params);

    /**
     * 添加提问信息
     * @param onlineQuestionAnswer  提问对象
     * @return
     */
    Result insert(OnlineQuestionAnswer onlineQuestionAnswer,String aidianmaoAdminToken);

    /**
     * 修改信息
     * @param onlineQuestionAnswer  提问对象
     * @return
     */
    Result update(OnlineQuestionAnswer onlineQuestionAnswer,String aidianmaoAdminToken);

    /**
     * 删除
     * @param id 主键id
     * @return
     */
    Result deleteQuestion(String id,String aidianmaoAdminToken);


    /**
     * 提问详情
     * @param id  主键id
     * @return
     */
    Result detail(String id);



}
