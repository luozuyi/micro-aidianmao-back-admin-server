package com.aidianmao.service;

import com.aidianmao.utils.Result;

public interface MemberRemarkService {
    /**
     * 查询销售对会员的备注信息
     * @return
     */
    Result pageList(String memberId);
    /**
     * 销售添加会员备注信息
     * @param content 备注内容
     * @param memberId 会员ID
     * @param aidianmaoAdminToken 凭据
     * @return
     */
    Result insert(String content,String memberId,String aidianmaoAdminToken);

    /**
     * 备注信息详情
     * @param id
     * @return
     */
    Result detail(String id);
}
