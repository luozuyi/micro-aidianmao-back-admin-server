package com.aidianmao.service;

import com.aidianmao.entity.AssessTmall;
import com.aidianmao.utils.Result;

import java.util.Map;

public interface AssessTmallService {

    /**
     * 分页查询店铺列表
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    Result pageAssessTmallList(Integer pageNum, Integer pageSize, Map<String, Object> params);

    /**
     * 填写估价结果
     * @param assessTmall  估价实体
     * @return
     */
    Result updateAssessResult(AssessTmall assessTmall,String  aidianmaoAdminToken);

    /**
     * 估价详情
     * @param id  主键id
     * @return
     */
    Result detail(String id);
}
