package com.aidianmao.service;


import com.aidianmao.esEntity.EsTmallOrder;
import com.aidianmao.utils.Result;

import java.util.Map;

public interface TmallOrderService {
    /**
     * 分页条件查询订单
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param
     * @return
     */
    Result pageList(String pageNum, String pageSize, EsTmallOrder esTmallOrder);


    /**
     * 订单详情
     * @param id 订单ID
     * @return
     */
    Result detailById(String id);

    /**
     * 订单为待付款，取消订单状态详情
     * @param id
     * @return
     */
    Result statusDetailById(String id);

    /**
     * 支付定金订单详情
     * @param id
     * @return
     */
    Result payDetailById(String id);

    /**
     * 支付全款，交接中，交易成功的订单详情
     * @param id
     * @return
     */
    Result paymentDetailById(String id);

    /**
     * 待付款订单
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    Result pageListByStatus0(Integer pageNum, Integer pageSize, Map<String,Object> params);

    /**
     * 已经付定金
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    Result pageListByStatus1(Integer pageNum, Integer pageSize, Map<String,Object> params);

    /**
     * 交易成功
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    Result pageListByStatus3(Integer pageNum, Integer pageSize, Map<String,Object> params);

    /**
     * 已取消
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    Result pageListByStatus4(Integer pageNum, Integer pageSize, Map<String,Object> params);

    /**
     * 异常订单（无资金调整）
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    Result pageListByStatus6(Integer pageNum, Integer pageSize, Map<String,Object> params);


    /**
     * 异常订单有资金调整
     * @param pageNum      当前多少页
     * @param pageSize     一页多少条
     * @param params       凭据
     * @return
     */
   Result selectFundAdjustAll(Integer pageNum, Integer pageSize, Map<String,Object> params);

    /**
     * 交接状态
     * @param pageNum    当前多少页
     * @param pageSize   一页显示多少条
     * @param params      凭据
     * @return
     */
   Result transferStatus(Integer pageNum, Integer pageSize, Map<String,Object> params);

    /**
     * 交接中
     * @param id 主键id
     * @return
     */
    Result transfer(String id,String aidianmaoAdminToken);

    /**
     * 取消交接
     * @param id  主键id
     * @return
     */
    Result cancelTransfer(String id);



    /**
     * 终止交易
     * @param id                     主键id
     * @param aidianmaoAdminToken   凭据
     * @param responsibility        责任
     * @return
     */
    Result terminationOrder(String id,String aidianmaoAdminToken,String responsibility);

    /**
     * 放款
     * @param id       主键id
     * @return
     */
    Result loanOrder(String id);

    /**
     * 卖家待确认
     * @param id      主键id
     * @return
     */
    Result saleSule(String id);


}
