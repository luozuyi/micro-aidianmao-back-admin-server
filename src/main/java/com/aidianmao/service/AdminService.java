package com.aidianmao.service;

import com.aidianmao.utils.Result;

import java.util.Map;

public interface AdminService {
    /**
     * 添加管理员
     * @param adminName 管理员名称
     * @param realName 管理员真实名称
     * @param password 管理员密码
     * @param roleId 管理员角色id
     * @param isDelFlag 是否禁用0：否，1：是
     * @param surePassword 确认密码
     * @param maximunNum 最大客户数
     * @return
     */
    Result addAdmin(String adminName,String realName,String qq, String password, String roleId,String isDelFlag, String surePassword,Integer maximunNum);

    /**
     * 分页查询管理员列表
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize,Map<String, Object> params);

    /**
     * 详情
     * @param adminId 主键id
     * @return
     */
    Result findById(String adminId);

    /**
     * 修改管理员信息
     * @param adminId 管理员id
     * @param password 管理员密码
     * @param roleId 角色id
     * @param isDelFlag 是否禁用
     * @param surePassword 确认密码
     * @return
     */
    Result update(String adminId, String password, String roleId, String isDelFlag, String surePassword);

    /**
     * 分页查询我的组员
     * @param aidianmaoAdminToken 凭据
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    Result pageListGroup(String aidianmaoAdminToken,Integer pageNum,Integer pageSize);

    /**
     * 分组信息修改
     * @param adminId 组长主键ID
     * @param id 组员主键ID
     * @return
     */
    Result updateGroup(String adminId, String id);

    /**
     * 启用
     * @param id  主键id
     * @return
     */
    Result updateDel(String id);

    /**
     * 禁用
     * @param id
     * @return
     */
    Result delFlag(String id);

    /**
     * 删除管理员
     * @param id  主键id
     * @return
     */
    Result delete(String id);
}
