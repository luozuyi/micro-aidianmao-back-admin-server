package com.aidianmao.service;

import com.aidianmao.entity.Advertise;
import com.aidianmao.utils.Result;


import java.util.Map;

public interface AdvertiseService {
    /**
     * 广告位列表
     * @param pageNum     当前页
     * @param pageSize    一页显示多少条
     * @param params      凭据
     * @return
     */
    Result pageListAdvertise(Integer pageNum, Integer pageSize, Map<String,Object> params);

    /**
     * 禁用
     * @param id                          主键id
     * @param aidianmaoAdminToken       凭据
     * @return
     */
    Result updateDel(String id,String aidianmaoAdminToken);

    /**
     * 启用
     * @param id                          主键id
     * @param aidianmaoAdminToken       凭据
     * @return
     */
    Result updateDelFlag(String id,String aidianmaoAdminToken);

    /**
     * 修改广告内容
     * @param advertise         广告实体
     * @param aidianmaoAdminToken  凭据
     * @return
     */
    Result updateAdvertise(Advertise advertise, String aidianmaoAdminToken);

    /**
     * 删除广告
     * @param id   主键id
     * @return
     */
    Result deleteAdvertise(String id);

    /**
     * 添加广告
     * @param advertise     广告实体
     * @param aidianmaoAdminToken  凭据
     * @return
     */
    Result addAdvertise(Advertise advertise,String aidianmaoAdminToken);

    /**
     * 广告详情
     * @param id  主键id
     * @return
     */
    Result detail(String id);

}
