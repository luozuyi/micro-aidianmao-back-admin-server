package com.aidianmao.service;

import com.aidianmao.utils.Result;

import java.util.Map;

/**
 * 公共查询接口
 * @author Catch22
 * @date 2018年6月6日
 */
public interface BaseService {

    /**
     * 条件查询
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    Result listPage(Integer pageNum, Integer pageSize, Map<String, Object> params);

    /**
     * id查询
     * @param id 主键id
     * @return
     */
    Result selectByPrimaryKey(String id);

}
