package com.aidianmao.service;


import com.aidianmao.utils.Result;

import java.util.Map;

public interface TmallAbnormalOrderRecordService {
    /**
     * 分页条件查询异常订单记录
     * @param pageNum     当前页
     * @param pageSize    一页显示多少条
     * @param params      参数条件
     * @return
     */
    Result listPage(Integer pageNum, Integer pageSize, Map<String, Object> params);

    /**
     * 异常订单详情
     * @param id   主键id
     * @return
     */
    Result detail(String id);

}
