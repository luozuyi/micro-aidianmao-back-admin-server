package com.aidianmao.service;

import com.aidianmao.entity.Group;
import com.aidianmao.utils.Result;

public interface GroupService {
    /**
     * 添加小组
     * @param group 小组对象
     * @return
     */
    Result addGroup(Group group);
    /**
     * 分页查询小组
     * @param pageSize   当前页
     * @param pageNum    一页显示多少条
     * @return
     */
    Result pageList(Integer pageSize, Integer pageNum);
    /**
     * 分页查询小组组长
     * @return
     */
    Result pageListLeader();
}
