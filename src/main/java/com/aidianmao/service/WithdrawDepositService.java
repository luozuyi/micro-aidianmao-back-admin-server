package com.aidianmao.service;

import com.aidianmao.entity.WithdrawDeposit;
import com.aidianmao.utils.Result;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

public interface WithdrawDepositService {
    /**
     * 分页条件查询提现信息
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    public Result pageList(Integer pageNum, Integer pageSize, Map<String,Object> params);
    /**
     * 提现信息详情
     * @param id 主键id
     * @return
     */
    Result detail(String id);
    /**
     * 提现信息修改
     * @param record 参数WithdrawDeposit实体
     * @param checkResult 审核结果
     * @param aidianmaoAdminToken 凭据
     * @return
     */
    Result update(WithdrawDeposit record,Integer checkResult,Integer checkType,String aidianmaoAdminToken);
    /**
     * 提现信息免手续费
     * @param id 主键id
     * @return
     */
    Result updateServiceFee(String id, String aidianmaoAdminToken);

    /**
     * 拒绝申请
     * @param id    主键id
     * @param refuseReason    拒绝理由
     * @param aidianmaoAdminToken  凭据
     * @return
     */
    Result refuseApply(String id,String refuseReason,String aidianmaoAdminToken);

    /**
     * 处理中
     * @param id   主键id
     * @param checkResult     申请结果
     * @param aidianmaoAdminToken  凭据
     * @return
     */
    Result acceptApply(String id,String checkResult,String aidianmaoAdminToken);

    /**
     * 接受申请
     * @param id
     * @param aidianmaoAdminToken
     * @return
     */
    Result processing(String id,String aidianmaoAdminToken);
}
