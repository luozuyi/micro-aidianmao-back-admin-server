package com.aidianmao.service;

import com.aidianmao.entity.SysRes;
import com.aidianmao.utils.Result;

public interface SysResService {
    /**
     * 添加权限
     * @param sysRes 权限对象
     * @return
     */
    Result add(SysRes sysRes);

    /**
     * 主键查询详情
     * @param sysResId 主键id
     * @return
     */
    Result findById(String sysResId);

    /**
     * 修改
     * @param sysRes 权限对象
     * @return
     */
    Result update(SysRes sysRes);

    /**
     * 查询列表
     * @return
     */
    Result getList();

    /**
     * 主键假删除权限
     * @param id 主键id
     * @return
     */
    Result updateDelFlag(String id);
}
