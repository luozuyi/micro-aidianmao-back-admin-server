package com.aidianmao.service;

import com.aidianmao.entity.ProvincesCitysCountrys;
import com.aidianmao.entity.Tmall;
import com.aidianmao.utils.Result;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface TmallService {
    /**
     * 审核店铺
     * @param tmall 天猫对象
     * @param provincesCitysCountrys 省市区
     * @param aidianmaoAdminToken 凭据
     * @return
     */
    Result update(Tmall tmall,ProvincesCitysCountrys provincesCitysCountrys,String aidianmaoAdminToken);

    /**
     * 修改天猫店铺规则
     * @param num  规则序号
     * @param id 天猫店铺主键ID
     * @return
     */
     Result updateRules(String id,String num);

    /**
     * 主键查询天猫详情
     * @param tmallId 主键id
     * @return
     */
    Result detail(String tmallId);

    /**
     * 分页查询店铺列表
     * @param pageNum  当前页
     * @param pageSize  一页显示多少条
     * @param params     参数map
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize,Map<String, Object> params);

    /**
     * 天猫图片
     * @param file  图片
     * @return
     */
    Result addTmallPicture(MultipartFile file);

    /**
     * 主键查询天猫详情
     * @param tmallId 主键id
     * @return
     */
    Result detailTmall(String tmallId);

}
