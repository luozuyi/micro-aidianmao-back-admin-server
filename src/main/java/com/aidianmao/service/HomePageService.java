package com.aidianmao.service;

import com.aidianmao.utils.Result;

import java.util.Map;

public interface HomePageService {
    /**
     * 运营数据
     * @return
     */
    Result operationalData();

    /**
     *财务数据
     * @param params
     * @return
     */
    Result finance(Map<String,Object> params);
}
