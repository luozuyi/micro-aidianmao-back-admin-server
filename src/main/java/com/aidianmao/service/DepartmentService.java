package com.aidianmao.service;

import com.aidianmao.entity.Department;
import com.aidianmao.utils.Result;

public interface DepartmentService {
    /**
     * 新增部门
     * @param department 参数实体
     * @return
     */
    Result addDepartment(Department department);

    /**
     * 分页查询部门
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    Result pageList(Integer pageNum,Integer pageSize);

    /**
     * 查询部长名字
     * @return
     */
    Result listName();
}
