package com.aidianmao.service;

import com.aidianmao.entity.SalespersonTarget;
import com.aidianmao.utils.Result;

public interface SalespersonTargetService {

    /**
     * 添加个人目标
     * @param id
     * @param target
     * @return
     */
    Result insert(String id, SalespersonTarget target);

    /**
     * 个人目标列表
     * @param pageNum
     * @param pageSize
     * @param saleapersonId
     * @return
     */
    Result pageList(Integer pageNum,Integer pageSize,String saleapersonId);
}
