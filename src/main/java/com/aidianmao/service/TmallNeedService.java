package com.aidianmao.service;

import com.aidianmao.entity.Provinces;
import com.aidianmao.entity.ProvincesCitysCountrys;
import com.aidianmao.entity.Tmall;
import com.aidianmao.entity.TmallNeed;
import com.aidianmao.utils.Result;

import java.util.Map;

public interface TmallNeedService {
    /**
     * 动态分页条件查询需求
     * @param pageNum   当前页
     * @param pageSize  一页显示多少条
     * @param params    条件参数
     * @return
     */
    Result listPageBySelection(Integer pageNum, Integer pageSize, Map<String, Object> params);



    /**
     * 修改求购信息
     * @param tmallNeed 参数实体
     * @return
     */
    Result updateTmallNeed(TmallNeed tmallNeed, ProvincesCitysCountrys provincesCitysCountrys, String aidianmaoAdminToken);

    /**
     * 求购的详细信息
     * @param id 主键ID
     * @return
     */
    Result selectById(String id);

    /**
     * 销售中的求购
     * @param pageNum
     * @param pageSize
     * @param params
     * @return
     */
    Result saleNeed(Integer pageNum, Integer pageSize, Map<String, Object> params);

    /**
     * 销售中的我去联系
     * @param id
     * @param aidianmaoAdminToken
     * @return
     */
    Result saleRelation(String id,String aidianmaoAdminToken);

    /**
     * 销售中的找店信息收集详情
     * @param id
     * @return
     */
    Result saleDetail(String id);

}
