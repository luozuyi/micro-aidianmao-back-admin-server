package com.aidianmao.service;

import com.aidianmao.utils.Result;

import java.util.Map;

public interface AdminMemberRecordService {
    /**
     * 分页条件销售操作会员记录
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize, Map<String,Object> params);

    /**
     * 销售操作会员记录新增
     * @param memberId 会员ID
     * @param operationType 操作类型
     * @param aidianmaoAdminToken 凭据
     * @return
     */
    Result insert(String memberId,Integer operationType,String aidianmaoAdminToken);
}
