package com.aidianmao.service;

import com.aidianmao.entity.AccountKeepingAgency;
import com.aidianmao.utils.Result;

import java.util.Map;

public interface AccountKeepingAgencyService {
    /**
     * 分页查询所有记账
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params);
    /**
     * 记账详情
     * @param id 主键
     * @return
     */
    Result detail(String id);
    /**
     *  记账修改
     *  @param record 记账对象
     *  @return
     */
    Result update(AccountKeepingAgency record);
    /**
     *  记账新增
     *  @param record 记账对象
     *  @return
     */
    Result insert(AccountKeepingAgency record);

    /**
     * 交接
     * @param list  交接数组
     * @return
     */
    Result update(String[] list);

    /**
     * 取消交接
     * @param list  取消交接数组
     * @return
     */
    Result cancelTakeOver(String[] list);
}
