package com.aidianmao.service;

import com.aidianmao.entity.Member;
import com.aidianmao.entity.MemberFundAdjust;
import com.aidianmao.utils.Result;

import java.math.BigDecimal;
import java.util.Map;

public interface MemberService {
    /**
     *分页条件查询会员
     * @param
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize,Map<String, Object> params);

    /**
     * 资金调整
     * @param member           会员对象
     * @param adjustMoney     调整后的资金
     * @param note             调整说明
     *@param aidianmaoAdminToken  凭据
     * @return
     */
    Result fundAdjust(Member member,BigDecimal adjustMoney,String note,String aidianmaoAdminToken);

    /**
     * 修改会员信息
     * @param member  会员对象
     */
    Result update(Member member);

    /**
     * 查看会员收支明细
     * @param
     * @return
     */
    Result detailIncomeExpenses(String id,Integer pageNum, Integer pageSize,Map<String, Object> params);


    /**
     * 会员详情
     * @param id  主键id
     * @return
     */
    Result detailMember(String id);

    /**
     *分页条件查询公共客户池
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数
     * @return
     */
    Result pageListCustomer(Integer pageNum, Integer pageSize,Map<String, Object> params);

    Result listCustomer(Member member,Integer pageNum, Integer pageSize);

    /**
     * 客户详情
     * @param memberId  客户id
     * @return
     */
    Result detailCustomer(String memberId);

    /**
     * 修改客户信息
     * @param record 实体
     * @param content 备注内容
     * @param aidianmaoAdminToken  凭据
     * @return
     */
    Result updateCustomer(Member record,String content,String aidianmaoAdminToken);
    /**
     *分页条件查询最近一周登录客户
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数
     * @return
     */
    Result pageListCustomerWeek(Integer pageNum, Integer pageSize,Map<String, Object> params);
}
