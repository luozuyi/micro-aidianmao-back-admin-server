package com.aidianmao.service;

import com.aidianmao.utils.Result;

import java.util.Map;

public interface CustomerQqRecordService {
    /**
     * 客服记录分页条件查询
     * @param pageNum    当前页
     * @param pageSize   一页显示多少条
     * @param params      条件参数
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params);
}
