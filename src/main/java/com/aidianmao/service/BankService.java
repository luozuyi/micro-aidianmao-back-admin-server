package com.aidianmao.service;

import com.aidianmao.entity.Bank;
import com.aidianmao.utils.Result;
import org.springframework.web.multipart.MultipartFile;

public interface BankService {
    /**
     * 分页查询银行信息
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize);
    /**
     * 银行信息详情
     * @param id 主键id
     * @return
     */
    Result detail(String id);
    /**
     * 银行信息修改
     * @param record 店铺费用规则实体
     * @return
     */
    Result update(Bank record);
    /**
     * 银行信息新增
     * @param record 店铺费用规则实体
     * @return
     */
    Result insert(Bank record);
    /**
     * 银行标志上传
     * @param file 文件
     * @return
     */
    public Result addBankLogo(MultipartFile file);
}
