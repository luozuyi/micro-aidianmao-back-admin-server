package com.aidianmao.service;

import com.aidianmao.utils.Result;

import java.util.Map;

public interface IncomeExpensesDetailService {
    /**
     * 分页条件查询会员收入明细信息
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize, Map<String,Object> params);
}
