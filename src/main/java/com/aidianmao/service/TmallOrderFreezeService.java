package com.aidianmao.service;

import com.aidianmao.utils.Result;

import java.util.Map;

public interface TmallOrderFreezeService {
    /**
     * 分页条件查询订单冻结资金记录
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 条件查询的参数
     * @return
     */
    public Result pageList(Integer pageNum, Integer pageSize, Map<String,Object> params);
}
