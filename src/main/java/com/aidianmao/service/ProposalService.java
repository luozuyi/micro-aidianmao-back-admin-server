package com.aidianmao.service;

import com.aidianmao.entity.Proposal;
import com.aidianmao.utils.Result;


import java.util.Map;

public interface ProposalService {
    /**
     * 建议分页条件查询
     * @param pageNum  当前页
     * @param pageSize  一页显示多少条
     * @param params     参数map
     * @return
     */
    Result pagaList(Integer pageNum,Integer pageSize,Map<String,Object> params);

    /**
     * 建议详情
     * @param id 主键id
     * @return
     */
    Result detail(String id);

    /**
     * 修改信息
     * @param proposal 参数实体
     * @return
     */
    Result update(Proposal proposal,String aidianmaoAdminToken);

    /**
     * 删除
     * @param id 主键id
     * @return
     */
    Result deleteProposal(String id,String aidianmaoAdminToken);
}
