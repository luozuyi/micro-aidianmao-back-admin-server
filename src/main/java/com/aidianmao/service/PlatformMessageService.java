package com.aidianmao.service;

import com.aidianmao.utils.Result;

import java.util.Map;

public interface PlatformMessageService {
    /**
     *  站内信分页查询
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize,Map<String, Object> params);
}
