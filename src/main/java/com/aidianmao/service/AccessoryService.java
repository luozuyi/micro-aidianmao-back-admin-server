package com.aidianmao.service;

import com.aidianmao.utils.Result;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

public interface AccessoryService {
    /**
     * 添加文章封面
     * @param file 文件
     * @return
     */
    Result addArticleCover(MultipartFile file);
    
    /**
     * 自营商品详情中上传图片
     * @param file 文件
     * @return
     */
    Result addSelfProductDetailPhoto(MultipartFile file);


    /**
     * 下载文件
     * @param accessoryId 文件id
     * @param response 响应
     * @return
     */
    Result downloads(String accessoryId, HttpServletResponse response);
}
