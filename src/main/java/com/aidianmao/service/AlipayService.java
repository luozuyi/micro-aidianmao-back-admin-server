package com.aidianmao.service;

import com.aidianmao.entity.Alipay;
import com.aidianmao.utils.Result;

import java.util.Map;

public interface AlipayService {
    /**
     * 分页条件查询支付宝充值信息
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize, Map<String,Object> params);
    /**
     * 支付宝充值信息详情
     * @param id 主键id
     * @return
     */
    Result detail(String id);
    /**
     * 支付宝充值信息修改
     * @param record 参数Alipay实体
     * @param checkResult 审核结果
     * @param aidianmaoAdminToken 凭据
     * @return
     */
    Result update(Alipay record,Integer checkResult,String aidianmaoAdminToken);
}
