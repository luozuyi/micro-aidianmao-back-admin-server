package com.aidianmao.service;

import com.aidianmao.entity.Remittance;
import com.aidianmao.utils.Result;

import java.util.Map;

public interface RemittanceService {
    /**
     * 分页条件查询线下汇款
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize, Map<String,Object> params);
    /**
     * 线下汇款详情
     * @param id 主键id
     * @return
     */
    Result detail(String id);
    /**
     * 线下汇款修改
     * @param record 参数Remittance实体
     * @param checkResult 审核结果
     * @param aidianmaoAdminToken 凭据
     * @return
     */
    Result update(Remittance record,Integer checkResult,String aidianmaoAdminToken);
}
