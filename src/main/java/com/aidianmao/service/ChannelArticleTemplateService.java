package com.aidianmao.service;

import com.aidianmao.utils.Result;

import java.util.Map;

public interface ChannelArticleTemplateService {

    /**
     * 添加模板
     * @param tplUrl 模板路径
     * @param type 模板类型 0：文章模板 1：栏目模板
     * @return
     */
    Result addChannelArticleTemplate(String tplUrl, String type);

    /**
     * 动态分页查询
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数
     * @return
     */
    Result listPageBySelect(Integer pageNum, Integer pageSize, Map<String, Object> params);

    /**
     * 删除模板
     * @param id 主键id
     * @return
     */
    Result delChannelArticleTemplate(String id);

    /**
     * 获取栏目模板列表
     * @return
     */
    Result getChannelTemplateList();

    /**
     * 获取文章模板列表
     * @return
     */
    Result getArticleTemplateList();
}
