package com.aidianmao.service;

import com.aidianmao.entity.FriendshipLink;
import com.aidianmao.utils.Result;

public interface FriendshipLinkService {

    /**
     * 友情链接分页列表
     * @param pageNum    当前多少页
     * @param pageSize   一页显示多少条
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize);

    /**
     * 添加友情链接
     * @param friendshipLink  友情链接实体
     * @return
     */
    Result insertFriendshipLink(FriendshipLink friendshipLink);

    /**
     * 修改友情链接
     * @param friendshipLink  友情链接实体
     * @return
     */
    Result update(FriendshipLink friendshipLink);

    /**
     * 禁用
     * @param id  主键id
     * @return
     */
    Result updateDel(String id);

    /**
     * 启用
     * @param id  主键id
     * @return
     */
    Result updateDelFlag(String id);

    /**
     * 删除（真删除）
     * @param id   主键id
     * @return
     */
    Result delete(String id);


}
