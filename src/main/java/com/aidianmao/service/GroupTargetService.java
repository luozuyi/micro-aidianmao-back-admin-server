package com.aidianmao.service;

import com.aidianmao.entity.GroupTarget;
import com.aidianmao.utils.Result;

import java.util.Map;

public interface GroupTargetService {
    /**
     * 分页条件查询小组月目标
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    public Result pageList(Integer pageNum, Integer pageSize, Map<String,Object> params);
    /**
     * 小组月目标详情
     * @param id 主键id
     * @return
     */
    Result detail(String id);
    /**
     * 小组月目标修改
     * @param record 参数实体
     * @return
     */
    Result update(GroupTarget record);
    /**
     * 小组月目标删除
     * @param id 主键id
     * @return
     */
    Result delete(String id);
    /**
     * 小组月目标新增
     * @param record 参数实体
     * @return
     */
    Result insert(GroupTarget record,String groupName);
}
