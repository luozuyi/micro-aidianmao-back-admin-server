package com.aidianmao.service;

import com.aidianmao.entity.TmallCostRule;
import com.aidianmao.utils.Result;

public interface TmallCostRuleService {
    /**
     * 分页查询店铺费用规则
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize);
    /**
     * 店铺费用规则详情
     * @param id 主键id
     * @return
     */
    Result detail(String id);
    /**
     * 店铺费用规则删除
     * @param id 主键id
     * @return
     */
    Result delete(String id);
    /**
     * 店铺费用规则修改
     * @param record 店铺费用规则实体
     * @return
     */
    Result update(TmallCostRule record);
    /**
     * 店铺费用规则新增
     * @param record 店铺费用规则实体
     * @return
     */
    Result insert(TmallCostRule record);
}
