package com.aidianmao.service;

import com.aidianmao.utils.Result;

public interface CustomerServiceQqService {
    /**
     * 客服分配列表
     * @param type 服务qq组类型(1.天猫特服 2.代记账特服 3.出售网店 4.代入驻咨询 5.售后及投诉)
     * @return
     */
    Result selectNameByType(String type);

    /**
     * 分配客服人员
     * @param list     客服列表
     * @return
     */
    Result update(String list[]);
}
