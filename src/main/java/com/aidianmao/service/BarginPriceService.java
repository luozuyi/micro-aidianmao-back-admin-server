package com.aidianmao.service;

import com.aidianmao.utils.Result;

import java.util.Map;

public interface BarginPriceService {
    /**
     * 分页条件砍价信息
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    public Result pageList(Integer pageNum, Integer pageSize, Map<String,Object> params);
    /**
     * 砍价信息详情
     * @param id 主键id
     * @return
     */
    Result detail(String id);

    Result myRelation(String id,String remark,String aidianmaoAdminToken);
}
