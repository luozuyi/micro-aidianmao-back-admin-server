package com.aidianmao.service;

import com.aidianmao.entity.AdminMember;
import com.aidianmao.entity.Member;
import com.aidianmao.utils.Result;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

public interface AdminMemberService {
    /**
     * 分页条件查询已被添加的客户信息
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    public Result pageList(Integer pageNum, Integer pageSize, Map<String,Object> params);
    /**
     * 分页条件查询我的客户信息
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @param aidianmaoAdminToken 凭据
     * @return
     */
    public Result pageListByAdmin(Integer pageNum, Integer pageSize, Map<String,Object> params,String aidianmaoAdminToken);
    /**
     * 我的客户信息删除
     * @param id 主键id
     * @param content 备注内容
     * @param memberId 会员ID
     * @param operationType 操作类型
     * @param aidianmaoAdminToken 凭据
     * @return
     */
    Result delete(String id,String content,String memberId,Integer operationType,String aidianmaoAdminToken);
    /**
     * 会员销售关联信息新增
     * @param record 参数实体
     * @param memberId 会员ID
     * @param operationType 操作类型
     * @param aidianmaoAdminToken  凭据
     * @return
     */
    Result insert(AdminMember record,String memberId,Integer operationType,String aidianmaoAdminToken);

    /**
     * 公共客户池
     * @param pageNum    当前页
     * @param pageSize   一页显示多少条
     * @param params     凭据
     * @return
     */
    Result selectMember(Integer pageNum, Integer pageSize, Map<String,Object> params);

    /**
     * 加入我的客户组
     * @param id                会员id
     * @param aidianmaoAdminToken  凭据
     * @return
     */
    Result addMember(String id, String aidianmaoAdminToken);

    /**
     * 添加会员
     * @param phone   会员手机号
     * @return
     */
    Result insertMember(String phone);

    /**
     * 所有客户信息
     * @param pageNum     当前页
     * @param pageSize    一页显示多少条
     * @param params      参数
     * @return
     */
    Result selectAdminMember(Integer pageNum, Integer pageSize, Map<String,Object> params);

    /**
     * 移到公共池
     * @param ids
     * @return
     */
    Result deleteMany(String ids);

    /**
     * 我的客户分页条件查询
     * @param pageNum    当前页
     * @param pageSize   一页显示多少条
     * @param params     参数
     * @return
     */
    Result selectSellAdminAll(Integer pageNum, Integer pageSize, Map<String,Object> params,String aidianmaoAdminToken);

    /**
     * 我的客户详情
     * @param id  主键id
     * @return
     */
    Result selectSellById(String id);

    /**
     * 修改我的客户信息
     * @param id
     * @param identity
     * @param importanceDegree
     * @return
     */
    Result updateAdminMember(String id,String identity,String importanceDegree);

    /**
     * 添加备注
     * @param content
     * @return
     */
    Result insertContent(String id,String content,String aidianmaoAdminToken);

    /**
     * 移除我的客户
     * @param id
     * @return
     */
    Result delete(String id);
}
