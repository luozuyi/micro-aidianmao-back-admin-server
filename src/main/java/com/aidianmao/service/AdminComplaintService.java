package com.aidianmao.service;

import com.aidianmao.utils.Result;

import java.util.Map;

public interface AdminComplaintService {
    /**
     * 分页条件查询业务员投诉管理信息
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    public Result pageList(Integer pageNum, Integer pageSize, Map<String,Object> params);
    /**
     * 业务员投诉管理信息详情
     * @param id 主键id
     * @return
     */
    Result detail(String id);
}
