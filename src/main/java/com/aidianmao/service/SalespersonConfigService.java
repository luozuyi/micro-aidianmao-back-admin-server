package com.aidianmao.service;


import com.aidianmao.utils.Result;
import org.elasticsearch.search.DocValueFormat;

import java.math.BigDecimal;
import java.util.Map;

public interface SalespersonConfigService {

    /**
     * 业务员分页条件查询
     * @param pageNum    当前页
     * @param pageSize   一页多少条
     * @param params     参数
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize, Map<String,Object> params,String adminId);

    /**
     * 业务员分组
     * @param groupName   组长名
     * @param list        业务员列表
     * @return
     */
    Result divideGroup(String groupName,String[] list);

    /**
     * 修改业务员信息
     * @param id              主键id
     * @param maximunNum     最大客户数
     * @param roleName       级别名称
     * @return
     */
    Result update(String id, Integer maximunNum, String roleName);

    /**
     * 业务员分部门
     * @param id
     * @param departName  部长名
     * @return
     */
    Result update(String id,String departName);

    /**
     * 详情
     * @param id  主键id
     * @return
     */
    Result detail(String id);


}
