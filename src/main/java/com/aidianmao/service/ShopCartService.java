package com.aidianmao.service;

import com.aidianmao.utils.Result;

import java.util.Map;

public interface ShopCartService {
    /**
     * 分页查询购店车
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数params
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params);
}
