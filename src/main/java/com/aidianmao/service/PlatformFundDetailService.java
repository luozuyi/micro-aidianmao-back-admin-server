package com.aidianmao.service;

import com.aidianmao.utils.Result;

import java.util.Map;

public interface PlatformFundDetailService {
    /**
     * 分页查询平台收支明细
     * @param pageNum   当前页
     * @param pageSize  一页显示多少条
     * @param params 参数map
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize, Map<String,Object> params);
    /**
     * 时间段查询平台收支明细与公司总收入
     * @param pageNum  当前页
     * @param pageSize  一页显示多少条
     * @param params 参数map
     * @return
     */
    Result pageListTotalIncome(Integer pageNum, Integer pageSize, Map<String,Object> params);
}
