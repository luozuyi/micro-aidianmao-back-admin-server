package com.aidianmao.service;

import com.aidianmao.entity.Article;
import com.aidianmao.utils.Result;

import java.util.Map;

public interface ArticleService {

    /**
     * 动态分页条件查询文章列表
     * @param pageNum   当前页
     * @param pageSize  一页显示多少条
     * @param params    条件参数
     * @return
     */
    Result listPageBySelection(Integer pageNum, Integer pageSize, Map<String, Object> params);

    /**
     * 删除文章
     * @param id 主键id
     * @return
     */
    Result delById(String id);

    /**
     * 添加文章
     * @param article 文章对象
     * @param content 内容
     * @return
     */
    Result addArticle(Article article, String content);

    /**
     * 修改文章
     * @param article 文章对象
     * @param content 文章内容
     * @return
     */
    Result updateArticle(Article article, String content);

    /**
     * 跳转修改文章
     * @param id 主键id
     * @return
     */
    Result toDetailArticle(String id);
}
