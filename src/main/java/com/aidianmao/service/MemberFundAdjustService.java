package com.aidianmao.service;

import com.aidianmao.entity.MemberFundAdjust;
import com.aidianmao.utils.Result;
import org.springframework.web.bind.annotation.CookieValue;

import java.util.Map;

public interface MemberFundAdjustService {
    /**
     * 分页条件查询会员资金调整记录
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    public Result pageList(Integer pageNum, Integer pageSize, Map<String,Object> params);
    /**
     * 会员资金调整记录新增
     * @param record 参数实体
     * @return
     */
    Result insert(MemberFundAdjust record, String phone, String aidianmaoAdminToken);

    /**
     * 会员资金调整记录
     * @param fundAdjust    会员资金调整的实体
     * @param phone          手机号
     * @param aidianmaoAdminToken   凭据
     * @return
     */
    Result insertFundAdjust(MemberFundAdjust fundAdjust, String phone, String aidianmaoAdminToken);
}
