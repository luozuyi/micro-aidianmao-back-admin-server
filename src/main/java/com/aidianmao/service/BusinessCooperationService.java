package com.aidianmao.service;

import com.aidianmao.entity.BusinessCooperation;
import com.aidianmao.utils.Result;

import java.util.Map;

public interface BusinessCooperationService {
    /**
     * 商业合作列表
     * @param pageNum  当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize, Map<String,Object> params);



    /**
     * 合作详情
     * @param id 主键id
     * @return
     */
    Result detail(String id);
}
