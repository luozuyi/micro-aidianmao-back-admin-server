package com.aidianmao.service;

import com.aidianmao.entity.DepartmentTarget;
import com.aidianmao.utils.Result;

import java.util.Map;

public interface DepartmentTargetService {
    /**
     * 分页条件部门月目标
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize, Map<String,Object> params);
    /**
     * 部门月目标详情
     * @param id 主键id
     * @return
     */
    Result detail(String id);
    /**
     * 部门月目标修改
     * @param record 参数实体
     * @return
     */
    Result update(DepartmentTarget record);
    /**
     * 部门月目标删除
     * @param id 主键id
     * @return
     */
    Result delete(String id);
    /**
     * 部门月目标新增
     * @param record 参数实体
     * @return
     */
    Result insert(DepartmentTarget record,String departNme);
}
