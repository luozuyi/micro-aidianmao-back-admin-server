package com.aidianmao.service;

import com.aidianmao.entity.Achievement;
import com.aidianmao.utils.Result;

import java.util.Map;

public interface AchievementService {
    /**
     * 分页条件查询业绩申报
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize, Map<String,Object> params);
    /**
     * 业绩申报详情
     * @param id 主键id
     * @return
     */
    Result detail(String id);
    /**
     * 业绩申报修改
     * @param record 参数实体
     * @param checkType 审核类型
     * @param note   审核结果
     * @param checkResult 审核结果
     * @param aidianmaoAdminToken 凭据
     * @return
     */
    Result update(Achievement record,String note,Integer checkType,Integer checkResult,String aidianmaoAdminToken);
    /**
     * 业绩申报新增
     * @param record 参数实体
     * @param aidianmaoAdminToken  凭据
     * @return
     */
    Result insert(Achievement record,String buyerAdminName,String sellerAchieveName,String aidianmaoAdminToken);

    /**
     * 作废
     * @param id  主键
     * @return
     */
    Result cancellation(String id);

    /**
     * 审核
     * @param id            主键id
     * @param checkType     审核结果 0:不通过  1: 通过
     * @param aidianmaoAdminToken 凭据
     * @return
     */
    Result checkAchieve(String id, String checkType, String nate,String aidianmaoAdminToken);

    /**
     * 审核
     * @param id            主键id
     * @param checkType     核实结果 0:不通过  1: 通过
     * @return
     */
    Result verifyAchieve(String id, String checkType, String nate);

    /**
     * 修改审核结果
     * @param id
     * @param checkType   核实结果 0:不通过  1: 通过
     * @return
     */
    Result updateAchieve(String id, String checkType);
}
